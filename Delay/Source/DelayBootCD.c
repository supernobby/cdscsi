#include <proto/exec.h>
#include <exec/execbase.h>
#include <proto/expansion.h>
#include <proto/intuition.h>
#include <intuition/intuitionbase.h>
#include <pragma/graphics_lib.h>
#include <graphics/gfxbase.h>
#include <libraries/expansion.h>
#include <libraries/expansionbase.h>
#include <resources/filesysres.h>
#include <exec/resident.h>
#include <proto/timer.h>
#include <devices/trackdisk.h>
#include "CompilerExtensions.h"
#include "ConfigFileData.h"
#include "Version.h"


/*
** function prototypes
*/
ULONG rommain_delay( REG( a0, ULONG *SegList ), REG( a6, struct ExecBase *SysBase ) ) ;
BOOL DelayBootCD( struct ExecBase *SysBase ) ;


/*
** standard version tag
*/
#define NAME_STRING "CdScsi Delay"
static const char VersionTag[ ] = "\0$VER: " NAME_STRING " " MAJOR_STRING "." MINOR_STRING "." REVISION_STRING " (" DATE_STRING ")"  ;


/*
** the ROMTag structure that we can run from FLASH ROM
*/
volatile const struct Resident ROMTag_Delay =
{
  RTC_MATCHWORD, /* rt_MatchWord; word to match on (ILLEGAL) */
  ( struct Resident * )&ROMTag_Delay,  /* struct Resident *rt_MatchTag; pointer to the above */
  ( APTR )&ROMTag_Delay + 1,  /* rt_EndSkip; address to continue scan */
  RTF_COLDSTART,  /* rt_Flags;  various tag flags */
  0,  /* rt_Version; release version number */
  NT_UNKNOWN,  /* rt_Type; type of module (NT_XXXXXX) */
  -51, /* rt_Pri; initialization priority */
  NAME_STRING,  /* *rt_Name; pointer to node name */
  ( char * )&VersionTag[ 7 ],  /* *rt_IdString; pointer to identification string */
  rommain_delay  /* rt_Init; pointer to init code */
} ;


/*
**  entry function when in ROM
*/
ULONG rommain_delay( REG( a0, ULONG *SegList ), REG( a6, struct ExecBase *SysBase ) )
{
  if( NULL != &ROMTag_Delay )
  {  /* ROMTag ok */
    DelayBootCD( SysBase ) ;
  }
  else
  {  /* ROMTag not ok */
  }

  return( 1 ) ;
}


#if 1

void ToggleScreen( struct ExecBase *SysBase, ULONG Pen ) ;


void ToggleScreen( struct ExecBase *SysBase, ULONG Pen )
{
  struct IntuitionBase *IntuitionBase ;
  struct GfxBase *GfxBase ;
  struct Screen *BootScreen ;
  volatile ULONG Count ;
  struct NewScreen NewScr =
  {
    0, 0, 320, 256, 4,
    0, 1,
    0, CUSTOMSCREEN,
    NULL,
    NULL,
    NULL,
    NULL
  } ;

  IntuitionBase = ( struct IntuitionBase * )OpenLibrary( "intuition.library", 0 ) ;
  if( NULL != IntuitionBase )
  {
    GfxBase = ( struct GfxBase * )OpenLibrary( "graphics.library", 0 ) ;
    if( NULL != GfxBase )
    {
      BootScreen = OpenScreen( &NewScr ) ;
      if( NULL != BootScreen )
      {
        SetRGB4( &BootScreen->ViewPort, 0, 0, 0, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 1, 15, 0, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 2, 0, 15, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 3, 0, 0, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 4, 15, 15, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 5, 15, 0, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 6, 0, 15, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 7, 7, 7, 7 ) ;
        SetRGB4( &BootScreen->ViewPort, 8, 15, 15, 15 ) ;
        for( Count = 0 ; Count < 0x4F ; Count++ )
        {
          SetRast( &BootScreen->RastPort, Pen ) ;
          //SetRast( &BootScreen->RastPort, ( unsigned char )( Count & 0xFF ) ) ;
        }
        SetRast( &BootScreen->RastPort, 0 ) ;
        CloseScreen( BootScreen ) ;
      }
      CloseLibrary( ( struct Library * )GfxBase ) ;
    }
    CloseLibrary( ( struct Library * )IntuitionBase ) ;
  }
}

#endif


/*
**  delay the boot CD
*/
BOOL DelayBootCD( struct ExecBase *SysBase )
{
  struct ExpansionBase *ExpansionBase ;
  struct BootNode *WalkBootNode ;
  struct MsgPort *TrackdiskMsgPort ;
  struct IOExtTD *TrackdiskRequest ;
  struct MsgPort *TimerMsgPort ;
  struct timerequest *DelayTimeRequest ;
  struct Device *TimerBase ;
  struct Resident *ConfigResident ;
  struct timeval DelayTimeval ;
  UBYTE *Name ;
  ULONG BootDelay ;
  BYTE IsBootDevice ;

  ExpansionBase = ( struct ExpansionBase * )OpenLibrary( "expansion.library", 0 ) ;
  if( NULL != ExpansionBase )
  {  /* expansion library ok */
    IsBootDevice = 0 ;
    WalkBootNode = ( struct BootNode * )ExpansionBase->MountList.lh_Head ;
    while( ( NULL != ( ( ( struct Node * )WalkBootNode )->ln_Succ ) ) && ( 0 == IsBootDevice ) )
    {
      Name = BADDR( ( ( struct DeviceNode * )WalkBootNode->bn_DeviceNode )->dn_Name ) ;
      if( 3 == Name[ 0 ]  && ( 'D' == Name[ 1 ] && 'F' == Name[ 2 ] && '0' == Name[ 3 ] ) )
      {  /* check if DF0 has a disk inserted */
        TrackdiskMsgPort = CreateMsgPort( ) ;
        if( NULL != TrackdiskMsgPort )
        {  /* trackdisk message port ok */
          TrackdiskRequest = ( struct IOExtTD * )CreateIORequest( TrackdiskMsgPort , sizeof ( struct IOExtTD ) ) ;
          if( TrackdiskRequest )
          {  /* trackdisk io request ok */
            /* open the trackdisk device */
            OpenDevice( TD_NAME, 0, ( struct IORequest * )TrackdiskRequest, 0 ) ;
            if( 0 == TrackdiskRequest->iotd_Req.io_Error )
            {  /* trackdisk device ok */
              TrackdiskRequest->iotd_Req.io_Command = TD_CHANGESTATE ;
              DoIO( ( struct IORequest * )TrackdiskRequest ) ;
              if( ( 0 == TrackdiskRequest->iotd_Req.io_Error ) && ( 0 == TrackdiskRequest->iotd_Req.io_Actual ) )
              {  /* there is a disk in the floppy drive */
                IsBootDevice = -1 ;
                //ToggleScreen( SysBase, 1 ) ;
              }
              CloseDevice( ( struct IORequest * )TrackdiskRequest ) ;
            }
            else
            {  /* trackdisk device not ok */
            }
            DeleteIORequest( ( struct IORequest * )TrackdiskRequest ) ;
          }
          else
          {  /* trackdisk io request ok */
          }
          DeleteMsgPort( TrackdiskMsgPort ) ;
        }
        else
        {  /* timer message port not ok */
        }
      }
      else if( 3 == Name[ 0 ]  && ( 'C' == Name[ 1 ] && 'D' == Name[ 2 ] && '0' == Name[ 3 ] ) )
      {
        IsBootDevice = 1 ;
      }
      else
      {  /* some other device before CD0: */
        IsBootDevice = -1 ;
      }
      WalkBootNode = ( struct BootNode * )( ( struct Node * )WalkBootNode )->ln_Succ ;
    }
    if( 1 == IsBootDevice )
    {  /* CD0 is the boot device */
      //ToggleScreen( SysBase, 2 ) ;
      TimerMsgPort = CreateMsgPort( ) ;
      if( NULL != TimerMsgPort )
      {  /* timer message port ok */
        /* create the timer io request */
        DelayTimeRequest = ( struct timerequest * )CreateIORequest( TimerMsgPort , sizeof ( struct timerequest ) ) ;
        if( DelayTimeRequest )
        {  /* timer io request ok */
          /* open the timer device */
          OpenDevice( TIMERNAME, UNIT_WAITUNTIL, ( struct IORequest * )DelayTimeRequest, 0 ) ;
          if( 0 == DelayTimeRequest->tr_node.io_Error )
          {  /* timer device ok */
            BootDelay = 5 ;  /* defaults to 5 seconds */
            ConfigResident = FindResident( CONFIG_RESIDENT_NAME ) ;
            if( NULL != ConfigResident )
            {  /* config resident found in rom */
              if( 1 <=( ( struct ConfigFileData * )ConfigResident->rt_Init )->cfd_Version )
              {  /* boot delay value avialable in config */
                BootDelay = ( ( struct ConfigFileData * )ConfigResident->rt_Init )->cfd_BootDelay ;
              }
            }
            /* store timer base to call functions of the timer device */
            TimerBase = DelayTimeRequest->tr_node.io_Device ;
            GetSysTime( &DelayTimeval ) ;
            DelayTimeval.tv_secs += BootDelay ;
            DelayTimeRequest->tr_time = DelayTimeval ;
            DelayTimeRequest->tr_node.io_Command = TR_ADDREQUEST ;
            SendIO( ( struct IORequest * )DelayTimeRequest ) ;
            WaitIO( ( struct IORequest * )DelayTimeRequest ) ;
            CloseDevice( ( struct IORequest * )DelayTimeRequest ) ;
          }
          else
          {  /* timer device not ok */
          }
          DeleteIORequest( ( struct IORequest * )DelayTimeRequest ) ;
        }
        else
        {  /* timer io request ok */
        }
        DeleteMsgPort( TimerMsgPort ) ;
      }
      else
      {  /* timer message port not ok */
      }
    }
    else
    {  /* CD0 is not the boot device */
    }
    CloseLibrary( ( struct Library * )ExpansionBase ) ;
  }
  else
  {  /* expansion library ok */
  }

  return( TRUE ) ;
}


