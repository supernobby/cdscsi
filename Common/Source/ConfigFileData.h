/*
 * ConfigFileData.h
 */

#include <exec/types.h>


#define CONFIG_VERSION ( 1 )
#define CONFIG_RESIDENT_NAME "cdscsi.config"
#define CONFIG_SCSIDEVICE_LENGTH ( 32UL )


/*
 * block of data how settings are stored in the config file
 */
struct ConfigFileData
{
  ULONG cfd_Version ;
  UBYTE cfd_ScsiDevice[ CONFIG_SCSIDEVICE_LENGTH ] ;
  ULONG cfd_ScsiUnit ;
  ULONG cfd_AudioOutput ;
  ULONG cfd_AhiUnit ;
  ULONG cfd_LogLevel ;
  LONG cfd_BootPriority ;
  LONG cfd_BootDelay ;
} ;
