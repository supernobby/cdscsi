/*
** Version.h
*/


#ifndef _VERSION_H
#define _VERSION_H


/*
** version definitions
*/
#define CDSCSI_MAJOR 0
#define CDSCSI_MINOR 2
#define CDSCSI_REVISION 0


/*
** string helper definitions
*/
#define QUOTE(_str) #_str
#define EXPAND_AND_QUOTE(_str) QUOTE(_str)


/*
** common version string definitions
*/
#define MAJOR_STRING EXPAND_AND_QUOTE( CDSCSI_MAJOR )
#define MINOR_STRING EXPAND_AND_QUOTE( CDSCSI_MINOR )
#define REVISION_STRING EXPAND_AND_QUOTE( CDSCSI_REVISION )
#define DATE_STRING __DATE__
#define COPYRIGHTPERIOD_STRING "Copyright 2020-2024"
#define AUTHOR_STRING "Andreas Barth"


/*
** some default strings exported by this module
*/
char *GetVersionTagString( void ) ;
char *GetVersionString( void ) ;
char *GetCopyrightString( void ) ;
char *GetAuthorString( void ) ;


#endif  /* _VERSION_H */
