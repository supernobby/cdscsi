# CdScsi

This is software for the classic AmigaOS 3.x to emulate the cd.device.
It should enable the Amiga to boot from a CD-ROM drive that is connected to the computer. So all the compontens are ROM-able.
Also the CD playing functions are implemented.

The basis is a cd.device clone, that forwards device commands as best as it can to a real scsi device.
Which scsi device and unit is used can be configured in a Prefs program and stored in a config file.
A little boot tool can be used to create a boot entry for the CD-ROM drive.

## Requirements
Obviously an Amiga with a CD-ROM drive connected to it.
An overview of the required components from this package are:

- cd.device (the cd.device clone that uses the real scsi device)
- CdScsi (the prefs program used to create the config file)
- CdScsiBoot (which creates the boot entry)
- CdScsiDelay (which causes a boot delay to avoid the "No disk" dialog from the OS)
- cdscsi.config (the config, created and used by other components)

An importand dependency for this to work is a CDFileSystem found in the FileSystem.resource at boot time with DOS type: 0x43443031UL /* 'CD01' */.
The CDFileSystem included in AmigaOS 3.2.1 (and probably newer) can be used for this.
I had tested this also with the "BootCDFileSystem" 50.21 found on the AmigaOS 4.1 CD for Classic Systems.

Generally, the package expects AmigaOS 3.x and the prefs program uses the ClassAct/Reaction GUI toolkit.

## Usage
First use the CdScsi prefs program to configure to what scsi device the CD-ROM is connected. The device name depends on the used host controller.
Configure also the unit number at that the CD-ROM is found.
Then save the configuration. This creates cdscsi.config in ENV.

Then, to boot from the CD-ROM, load the BootCDFileSystem, cd.device, CdScsiBoot and cdscsi.config resident. This can be done with LoadModule.
But these files can also be stored in the FlashROM of USB cards like Algor and Deneb.

If you see a "No Disk" dialog when booting from a CD, then this is because the drive is too slow for the OS to detect the disk. In that case you can use the CdScsiDelay module and configure the actual timeout in the prefs.

## Important
If the device and/or unit of the CD-ROM drive was changed, you need to use the prefs program to change the configuration and also update the resident files, if you use the boot feature. If you stored things in a FlashROM, this needs to be reflashed.
