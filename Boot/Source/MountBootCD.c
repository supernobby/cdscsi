#include <proto/exec.h>
#include <exec/execbase.h>
#include <proto/expansion.h>
#include <proto/intuition.h>
#include <intuition/intuitionbase.h>
#include <pragma/graphics_lib.h>
#include <graphics/gfxbase.h>
#include <libraries/expansion.h>
#include <libraries/expansionbase.h>
#include <resources/filesysres.h>
#include <exec/resident.h>
#include <proto/timer.h>
#include "CompilerExtensions.h"
#include "ConfigFileData.h"
#include "Version.h"


/*
** function prototypes
*/
ULONG rommain_mount( REG( a0, ULONG *SegList ), REG( a6, struct ExecBase *SysBase ) ) ;
BOOL MountBootCD( struct ExecBase *SysBase ) ;


/*
** standard version tag
*/
#define NAME_STRING "CdScsi Boot"
static const char VersionTag[ ] = "\0$VER: " NAME_STRING " " MAJOR_STRING "." MINOR_STRING "." REVISION_STRING " (" DATE_STRING ")"  ;


/*
** the ROMTag structure that we can run from FLASH ROM
*/
volatile const struct Resident ROMTag_Mount =
{
  RTC_MATCHWORD, /* rt_MatchWord; word to match on (ILLEGAL) */
  ( struct Resident * )&ROMTag_Mount,  /* struct Resident *rt_MatchTag; pointer to the above */
  ( APTR )&ROMTag_Mount + 1,  /* rt_EndSkip; address to continue scan */
  RTF_COLDSTART,  /* rt_Flags;  various tag flags */
  0,  /* rt_Version; release version number */
  NT_UNKNOWN,  /* rt_Type; type of module (NT_XXXXXX) */
  -49, /* rt_Pri; initialization priority */
  "CdScsi Mount",  /* *rt_Name; pointer to node name */
  ( char * )&VersionTag[ 7 ],  /* *rt_IdString; pointer to identification string */
  rommain_mount  /* rt_Init; pointer to init code */
} ;


/*
**  entry function when in ROM
*/
ULONG rommain_mount( REG( a0, ULONG *SegList ), REG( a6, struct ExecBase *SysBase ) )
{
  if( NULL != &ROMTag_Mount )
  {  /* ROMTag ok */
    MountBootCD( SysBase ) ;
  }
  else
  {  /* ROMTag not ok */
  }

  return( 1 ) ;
}


#if 0

void ToggleScreen( struct ExecBase *SysBase, ULONG Pen ) ;


void ToggleScreen( struct ExecBase *SysBase, ULONG Pen )
{
  struct IntuitionBase *IntuitionBase ;
  struct GfxBase *GfxBase ;
  struct Screen *BootScreen ;
  volatile ULONG Count ;
  struct NewScreen NewScr =
  {
    0, 0, 320, 256, 4,
    0, 1,
    0, CUSTOMSCREEN,
    NULL,
    NULL,
    NULL,
    NULL
  } ;

  IntuitionBase = ( struct IntuitionBase * )OpenLibrary( "intuition.library", 0 ) ;
  if( NULL != IntuitionBase )
  {
    GfxBase = ( struct GfxBase * )OpenLibrary( "graphics.library", 0 ) ;
    if( NULL != GfxBase )
    {
      BootScreen = OpenScreen( &NewScr ) ;
      if( NULL != BootScreen )
      {
        SetRGB4( &BootScreen->ViewPort, 0, 0, 0, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 1, 15, 0, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 2, 0, 15, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 3, 0, 0, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 4, 15, 15, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 5, 15, 0, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 6, 0, 15, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 7, 7, 7, 7 ) ;
        SetRGB4( &BootScreen->ViewPort, 8, 15, 15, 15 ) ;
        for( Count = 0 ; Count < 0xFF ; Count++ )
        {
          SetRast( &BootScreen->RastPort, Pen ) ;
          //SetRast( &BootScreen->RastPort, ( unsigned char )( Count & 0xFF ) ) ;
        }
        SetRast( &BootScreen->RastPort, 0 ) ;
        CloseScreen( BootScreen ) ;
      }
      CloseLibrary( ( struct Library * )GfxBase ) ;
    }
    CloseLibrary( ( struct Library * )IntuitionBase ) ;
  }
}

#endif


LONG StartCdfs( void )
{
  struct Library *SysBase = *( struct Library ** )4 ;
  struct Resident *CdfsResident ;
  LONG RetVal ;

  CdfsResident = FindResident( "cdfs.library" ) ;

  if( CdfsResident )
  {
    RetVal = ( LONG )InitResident( CdfsResident, TRUE ) ;
  }
  else
  {
    RetVal = -1 ;
  }

  return( RetVal ) ;
}



struct FakeSegList
{
  ULONG fsl_Size ;
  ULONG fsl_List[ 2 ] ;
} ;


const struct FakeSegList CdfsFakeSegList =
{
  ( ULONG )( ( sizeof( struct FakeSegList ) + 3 ) & ( ( ULONG ) ~3 ) ),
  {
    0,
    ( ULONG )StartCdfs
  }
} ;



void FakeCdfsFileSysEntry( struct ExecBase *SysBase, struct List *FileSysEntries )
{
  struct FileSysEntry *CdfsStarterFileSysEntry ;

  CdfsStarterFileSysEntry = ( struct FileSysEntry * )AllocMem( sizeof( struct FileSysEntry ), MEMF_PUBLIC | MEMF_CLEAR ) ;
  if( CdfsStarterFileSysEntry )
  {
    CdfsStarterFileSysEntry->fse_Node.ln_Name = "CDFileSystemStarter" ;
    CdfsStarterFileSysEntry->fse_DosType = 0x43443031UL /* 'CD01' */ ;
    CdfsStarterFileSysEntry->fse_Version = 1<<16 | 1 ;
    CdfsStarterFileSysEntry->fse_PatchFlags = 0x190 ; // GlobalVec, SegList and StackSize
    CdfsStarterFileSysEntry->fse_StackSize = 8*1024 ;
    CdfsStarterFileSysEntry->fse_Priority = 10 ;
    CdfsStarterFileSysEntry->fse_SegList = MKBADDR( &CdfsFakeSegList.fsl_List ) ;
    CdfsStarterFileSysEntry->fse_GlobalVec = -1 ;

    AddHead( FileSysEntries, &CdfsStarterFileSysEntry->fse_Node);
  }
}


/*
 *  find the cdfs file system entry
 */
struct FileSysEntry *FindCdfsFileSysEntry( struct List *FileSysEntries )
{
  struct FileSysEntry *CdfsFileSysEntry, *WalkFileSysEntry ;

  CdfsFileSysEntry = NULL ;
  WalkFileSysEntry = ( struct FileSysEntry * )FileSysEntries->lh_Head ;
  while( WalkFileSysEntry->fse_Node.ln_Succ )
  {
    if( 0x43443031UL /* 'CD01' */ == WalkFileSysEntry->fse_DosType )
    {
      CdfsFileSysEntry = WalkFileSysEntry ;
      break ;
    }
    WalkFileSysEntry = ( struct FileSysEntry * )WalkFileSysEntry->fse_Node.ln_Succ ;
  }

  return( CdfsFileSysEntry ) ;
}


/*
** data required to fake a ConfigDev with boot code
*/
struct FakeBoard
{
  struct ConfigDev fb_ConfigDev ;
  struct DiagArea fb_DiagArea ;
  ULONG fb_BootCode[ 8 ] ;
} ;


/*
**  mount the boot CD
*/
BOOL MountBootCD( struct ExecBase *SysBase )
{
  struct ExpansionBase *ExpansionBase ;
  struct FileSysResource *FileSysResource ;
  struct FileSysEntry *CdfsFileSysEntry ;
  struct DeviceNode *BootDeviceNode ;
  struct ConfigDev *BootConfigDev ;
  struct FakeBoard *BootFakeBoard ;
  ULONG ParmPkt[ 21 ] ;
  struct Resident *ConfigResident ;
  LONG BootPriority ;
  struct MsgPort *CdMsgPort ;
  struct IOStdReq *CdIORequest ;
  BOOL RetVal ;

  RetVal = FALSE ;

#if 0
  CdMsgPort = CreateMsgPort( ) ;
  if( NULL != CdMsgPort )
  {  /* cd message port ok */
    /* create the cd io request */
    CdIORequest = ( struct IOStdReq * )CreateIORequest( CdMsgPort , sizeof ( struct IOStdReq ) ) ;
    if( CdIORequest )
    {  /* cd io request ok */
      /* open the cd device */
      OpenDevice( "cd.device", 0, ( struct IORequest * )CdIORequest, 0 ) ;
      if( 0 == CdIORequest->io_Error )
      {  /* cd device ok */
#endif
        ExpansionBase = ( struct ExpansionBase * )OpenLibrary( "expansion.library", 36 ) ;
        if( NULL != ExpansionBase )
        {  /* expansion library ok */
          FileSysResource = OpenResource( "FileSystem.resource" ) ;
          if( NULL != FileSysResource )
          {  /* file system resource ok */
            CdfsFileSysEntry = FindCdfsFileSysEntry( &FileSysResource->fsr_FileSysEntries ) ;
            if( NULL == CdfsFileSysEntry )
            {  /* cdfs not yet ok */
              //FakeCdfsFileSysEntry( SysBase, &FileSysResource->fsr_FileSysEntries ) ;
              //CdfsFileSysEntry = FindCdfsFileSysEntry( &FileSysResource->fsr_FileSysEntries ) ;
            }
            if( NULL != CdfsFileSysEntry )
            {  /* cdfs ok */
              ConfigResident = FindResident( CONFIG_RESIDENT_NAME ) ;
              BootPriority = 0 ;  /* defaults to 0 */
              if( NULL != ConfigResident )
              {  /* config resident found in rom */
                BootPriority = ( ( struct ConfigFileData * )ConfigResident->rt_Init )->cfd_BootPriority ;
              }
              BootFakeBoard = AllocMem( sizeof( struct FakeBoard ), MEMF_CLEAR ) ;
              if( NULL != BootFakeBoard )
              {  /* boot fake board ok */
                BootFakeBoard->fb_ConfigDev.cd_Rom.er_Type = ERTF_DIAGVALID ;
                BootFakeBoard->fb_ConfigDev.cd_Rom.er_Reserved0c = 0xFF & ( ( ( ULONG )( &BootFakeBoard->fb_DiagArea ) >> 24 ) ) ;
                BootFakeBoard->fb_ConfigDev.cd_Rom.er_Reserved0d = 0xFF & ( ( ( ULONG )( &BootFakeBoard->fb_DiagArea ) >> 16 ) ) ;
                BootFakeBoard->fb_ConfigDev.cd_Rom.er_Reserved0e = 0xFF & ( ( ( ULONG )( &BootFakeBoard->fb_DiagArea ) >> 8 ) ) ;
                BootFakeBoard->fb_ConfigDev.cd_Rom.er_Reserved0f = 0xFF & ( ( ( ULONG )( &BootFakeBoard->fb_DiagArea ) >> 0 ) ) ;
                BootFakeBoard->fb_DiagArea.da_Config = DAC_CONFIGTIME ;
                BootFakeBoard->fb_DiagArea.da_BootPoint = ( UWORD ) ( ( ULONG )( BootFakeBoard->fb_BootCode ) - ( ULONG )( &BootFakeBoard->fb_DiagArea ) ) ;
                BootFakeBoard->fb_BootCode[ 0 ] = 0x43FA0010 ;
                BootFakeBoard->fb_BootCode[ 1 ] = 0x4EAEFFA0 ;
                BootFakeBoard->fb_BootCode[ 2 ] = 0x22407200 ;
                BootFakeBoard->fb_BootCode[ 3 ] = 0x4EAEFF9A ;
                BootFakeBoard->fb_BootCode[ 4 ] = 0x4E75646F ;
                BootFakeBoard->fb_BootCode[ 5 ] = 0x732E6C69 ;
                BootFakeBoard->fb_BootCode[ 6 ] = 0x62726172 ;
                BootFakeBoard->fb_BootCode[ 7 ] = 0x79000000 ;
                BootConfigDev = &BootFakeBoard->fb_ConfigDev ;

                ParmPkt[ 0 ] = ( ULONG )"CD0" ;
                ParmPkt[ 1 ] = ( ULONG )"cd.device" ;
                ParmPkt[ 2 ] = 0 ;  /* unit number */
                ParmPkt[ 3 ] = 0 ;  /* flags for OpenDevice( ) */
                ParmPkt[ 4 ] = 16 ;  /* number of longwords to come */
                ParmPkt[ 5 ] = 2048 ;  /* physical disk block size */
                ParmPkt[ 6 ] = 0 ;  /* sector origin */
                ParmPkt[ 7 ] = 1 ;  /* number of surfaces */
                ParmPkt[ 8 ] = 1 ;  /* sectors per logical block */
                ParmPkt[ 9 ] = 1 ;  /* blocks per track */
                ParmPkt[ 10 ] = 0 ;  /* reserved blocks */
                ParmPkt[ 11 ] = 0 ;  /* ?? */
                ParmPkt[ 12 ] = 0 ;  /* interleave */
                ParmPkt[ 13 ] = 0 ;  /* lower cylinder */
                ParmPkt[ 14 ] = 0 ;  /* upper cylinder */
                ParmPkt[ 15 ] = 5 ;  /* num of buffers */
                ParmPkt[ 16 ] = MEMF_PUBLIC ;  /* type of mem for the buffer */
                ParmPkt[ 17 ] = 0x100000 ;  /* max transfer */
                ParmPkt[ 18 ] = 0x7ffffffe ;  /* mask */
                ParmPkt[ 19 ] = BootPriority ;  /* boot prio */
                ParmPkt[ 20 ] = 0x43443031 ;  /* dostype of CDFileSystem */
                BootDeviceNode = MakeDosNode( ( CONST APTR ) ParmPkt ) ;
                if( NULL != BootDeviceNode )
                {  /* boot device node ok */
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000001 ) BootDeviceNode->dn_Type = CdfsFileSysEntry->fse_Type ;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000002 ) BootDeviceNode->dn_Task = ( struct MsgPort * )CdfsFileSysEntry->fse_Task;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000004 ) BootDeviceNode->dn_Lock = CdfsFileSysEntry->fse_Lock;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000008 ) BootDeviceNode->dn_Handler = CdfsFileSysEntry->fse_Handler;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000010 ) BootDeviceNode->dn_StackSize = CdfsFileSysEntry->fse_StackSize;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000020 ) BootDeviceNode->dn_Priority = CdfsFileSysEntry->fse_Priority;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000040 ) BootDeviceNode->dn_Startup = CdfsFileSysEntry->fse_Startup;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000080 ) BootDeviceNode->dn_SegList = CdfsFileSysEntry->fse_SegList;
                  if( CdfsFileSysEntry->fse_PatchFlags & 0x00000100 ) BootDeviceNode->dn_GlobalVec = CdfsFileSysEntry->fse_GlobalVec;
                  if( 0 != AddBootNode( BootPriority, ADNF_STARTPROC, BootDeviceNode, BootConfigDev ) )
                  {  /* adding boot node ok */
                    RetVal = TRUE ;
                  }
                  else
                  {  /* adding boot node ok */
                    FreeMem( BootFakeBoard, sizeof( struct FakeBoard ) ) ;
                  }
                }
                else
                {  /* boot device node not ok */
                  FreeMem( BootFakeBoard, sizeof( struct FakeBoard ) ) ;
                }
              }
              else
              {  /* boot fake board not ok */
              }
            }
            else
            {  /* cdfs not ok */
            }
          }
          else
          {  /* file system resource not ok */
          }
          CloseLibrary( ( struct Library * )ExpansionBase ) ;
        }
        else
        {  /* expansion library ok */
        }
#if 0
        CloseDevice( ( struct IORequest * )CdIORequest ) ;
      }
      else
      {  /* cd device not ok */
      }
      DeleteIORequest( ( struct IORequest * )CdIORequest ) ;
    }
    else
    {  /* cd io request ok */
    }
    DeleteMsgPort( CdMsgPort ) ;
  }
  else
  {  /* cd message port not ok */
  }
#endif

  return( RetVal ) ;
}
