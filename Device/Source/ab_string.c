/*
** ab_stdlib.c
*/


#include "ab_string.h"


long ab_strlen( const char *String )
{
  long Result ;
  unsigned long StringIndex, Skip ;

  Result = Skip = StringIndex = 0 ;
  while( ( '\0' != String[ StringIndex ] ) )
  {
    if( Skip )
    {
      Skip = 0 ;
    }
    else
    {
      Result++ ;
      if( '\\' == String[ StringIndex ] )
      {
        Skip = 1 ;
      }
    }
  
    StringIndex++ ;
  }

  return( Result ) ;
}
