/*
** ab_stdio.h
*/


#ifndef _AB_STDIO_H
#define _AB_STDIO_H


#include <stdarg.h>


int ab_vsprintf( char *Buffer, const char *Format, va_list Arguments ) ;
int ab_sprintf( char *Buffer, const char *Format, ... ) ;
int ab_vsnprintf( char *Buffer, int Length, const char * Format, va_list Arguments ) ;
int ab_snprintf( char *Buffer, int Length, const char * Format, ... ) ;


#endif  /* _AB_STDIO_H */
