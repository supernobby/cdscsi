/*
** LogOutput.c
*/


#include "LogOutput.h"
#include "DeviceCore.h"
#include "Settings.h"
#include "ab_stdio.h"
#include <proto/exec.h>
#include <devices/serial.h>
#include <stdarg.h>



/*
** get resources belonging to serial output context and set bautrate
*/
static void ConfigSerial( struct ExecBase *SysBase, unsigned long BaudRate )
{
  struct MsgPort *MyMsgPort ;
  struct IOExtSer *MyIOExtSer ;

  MyMsgPort = CreateMsgPort( ) ;
  if( NULL != MyMsgPort )
  {  /* message port ok */
    MyIOExtSer = ( struct IOExtSer * )CreateIORequest( MyMsgPort , sizeof ( struct IOExtSer ) ) ;
    if( NULL != MyIOExtSer )
    {  /* io request ok */
      OpenDevice( "serial.device", 0, ( struct IORequest * )MyIOExtSer, 0 ) ;
      if( 0 == MyIOExtSer->IOSer.io_Error )
      {  /* serial device ok */
        MyIOExtSer->IOSer.io_Command = SDCMD_SETPARAMS ;
        MyIOExtSer->io_Baud = BaudRate ;
        MyIOExtSer->io_SerFlags &= ~( SERF_PARTY_ON ) ;
        MyIOExtSer->io_SerFlags |= ( SERF_XDISABLED ) ;
        MyIOExtSer->io_StopBits = 1 ;
        DoIO( ( struct IORequest * )MyIOExtSer ) ;
        /* all done */
        CloseDevice( ( struct IORequest * )MyIOExtSer ) ;
      }
      else
      {  /* serial device not ok */
      }
      DeleteIORequest( ( struct IORequest * )MyIOExtSer ) ;
    }
    else
    {  /* io request not ok */
    }
    DeleteMsgPort( MyMsgPort ) ;
  }
  else
  {  /* message port not ok */
  }
}




/*
** create the log output context
*/
struct LogOutput *CreateLogOutput( struct DeviceCore *MyDeviceCore )
{
  struct ExecBase *SysBase ;
  struct LogOutput *MyLogOutput ;

  SysBase = MyDeviceCore->dc_SysBase ;

  MyLogOutput = AllocVec( sizeof( struct LogOutput ), ( MEMF_ANY | MEMF_CLEAR ) ) ;
  if( NULL != MyLogOutput )
  {  /* log output memory ok */
    MyLogOutput->lo_DeviceCore = MyDeviceCore ;
    MyLogOutput->lo_LogLevel = OFF_LEVEL ;
    //ConfigSerial( SysBase, 115200 ) ;
  }
  else
  {  /* log output memory not ok */
  }

  return( MyLogOutput ) ;
}


/*
** update log level from settings
*/
void ConfigureLogOutput( struct LogOutput *MyLogOutput )
{
  struct DeviceCore *MyDeviceCore ;
  
  if( NULL != MyLogOutput )
  {  /* log output ok */
    MyDeviceCore = MyLogOutput->lo_DeviceCore ;
    MyLogOutput->lo_LogLevel = GetSetting( MyDeviceCore->dc_Settings, LogLevel ) ;
  }
}


/*
** delete the log output context
*/
void DeleteLogOutput( struct LogOutput *MyLogOutput )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;

  if( NULL != MyLogOutput )
  {  /* log output memory needs to be freed */
    MyDeviceCore = MyLogOutput->lo_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;

    FreeVec( MyLogOutput ) ;
  }
}


/*
** log some formatted message
*/
void LogText( struct LogOutput *MyLogOutput, enum LogLevel MyLogLevel, STRPTR MyFormat, ... )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  va_list MyValues ;
  static const UWORD DebugPutChar[ 5 ] = { 0xCD4B, 0x4EAE, 0xFDFC, 0xCD4B, 0x4E75 } ;

  if( ( NULL != MyLogOutput ) && ( MyLogOutput->lo_LogLevel >= MyLogLevel ) )
  {  /* log text requirements ok */
    MyDeviceCore = MyLogOutput->lo_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;

    va_start( MyValues, MyFormat ) ;
    ab_vsnprintf( MyLogOutput->lo_LogString, LOGOUTPUT_LOGSTRING_LENGTH, MyFormat, MyValues ) ;
    MyFormat = MyLogOutput->lo_LogString ;
    RawDoFmt( ( CONST_STRPTR )"%s\n", &( MyFormat ), ( VOID (*CONST )( VOID ) )DebugPutChar, SysBase ) ;
    va_end( MyValues ) ;
  }
  else
  {  /* log text requirements ok */
  }
}
