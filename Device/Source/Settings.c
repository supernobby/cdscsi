/*
** Settings.c
*/


#include "Settings.h"
#include "ConfigFileData.h"
#include "DeviceCore.h"
#include "LogOutput.h"
#include <proto/exec.h>
#include <proto/dos.h>
#include <exec/resident.h>
#include "ab_stdlib.h"
#include "ab_string.h"


/*
** with this we use the index name as name string
*/
#define MAKE_SETTING_DESCRIPTOR( _Index, _Flags ) { _Index, #_Index, _Flags }


/*
** possible types
*/
#define SETTING_TYPE_STRING ( 0 )
#define SETTING_TYPE_NUMBER ( 1 )
#define SETTING_TYPE_SWITCH ( 2 )


/*
** the setting description
*/
static const struct SettingDescriptor Descriptors[ NUM_SETTINGS ] =
{
  MAKE_SETTING_DESCRIPTOR( ScsiDevice, SETTING_TYPE_STRING ),
  MAKE_SETTING_DESCRIPTOR( ScsiUnit, SETTING_TYPE_NUMBER ),
  MAKE_SETTING_DESCRIPTOR( AudioOutput, SETTING_TYPE_NUMBER ),
  MAKE_SETTING_DESCRIPTOR( AhiUnit, SETTING_TYPE_NUMBER ),
  MAKE_SETTING_DESCRIPTOR( LogLevel, SETTING_TYPE_NUMBER ),
} ;


/*
** set a setting
*/
void SetSetting( struct Settings *MySettings, enum SettingIndex Index, LONG Value )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  STRPTR NewString ;
  ULONG NewStringLength ;
  
  if( ( NULL != MySettings ) && ( NUM_SETTINGS > Index ) )
  {  /* parameter ok */
    MyDeviceCore = MySettings->s_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;
    
    switch( MySettings->s_Descriptors[ Index ].sd_Type )
    {  /* action depends on type */
      case SETTING_TYPE_STRING:
        if( ( NULL != ( STRPTR )Value ) && ( '\0' !=  ( ( STRPTR )Value )[ 0 ] ) )
        {  /* set a new string value */
          NewStringLength = ( ab_strlen( ( STRPTR )Value ) + 1 ) ;
          NewString = AllocVec( NewStringLength, MEMF_ANY ) ;
          if( NULL != NewString )
          {  /* new string setting can be stored */
            CopyMem( ( STRPTR )Value, NewString, NewStringLength ) ;
            if( NULL != ( STRPTR )MySettings->s_Current[ Index ] )
            {  /* old string setting needs to be freed */
              FreeVec( ( APTR )MySettings->s_Current[ Index ] ) ;
            }
            MySettings->s_Current[ Index ] = ( LONG )NewString ;
            LOG_TEXT( ( _DEBUG_LEVEL, "new setting value for %s: %s", MySettings->s_Descriptors[ Index ].sd_Name, NewString ) ) ;
          }
        }
        else
        {  /* no new string value */
          MySettings->s_Current[ Index ] = ( LONG )NULL ;
        }
        break ;
      case SETTING_TYPE_NUMBER:
        MySettings->s_Current[ Index ] = Value ;
        LOG_TEXT( ( _DEBUG_LEVEL, "new setting value for %s: %ld", MySettings->s_Descriptors[ Index ].sd_Name, Value ) ) ;
        break ;
      case SETTING_TYPE_SWITCH:
        MySettings->s_Current[ Index ] = ( 1 && Value ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "new setting value for %s: %ld", MySettings->s_Descriptors[ Index ].sd_Name, ( 1 && Value ) ) ) ;
        break ;
      default:  /* unknown type */
        break ;
    }
    MySettings->s_Changed = 1 ;
  }
  else
  {  /* parameter not ok */
  }
}

/*
** get a setting
*/
LONG GetSetting( struct Settings *MySettings, enum SettingIndex Index )
{
  LONG Value ;
  
  Value = 0 ;

  if( ( NULL != MySettings ) && ( NUM_SETTINGS > Index ) )
  {  /* parameter ok */
    Value = MySettings->s_Current[ Index ] ;
  }
  else
  {  /* parameter not ok */
  }

  return ( Value ) ;
}


/*
** set some defaults
*/
void SetDefaultSettings( struct Settings *MySettings )
{
  if( NULL != MySettings )
  {  /* requirements ok */
    SetSetting( MySettings, ScsiDevice, ( LONG )"scsi.device" ) ;
    SetSetting( MySettings, ScsiUnit, 0 ) ;
    SetSetting( MySettings, AudioOutput, 0 ) ;
    SetSetting( MySettings, AhiUnit, 0 ) ;
    SetSetting( MySettings, LogLevel, OFF_LEVEL ) ;
    MySettings->s_Changed = 0 ;
  }
  else
  {  /* requirements not ok */
  }
}



#if 0
/*
** buffer size for GetVar() buffer
*/
#define GETVAR_BUFFER_LENGTH ( 256 )


/*
** load settings
*/
void LoadSettings( struct Settings *MySettings )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  struct Task *CallerTask ;
  struct DosLibrary *DOSBase ;
  STRPTR GetVarBuffer ;
  LONG Length, Value ;
  struct DosList *MyDosList ;
  
  if( NULL != MySettings )
  {  /* requirements ok */
    MyDeviceCore = MySettings->s_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;

    CallerTask = FindTask( NULL ) ;
    if( NT_PROCESS == CallerTask->tc_Node.ln_Type )
    {  /* we can use dos.library */
      DOSBase = ( struct DosLibrary * )OpenLibrary( "dos.library", 36 ) ;
      if( NULL != DOSBase )
      {  /* dos.library ok */
        MyDosList = LockDosList( LDF_READ | LDF_ASSIGNS ) ;
        MyDosList = FindDosEntry( MyDosList, "ENV", LDF_ASSIGNS ) ;
        UnLockDosList( LDF_READ | LDF_ASSIGNS ) ;
        if( NULL != MyDosList )
        {  /* ENV: is available */
          GetVarBuffer = AllocVec( GETVAR_BUFFER_LENGTH, ( MEMF_ANY | MEMF_CLEAR ) ) ;
          if( NULL != GetVarBuffer )
          {  /* getvar buffer ok */
            Length = GetVar( "CDSCSI/SCSIDEVICE", GetVarBuffer, GETVAR_BUFFER_LENGTH, ( GVF_GLOBAL_ONLY ) ) ;
            if( 0 < Length )
            {  /* got the content */
              Value = ( LONG )GetVarBuffer ;
              SetSetting( MySettings, ScsiDevice, Value ) ;
            }
            else
            {  /* could not get content */
              LOG_TEXT( ( _DEBUG_LEVEL, "could not get var: CDSCSI/SCSIDEVICE" ) ) ;
            }
            Length = GetVar( "CDSCSI/SCSIUNIT", GetVarBuffer, GETVAR_BUFFER_LENGTH, ( GVF_GLOBAL_ONLY ) ) ;
            if( 0 < Length )
            {  /* got the content */
              Value = ab_atol( GetVarBuffer ) ;
              SetSetting( MySettings, ScsiUnit, Value ) ;
            }
            Length = GetVar( "CDSCSI/AUDIOOUTPUT", GetVarBuffer, GETVAR_BUFFER_LENGTH, ( GVF_GLOBAL_ONLY ) ) ;
            if( 0 < Length )
            {  /* got the content */
              Value = ab_atol( GetVarBuffer ) ;
              SetSetting( MySettings, AudioOutput, Value ) ;
            }
            Length = GetVar( "CDSCSI/AHIUNIT", GetVarBuffer, GETVAR_BUFFER_LENGTH, ( GVF_GLOBAL_ONLY ) ) ;
            if( 0 < Length )
            {  /* got the content */
              Value = ab_atol( GetVarBuffer ) ;
              SetSetting( MySettings, AhiUnit, Value ) ;
            }
            Length = GetVar( "CDSCSI/LOGLEVEL", GetVarBuffer, GETVAR_BUFFER_LENGTH, ( GVF_GLOBAL_ONLY ) ) ;
            if( 0 < Length )
            {  /* got the content */
              Value = ab_atol( GetVarBuffer ) ;
              SetSetting( MySettings, LogLevel, Value ) ;
            }
            MySettings->s_Changed = 0 ;
            FreeVec( GetVarBuffer ) ;
          }
          else
          {  /* getvar buffer not ok */
          }
        }
        else
        {  /* ENV: is not available */
        }
        CloseLibrary( ( struct Library * )DOSBase ) ;
      }
      else
      {  /* dos.library not ok */
        LOG_TEXT( ( _ERROR_LEVEL, "dos.library not ok" ) ) ;
      }
    }
    else
    {  /* we can't use dos.library */
      LOG_TEXT( ( _ERROR_LEVEL, "can't use dos.library" ) ) ;
    }
  }
  else
  {  /* requirements not ok */
  }
}
#else


/*
 * set settings from settings file data
 */
static void SetUserSesstings( struct Settings *MySettings, struct ConfigFileData *MyConfigFileData  )
{
  if( NULL != MyConfigFileData )
  {
    SetSetting( MySettings, LogLevel, ( LONG )MyConfigFileData->cfd_LogLevel ) ;
    SetSetting( MySettings, ScsiDevice, ( LONG )&MyConfigFileData->cfd_ScsiDevice ) ;
    SetSetting( MySettings, ScsiUnit, ( LONG )MyConfigFileData->cfd_ScsiUnit ) ;
    SetSetting( MySettings, AudioOutput, ( LONG )MyConfigFileData->cfd_AudioOutput ) ;
    SetSetting( MySettings, AhiUnit, ( LONG )MyConfigFileData->cfd_AhiUnit ) ;
  }
}

/*
** load settings
*/
void LoadSettings( struct Settings *MySettings )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  struct Task *CallerTask ;
  struct DosLibrary *DOSBase ;
  struct DosList *MyDosList ;
  BPTR Segment ;
  struct Resident *ConfigResident ;
  
  if( NULL != MySettings )
  {  /* requirements ok */
    MyDeviceCore = MySettings->s_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;
    Segment = NULL ;
    
    ConfigResident = FindResident( CONFIG_RESIDENT_NAME ) ; 
    if( NULL != ConfigResident )
    {  /* config resident found in rom */
      SetUserSesstings( MySettings, ( struct ConfigFileData * )ConfigResident->rt_Init ) ;
    }
    if( NULL == ConfigResident )
    {  /* config resident not in rom, try file in ENV:cdscsi.config */
      CallerTask = FindTask( NULL ) ;
      if( NT_PROCESS == CallerTask->tc_Node.ln_Type )
      {  /* we can use dos.library */
        DOSBase = ( struct DosLibrary * )OpenLibrary( "dos.library", 36 ) ;
        if( NULL != DOSBase )
        {  /* dos.library ok */
          MyDosList = LockDosList( LDF_READ | LDF_ASSIGNS ) ;
          MyDosList = FindDosEntry( MyDosList, "ENV", LDF_ASSIGNS ) ;
          UnLockDosList( LDF_READ | LDF_ASSIGNS ) ;
          if( NULL != MyDosList )
          {  /* ENV: is available */
            Segment = LoadSeg( "ENV:cdscsi.config" ) ;
            if( NULL != Segment )
            {  /* config file ok */
              ConfigResident = ( struct Resident * )( ( ( ( ULONG )BADDR( Segment ) ) + sizeof( BPTR ) ) ) ;
              if( RTC_MATCHWORD == ConfigResident->rt_MatchWord )
              {
                SetUserSesstings( MySettings, ( struct ConfigFileData * )ConfigResident->rt_Init ) ;
              }
              UnLoadSeg( Segment ) ;
            }
            else
            {  /* config file not ok */
            }
          }
          else
          {  /* ENV: is not available */
          }
          CloseLibrary( ( struct Library * )DOSBase ) ;
        }
        else
        {  /* dos.library not ok */
          LOG_TEXT( ( _ERROR_LEVEL, "dos.library not ok" ) ) ;
        }
      }
      else
      {  /* we can't use dos.library */
        LOG_TEXT( ( _ERROR_LEVEL, "can't use dos.library" ) ) ;
      }
    }
  }
  else
  {  /* requirements not ok */
  }
}


#endif



/*
** alloc space for setting values
*/
static LONG *AllocValues( struct Settings *MySettings )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  LONG *NewValues ;
  
  NewValues = NULL ;
  
  if( NULL != MySettings )
  {  /* requirements ok */
    MyDeviceCore = MySettings->s_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;

    NewValues = AllocVec( ( sizeof( LONG ) * NUM_SETTINGS ), ( MEMF_ANY | MEMF_CLEAR ) ) ;
  }
  else
  {  /* requirements not ok */
  }
   
  return( NewValues ) ;
}


/*
** free space for setting values and if string, also the string
*/
static void FreeValues( struct Settings *MySettings, LONG *OldValues )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  ULONG Count ;

  if( ( NULL != MySettings ) && ( NULL != OldValues ) )
  {  /* space for setting values needs to be freed */
    MyDeviceCore = MySettings->s_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;

    for( Count = 0 ; Count < NUM_SETTINGS ; Count++ )
    {  /* check values for strings */
      if( ( SETTING_TYPE_STRING == MySettings->s_Descriptors[ Count ].sd_Type ) && ( NULL != OldValues[ Count ] ) )
      {  /* string needs to be freed */
        FreeVec( ( APTR )OldValues[ Count ] ) ;
      }
    }
    FreeVec( OldValues ) ;
  }  
}


/*
** create settings context
*/
struct Settings *CreateSettings( struct DeviceCore *MyDeviceCore )
{
  struct ExecBase *SysBase ;
  struct Settings *NewSettings ;
  ULONG Count ;
  
  NewSettings = NULL ;

  if( NULL != MyDeviceCore )
  {  /* requirements ok */
    SysBase = MyDeviceCore->dc_SysBase ;

    for( Count = 0 ; Count < NUM_SETTINGS ; Count++ )
    {  /* check if index number are in order */
      if( Count != Descriptors[ Count ].sd_Index )
      {
        break ;
      }
    }
    if( ( NUM_SETTINGS == Count ) && ( NUM_SETTINGS == ( sizeof( Descriptors ) / sizeof( struct SettingDescriptor ) ) ) )
    {  /* setting descriptors properly ordered */
      NewSettings = AllocVec( sizeof( struct Settings ), MEMF_ANY | MEMF_CLEAR ) ;
      if( NULL != NewSettings )
      {  /* settings context memory ok */
        NewSettings->s_DeviceCore = MyDeviceCore ;
        NewSettings->s_Descriptors = Descriptors ;
        NewSettings->s_Current = AllocValues( NewSettings ) ;
        if( NULL != NewSettings->s_Current )
        {  /* space for current vlaues ok */
        }
        else
        {  /* space for current vlaues not ok */
          LOG_TEXT( ( _ERROR_LEVEL, "space for current setting vlaues not ok" ) ) ;
          DeleteSettings( NewSettings ) ;
          NewSettings = NULL ;
        }
      }
      else
      {  /* settings context memory not ok */
        LOG_TEXT( ( _ERROR_LEVEL, "settings memory not ok" ) ) ;
      }
    }
    else
    {  /* setting descriptors not properly ordered */
      LOG_TEXT( ( _ERROR_LEVEL, "setting descriptors not properly ordered" ) ) ;
    }
  }
  else
  {  /* requirements not ok */
  }

  return( NewSettings ) ;
}


/*
** delete the settings
*/
void DeleteSettings( struct Settings *OldSettings )
{
  struct DeviceCore *MyDeviceCore ; 
  struct ExecBase *SysBase ;
  
  if( NULL != OldSettings )
  {  /* settings context needs to be freed */
    MyDeviceCore = OldSettings->s_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;

    if( NULL != OldSettings->s_Current )
    {  /* old current values need to be freed */
      FreeValues( OldSettings, OldSettings->s_Current ) ;
    }
    FreeVec( OldSettings ) ;
  }
}

