/*
** Version.c
*/


#include "Version.h"


#define NAME_STRING "CdScsi Device"


/*
** standard version tag
*/
static const char VersionTag[ ] = "\0$VER: " NAME_STRING " " MAJOR_STRING "." MINOR_STRING "." REVISION_STRING " (" DATE_STRING ")" ;
static const char Copyright[ ] = COPYRIGHTPERIOD_STRING " " AUTHOR_STRING ;

/*
** return pointer to the version string including $VER:
*/
char *GetVersionTagString( void )
{
  return( ( char * )&VersionTag[ 1 ] ) ;
}


/*
** return pointer to the version string excluding $VER:
*/
char *GetVersionString( void )
{
  return( ( char * )&VersionTag[ 6 ] ) ;
}


/*
** return pointer to the copyright string
*/
char *GetCopyrightString( void )
{
  return( ( char * )&Copyright[ 0 ] ) ;
}


/*
** return pointer to the author string
*/
char *GetAuthorString( void )
{
  return( ( char * )&Copyright[ 6 ] ) ;
}
