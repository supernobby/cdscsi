/*
 * CommandTranslation.h
 */

#ifndef _COMMANDTRANSLATION_H
#define _COMMANDTRANSLATION_H


#include <exec/types.h>


/*
 * some forward declarations to avoid recursive inludes
 */
struct CdScsiIOReq ;


/*
 * module functions
 */
LONG TranslateCdToScsi( struct CdScsiIOReq *MyCdScsiIOReq ) ;
LONG TranslateScsiToCd( struct CdScsiIOReq *MyCdScsiIOReq ) ;


#endif   /* _COMMANDTRANSLATION_H */
