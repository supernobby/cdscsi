/*
** DeviceUnit.c
*/


#include "DeviceUnit.h"
#include "DeviceCore.h"
#include "ScsiCommands.h"
#include "Settings.h"
#include "LogOutput.h"
#include "CommandTranslation.h"
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/timer.h>
#include <clib/alib_protos.h>
#include <dos/dosextens.h>
#include <dos/dostags.h>
#include <exec/errors.h>
#include <devices/scsidisk.h>
#include <devices/cd.h>
#include <exec/nodes.h>


/*
 * helper to trun command code into string
 */
STRPTR IOCommandToString( ULONG IOCommand )
{
  STRPTR CommandString ;
  
  switch( IOCommand )
  {
    case CD_RESET:  /* 1 */
      CommandString = "CD_RESET" ;
      break ;
    case CD_READ:  /* 2 */
      CommandString = "CD_READ" ;
      break ;
    case CD_WRITE:  /* 3 */
      CommandString = "CD_WRITE" ;
      break ;
    case CD_UPDATE:  /* 4 */
      CommandString = "CD_UPDATE" ;
      break ;
    case CD_CLEAR:  /* 5 */
      CommandString = "CD_CLEAR" ;
      break ;
    case CD_STOP:  /* 6 */
      CommandString = "CD_STOP" ;
      break ;
    case CD_START:  /* 7 */
      CommandString = "CD_START" ;
      break ;
    case CD_FLUSH:  /* 8 */
      CommandString = "CD_FLUSH" ;
      break ;
    case CD_MOTOR:  /* 9 */
      CommandString = "CD_MOTOR" ;
      break ;
    case CD_SEEK:  /* 10 */
      CommandString = "CD_SEEK" ;
      break ;
    case CD_FORMAT:  /* 11 */
      CommandString = "CD_FORMAT" ;
      break ;
    case CD_REMOVE:  /* 12 */
      CommandString = "CD_REMOVE" ;
      break ;
    case CD_CHANGENUM:  /* 13 */
      CommandString = "CD_CHANGENUM" ;
      break ;
    case CD_CHANGESTATE:  /* 14 */
      CommandString = "CD_CHANGESTATE" ;
      break ;
    case CD_PROTSTATUS:  /* 15 */
      CommandString = "CD_PROTSTATUS" ;
      break ;
    case CD_GETDRIVETYPE:  /* 18 */
      CommandString = "CD_GETDRIVETYPE" ;
      break ;
    case CD_GETNUMTRACKS:  /* 19 */
      CommandString = "CD_GETNUMTRACKS" ;
      break ;
    case CD_ADDCHANGEINT:  /* 20 */
      CommandString = "CD_ADDCHANGEINT" ;
      break ;
    case CD_REMCHANGEINT:  /* 21 */
      CommandString = "CD_REMCHANGEINT" ;
      break ;
    case CD_GETGEOMETRY:  /* 22 */
      CommandString = "CD_GETGEOMETRY" ;
      break ;
    case CD_EJECT:  /* 23 */
      CommandString = "CD_EJECT" ;
      break ;
    case HD_SCSICMD:
      CommandString = "HD_SCSICMD" ;
      break ;
    case CD_INFO:  /* 32 */
      CommandString = "CD_INFO" ;
      break ;
    case CD_CONFIG:  /* 33 */
      CommandString = "CD_CONFIG" ;
      break ;
    case CD_TOCMSF:  /* 34 */
      CommandString = "CD_TOCMSF" ;
      break ;
    case CD_TOCLSN:  /* 35 */
      CommandString = "CD_TOCLSN" ;
      break ;
    case CD_READXL:  /* 36 */
      CommandString = "CD_READXL" ;
      break ;
    case CD_PLAYTRACK:  /* 37 */
      CommandString = "CD_PLAYTRACK" ;
      break ;
    case CD_PLAYMSF:  /* 38 */
      CommandString = "CD_PLAYMSF" ;
      break ;
    case CD_PLAYLSN:  /* 39 */
      CommandString = "CD_PLAYLSN" ;
      break ;
    case CD_PAUSE:  /* 40 */
      CommandString = "CD_PAUSE" ;
      break ;
    case CD_SEARCH:  /* 41 */
      CommandString = "CD_SEARCH" ;
      break ;
    case CD_QCODEMSF:  /* 42 */
      CommandString = "CD_QCODEMSF" ;
      break ;
    case CD_QCODELSN:  /* 43 */
      CommandString = "CD_QCODELSN" ;
      break ;
    case CD_ATTENUATE:  /* 44 */
      CommandString = "CD_ATTENUATE" ;
      break ;
    case CD_ADDFRAMEINT:  /* 45 */
      CommandString = "CD_ADDFRAMEINT" ;
      break ;
    case CD_REMFRAMEINT:  /* 46 */
      CommandString = "CD_REMFRAMEINT" ;
      break ;
    default:
      CommandString = "unknown" ;
      break ;
  }

  return( CommandString ) ;
}


/*
 * wait for the medium to be ready
 */
void WaitForCDReady( struct DeviceUnit *MyDeviceUnit, ULONG Seconds )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  struct Device *TimerBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  struct timerequest PollTimeRequest ;
  struct timeval PollInterval, PollTimeVal ;
  ULONG Count ;

  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  TimerBase = MyDeviceUnit->du_TimerBase ;
  MyCdScsiIOReq = MyDeviceUnit->du_QuickCdScsiIOReq ;
  PollInterval.tv_secs = 1 ;
  PollInterval.tv_micro = 0 ;
  CopyMem( MyDeviceUnit->du_PollTimeRequest, &PollTimeRequest, sizeof( struct timerequest ) ) ;
  GetSysTime( &PollTimeVal ) ;
  Count = Seconds ;

  do
  {
    ScsiTestUnitReadyCommand( MyCdScsiIOReq ) ;
    DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
    if( SCSI_STATUS_GOOD != MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
    {  /* unit is not (yet) ready */
      LOG_TEXT( ( _DEBUG_LEVEL, "still waiting for the medium: %d", Count ) ) ;
      AddTime( &PollTimeVal, &PollInterval ) ;
      PollTimeRequest.tr_time = PollTimeVal ;
      PollTimeRequest.tr_node.io_Command = TR_ADDREQUEST ;
      SendIO( ( struct IORequest * )&PollTimeRequest ) ;
      WaitIO( ( struct IORequest * )&PollTimeRequest ) ;
      Count-- ;
    }
    else
    {  /* unit is ready */
      break ;
    }
  } while( ( Count ) ) ;
}


/*
 * return the cd io request to sender after scsi device finished with it
 */
static void ReplyCdIORequest( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct ExecBase *SysBase ;
  struct IOStdReq *MyCdIOStdReq ;

  /* get the cd io request */
  MyCdIOStdReq = MyCdScsiIOReq->csi_CdIOStdReq ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;

  if( MyCdScsiIOReq->csi_ThreadMessage )
  {  /* io request was started with quick io */
    LOG_TEXT( ( _DEBUG_LEVEL, "cd device finished quick io request %s(0x%02X) with error: %ld", IOCommandToString( MyCdIOStdReq->io_Command ), MyCdIOStdReq->io_Command,MyCdIOStdReq->io_Error ) ) ;
    MyCdIOStdReq->io_Message.mn_Node.ln_Type = NT_REPLYMSG ;
    ReplyMsg( ( struct Message *)MyCdScsiIOReq->csi_ThreadMessage ) ;
    MyCdScsiIOReq->csi_ThreadMessage = NULL ;
  }
  else
  {  /* io request was started with async io */
    LOG_TEXT( ( _DEBUG_LEVEL, "cd device finished async io request %s(0x%02X) with error: %ld", IOCommandToString( MyCdIOStdReq->io_Command ), MyCdIOStdReq->io_Command, MyCdIOStdReq->io_Error ) ) ;
    ReplyMsg( ( struct Message * )MyCdIOStdReq ) ;
    /* add this back to the free cd scsi io requests list */
    AddTail( &MyDeviceUnit->du_FreeIOReqList, ( struct Node * )MyCdScsiIOReq ) ;
  }
  MyCdScsiIOReq->csi_CdIOStdReq = NULL ;
}


/*
 * 
 */
static void BeginAsyncCdScsiIORequest( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct ExecBase *SysBase ;
  struct IOStdReq *MyCdIOStdReq ;
  struct SCSICmd *MySCSICmd ;
  LONG ScsiDeviceNext ;

  MyCdIOStdReq = MyCdScsiIOReq->csi_CdIOStdReq ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;

  /* clear error if sender did not */
  do
  {
    MyCdIOStdReq->io_Error = 0 ;
    ScsiDeviceNext = TranslateCdToScsi( MyCdScsiIOReq ) ;
    if( ScsiDeviceNext )
    {  /* cd io request needs to be send to scsi device */
      MySCSICmd = ( struct SCSICmd * )MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data ;
      LOG_TEXT( ( _DEBUG_LEVEL, "sending async io request to scsi device: %s(0x%02X)", ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? ScsiCommandToString( MySCSICmd->scsi_Command[ 0 ] ) : IOCommandToString( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? MySCSICmd->scsi_Command[ 0 ] : MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ) ;
#if 0
      if( CD_READ == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command )
      {
        LOG_TEXT( ( _DEBUG_LEVEL, "io length: %ld, io offset: %ld", MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length, MyCdScsiIOReq->csi_ScsiIOStdReq.io_Offset ) ) ;
      }
#endif
      MyCdScsiIOReq->csi_ScsiIOStdReq.io_Message.mn_Node.ln_Type = NT_MESSAGE ;
      SendIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      //LOG_TEXT( ( _DEBUG_LEVEL, "async io request sent" ) ) ;
      break ;
    }
  } while( MyCdScsiIOReq->csi_State && !( MyCdIOStdReq->io_Error ) ) ;
  if( !( ScsiDeviceNext ) )
  {  /* cd io request does not need to be send to scsi device  */
    ReplyCdIORequest( MyCdScsiIOReq ) ;
  }
}


/*
 * 
 */
static void FinishAsyncCdScsiIORequest( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct ExecBase *SysBase ;
  struct IOStdReq *MyCdIOStdReq ;
  LONG Finished ;

  /* get the cd io request */
  MyCdIOStdReq = MyCdScsiIOReq->csi_CdIOStdReq ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;

  Finished = TranslateScsiToCd( MyCdScsiIOReq ) ;

  if( Finished )
  {
    if( MyCdScsiIOReq->csi_State )
    {  /* cd scsi io request has actions left */
      BeginAsyncCdScsiIORequest( MyCdScsiIOReq ) ;
    }
    else
    {  /* reply the io request now */
      ReplyCdIORequest( MyCdScsiIOReq ) ;
    }
  }
}


/*
 * send the io request from the cd device to the scsi device
 */
static void HandleAsyncCdIORequest( struct IOStdReq *MyCdIOStdReq )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct ExecBase *SysBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  
  LOG_TEXT( ( _DEBUG_LEVEL, "cd device got async io request: %s(%ld)", IOCommandToString( MyCdIOStdReq->io_Command ), MyCdIOStdReq->io_Command ) ) ;
  
  /* get a scsi io request from the free io request list */
  MyCdScsiIOReq = ( struct CdScsiIOReq * )RemHead( &MyDeviceUnit->du_FreeIOReqList ) ;  
  if( NULL != MyCdScsiIOReq )
  {  /* did get a free scsi io request */
    MyCdScsiIOReq->csi_CdIOStdReq = MyCdIOStdReq ;
    MyCdScsiIOReq->csi_State = 0 ;
    BeginAsyncCdScsiIORequest( MyCdScsiIOReq ) ;
  }
  else
  {  /* did not get a free scsi io request */
    LOG_TEXT( ( _WARNING_LEVEL, "did not get a free scsi io request" ) ) ;
    MyCdIOStdReq->io_Error = IOERR_UNITBUSY ;
    /* mark this io request as complete */
    ReplyMsg( ( struct Message * )MyCdIOStdReq ) ;
  }
}


/*
 * send the quick io request from the cd.device to the scsi.device
 */
static void HandleQuickCdIORequest( struct ThreadMessage *MyThreadMessage )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct ExecBase *SysBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  struct IOStdReq *MyCdIOStdReq ;
  struct SCSICmd *MySCSICmd ;
  LONG ScsiDeviceNext ;
  ULONG Finished ;
  
  MyCdIOStdReq = ( struct IOStdReq * )MyThreadMessage->tm_Data ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  
  LOG_TEXT( ( _DEBUG_LEVEL, "cd device got quick io request with: %s(%ld)", IOCommandToString( MyCdIOStdReq->io_Command ), MyCdIOStdReq->io_Command ) ) ;

  MyCdScsiIOReq = MyDeviceUnit->du_QuickCdScsiIOReq ;
  Finished = 1 ;
  MyCdScsiIOReq->csi_ThreadMessage = MyThreadMessage ;
#if 0
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq: 0x%08X", MyCdIOStdReq ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Message.mn_ReplyPort: 0x%08X", MyCdIOStdReq->io_Message.mn_ReplyPort ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Message.mn_Lenght: %ld", MyCdIOStdReq->io_Message.mn_Length ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Device: 0x%08X", MyCdIOStdReq->io_Device ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Unit: 0x%08X", MyCdIOStdReq->io_Unit ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Command: %ld", MyCdIOStdReq->io_Command ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Flags: 0x%08X", MyCdIOStdReq->io_Flags ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Error: %ld", MyCdIOStdReq->io_Error ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Actual: %ld", MyCdIOStdReq->io_Actual ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Length: %ld", MyCdIOStdReq->io_Length ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Data: 0x%08X", MyCdIOStdReq->io_Data ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Offset: %ld", MyCdIOStdReq->io_Offset ) ) ;
#endif
  MyCdScsiIOReq->csi_CdIOStdReq = MyCdIOStdReq ;
  MyCdScsiIOReq->csi_State = 0 ;
  /* clear error if sender did not */
  MyCdIOStdReq->io_Error = 0 ;
  do
  {
    ScsiDeviceNext = TranslateCdToScsi( MyCdScsiIOReq ) ;
    if( ScsiDeviceNext )
    {  /* command needs to be executed by scsi device */
#if 0
        LOG_TEXT( ( _DEBUG_LEVEL, "DoIO request to scsi device with: %s(%ld)", IOCommandToString( MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq: 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Message.mn_ReplyPort: 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Message.mn_ReplyPort ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Message.mn_Lenght: %ld", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Message.mn_Length ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Device: 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Device ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Unit: 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Unit ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Command: %ld", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Flags: 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Flags ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Error: %ld", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Error ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Actual: %ld", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Actual ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Length: %ld", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Length ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Data: 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Data ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "IOStdReq->io_Offset: %ld", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Offset ) ) ;
        
#endif
      MySCSICmd = ( struct SCSICmd * )MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data ;
      LOG_TEXT( ( _DEBUG_LEVEL, "sending quick io request to scsi device: %s(0x%02X)", ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? ScsiCommandToString( MySCSICmd->scsi_Command[ 0 ] ) : IOCommandToString( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? MySCSICmd->scsi_Command[ 0 ] : MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ) ;
#if 0
      if( CD_READ == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command )
      {
        LOG_TEXT( ( _DEBUG_LEVEL, "io length: %ld, io offset: %ld", MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length, MyCdScsiIOReq->csi_ScsiIOStdReq.io_Offset ) ) ;
      }
#endif
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      if( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error )
      {
        LOG_TEXT( ( _ERROR_LEVEL, "scsi device finished quick io request %s(0x%02X) with error: %ld", ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? ScsiCommandToString( MySCSICmd->scsi_Command[ 0 ] ) : IOCommandToString( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? MySCSICmd->scsi_Command[ 0 ] : MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command, MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error ) ) ;
      }
      else
      {
        LOG_TEXT( ( _DEBUG_LEVEL, "scsi device finished quick io request %s(0x%02X) with error: %ld", ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? ScsiCommandToString( MySCSICmd->scsi_Command[ 0 ] ) : IOCommandToString( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? MySCSICmd->scsi_Command[ 0 ] : MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command, MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error ) ) ;
      }
      Finished = TranslateScsiToCd( MyCdScsiIOReq ) ;        
    }
  } while( MyCdScsiIOReq->csi_State && !( MyCdIOStdReq->io_Error ) ) ;
  if( !( Finished ) )
  {
    MyCdIOStdReq->io_Flags &= ~( IOF_QUICK ) ;
    // TODO: copy and park the request
    //MyCdScsiIOReq = ( struct CdScsiIOReq * )RemHead( &MyDeviceUnit->du_FreeIOReqList ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "quick io request not finished!!!" ) ) ;
  }
  else
  {
  }
  ReplyCdIORequest( MyCdScsiIOReq ) ;
}


/*
 * abort current play operation
 */
static void StopPlay( struct DeviceUnit *MyDeviceUnit )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  ULONG Count ;

  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  MyCdScsiIOReq = MyDeviceUnit->du_QuickCdScsiIOReq ;

  switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
  {
    case 0:  /* cd-rom output */
      ScsiStartStopUnitCommand( MyCdScsiIOReq, 0, 0 ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      ScsiStartStopUnitCommand( MyCdScsiIOReq, 0, 1 ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      break ;
    case 1:  /* ahi output */
      for( Count = 0 ; Count < NUM_AHI_BUFFERS ; Count++ )
      {
        if( NULL == CheckIO( ( struct IORequest * )&MyDeviceUnit->du_AHIBuffer[ Count ].ab_AHIRequest ) )
        {  /* ask scsi device to abort the io request */
          LOG_TEXT( ( _DEBUG_LEVEL, "ask ahi device to abort the io request" ) ) ;
          AbortIO( ( struct IORequest * )&MyDeviceUnit->du_AHIBuffer[ Count ].ab_AHIRequest ) ;
        }
      }
      break ;
  }
}



/*
 * 
 */
void AbortCdIORequest( struct IOStdReq *MyCdIOStdReq )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct ExecBase *SysBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  ULONG Count ;
  
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;

  for( Count = 0 ; Count < NUM_CDSCSI_REQUESTS ; Count++ )
  {  /* look for the io request */
    if( MyCdIOStdReq == MyDeviceUnit->du_CdScsiIOReq[ Count ].csi_CdIOStdReq )
    {  /* cd io request to be aborted found */
      MyCdScsiIOReq = &MyDeviceUnit->du_CdScsiIOReq[ Count ] ;
      LOG_TEXT( ( _INFO_LEVEL, "aborting io request %s(0x%02X)", ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? ScsiCommandToString( MyCdScsiIOReq->csi_CommandBuffer[ 0 ] ) : IOCommandToString( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? MyCdScsiIOReq->csi_CommandBuffer[ 0 ] : MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ) ;
      if( ( MyDeviceUnit->du_PlayCdScsiIOReq ) && ( MyDeviceUnit->du_PlayCdScsiIOReq == MyCdScsiIOReq ) )
      {  /* aborting the current play io request */
        LOG_TEXT( ( _DEBUG_LEVEL, "aborting play io request" ) ) ;
        StopPlay( MyDeviceUnit ) ;
        MyDeviceUnit->du_AbortPlay = 1 ;
      }  
      if( NULL == CheckIO( ( struct IORequest * )&MyCdScsiIOReq->csi_ScsiIOStdReq ) )
      {  /* ask scsi device to abort the io request */
        LOG_TEXT( ( _DEBUG_LEVEL, "ask scsi device to abort the io request" ) ) ;
        AbortIO( ( struct IORequest * )&MyCdScsiIOReq->csi_ScsiIOStdReq ) ;
      }

      break ;
    }
  }
}


/*
 * fade to volume to the target value
 */
void UpdateVolume( struct DeviceUnit *MyDeviceUnit )
{
  struct ExecBase *SysBase ;
  struct DeviceCore *MyDeviceCore ;
  struct Device *TimerBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  struct timeval MyTimeVal ;
  struct ModeParameterHeader06 *MyModeParameterHeader06 ;
  struct ModeParameterBlockDescriptor *MyModeParameterBlockDescriptor ;
  struct CdromAudioControlParametersPage *MyCdromAudioControlParametersPage ;
  
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  TimerBase = MyDeviceUnit->du_TimerBase ;
  MyCdScsiIOReq = MyDeviceUnit->du_QuickCdScsiIOReq ;
  
  if( MyDeviceUnit->du_CurrentVolume != MyDeviceUnit->du_TargetVolume )
  {
    MyDeviceUnit->du_CurrentVolume += ( ( MyDeviceUnit->du_TargetVolume - MyDeviceUnit->du_CurrentVolume ) / MyDeviceUnit->du_VolumeFadeTime ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "current volume: %d (%d)", MyDeviceUnit->du_CurrentVolume, MyDeviceUnit->du_TargetVolume ) ) ;
    /* set new current volume */
    switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
    {
      case 0:  /* cd-rom output */
        ScsiModeSense06Command( MyCdScsiIOReq, 0, 0x0E ) ;
        DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
        //LOG_TEXT( ( _DEBUG_LEVEL, "reading cd-rom volume status: %d", MyCdScsiIOReq->csi_SCSICmd.scsi_Status ) ) ;
        if( SCSI_STATUS_GOOD == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
        {  /* scsi command result ok */
          MyModeParameterHeader06 = ( struct ModeParameterHeader06 * )MyCdScsiIOReq->csi_DataBuffer ;
          MyModeParameterBlockDescriptor = ( struct ModeParameterBlockDescriptor * )( MyModeParameterHeader06 + 1 ) ;
          MyCdromAudioControlParametersPage = ( struct CdromAudioControlParametersPage * ) ( ( MyModeParameterBlockDescriptor ) + 
            ( MyModeParameterHeader06->BlockDescriptorLength / sizeof ( struct ModeParameterBlockDescriptor ) ) ) ;
          MyCdromAudioControlParametersPage->OutputPort0Volume = MyCdromAudioControlParametersPage->OutputPort1Volume =
            255 * MyDeviceUnit->du_CurrentVolume / MyDeviceUnit->du_CDInfo.AudioPrecision ;
          ScsiModeSelect06Command( MyCdScsiIOReq, MyCdScsiIOReq->csi_SCSICmd.scsi_Actual ) ;
          DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
          //LOG_TEXT( ( _DEBUG_LEVEL, "writing cd-rom volume status: %d", MyCdScsiIOReq->csi_SCSICmd.scsi_Status ) ) ;
        }
        break ;
      case 1:  /* ahi output */
        break ;
    }
    if( MyDeviceUnit->du_CurrentVolume != MyDeviceUnit->du_TargetVolume )
    {  /* target volume still not reached */
      MyDeviceUnit->du_VolumeFadeTime-- ;
      MyTimeVal.tv_secs = 0 ;
      MyTimeVal.tv_micro = 1000000UL / MyDeviceUnit->du_CDInfo.PlaySpeed ;
      AddTime( &MyDeviceUnit->du_VolumeTimeVal, &MyTimeVal ) ;
      MyDeviceUnit->du_VolumeTimeRequest.tr_time = MyDeviceUnit->du_VolumeTimeVal ;
      MyDeviceUnit->du_VolumeTimeRequest.tr_node.io_Command = TR_ADDREQUEST ;
      SendIO( ( struct IORequest * )&MyDeviceUnit->du_VolumeTimeRequest ) ;
    }
  }
}



/*
ScsiReadCdromCapacityCommand result:

LogicalBlockAddress: 269538
BlockLength: 2048
q

track 11:

cd device got async io request: CD_PLAYLSN(39)
play lsn, offset: 194427, length: 1432


track 25:

cd device got async io request: CD_PLAYLSN(39)
play lsn, offset: 253035, length: 16504
 */


/*
 * scan known devices and set config to the first found drive
 */
void FindCdDrive( struct DeviceUnit *MyDeviceUnit )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  STRPTR DeviceName ;
  ULONG UnitNumber ;
  ULONG Count ;
  STRPTR KnownDevices[] =
  {
    "usbscsi.device",
    "scsi.device",
    "2nd.scsi.device",
    "cybscsi.device",
    "cybppc.device",
    "uaescsi.device",
    NULL
  } ;

  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  MyCdScsiIOReq = MyDeviceUnit->du_QuickCdScsiIOReq ;
  Count = 0 ;

  DeviceName = ( STRPTR )( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, ScsiDevice ) ) ;
  UnitNumber = ( ULONG )GetSetting( MyDeviceCore->dc_Settings, ScsiUnit ) ;
  
  do
  {
    OpenDevice( DeviceName, UnitNumber, ( struct IORequest * )MyCdScsiIOReq, 0 ) ;
    if( ( !( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error ) ) && ( NULL != MyCdScsiIOReq->csi_ScsiIOStdReq.io_Device ) )
    {  /* device ok */
      ScsiInquiryCommand( MyCdScsiIOReq ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      CloseDevice( ( struct IORequest * )MyCdScsiIOReq ) ;
      MyCdScsiIOReq->csi_ScsiIOStdReq.io_Device = NULL ;
      MyCdScsiIOReq->csi_ScsiIOStdReq.io_Unit = NULL ;
      if( SCSI_STATUS_GOOD == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
      {  /* inquiry command result ok */
        if( 0x05 == SCSI_INQUIRY_DATA_PERIPHERAL_DEVICE_TYPE( MyCdScsiIOReq->csi_DataBuffer ) )
        {
          LOG_TEXT( ( _INFO_LEVEL, "using cd drive at: %s/%d", DeviceName, UnitNumber ) ) ;
          SetSetting( MyDeviceCore->dc_Settings, ScsiDevice, ( LONG )DeviceName ) ;
          SetSetting( MyDeviceCore->dc_Settings, ScsiUnit, ( LONG )UnitNumber ) ;
          break ; ;
        }
      }
    }
    UnitNumber++ ;
    if( 8 <= UnitNumber )
    {
      UnitNumber = 0 ;
      DeviceName = KnownDevices[ Count ] ;
      Count++ ;
    }
  } while( DeviceName ) ;
}


/*
 * check, if the unit is a CD-ROM drive
 */
static LONG QueryCdDrive( struct DeviceUnit *MyDeviceUnit )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  LONG Error ;
  UBYTE *MyData ;
  struct ModeParameterHeader06 *MyModeParameterHeader06 ;
  struct ModeParameterBlockDescriptor *MyModeParameterBlockDescriptor ;
  struct CdromAudioControlParametersPage *MyCdromAudioControlParametersPage ;
  ULONG LogicalBlockAddress, BlockLength ;
  
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  MyCdScsiIOReq = MyDeviceUnit->du_QuickCdScsiIOReq ;
  Error = 1 ;
  
  ScsiInquiryCommand( MyCdScsiIOReq ) ;
  DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
  if( SCSI_STATUS_GOOD == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
  {  /* inquiry command result ok */
    LOG_TEXT( ( _DEBUG_LEVEL, "Peripheral Device Type: 0x%0X", SCSI_INQUIRY_DATA_PERIPHERAL_DEVICE_TYPE( MyCdScsiIOReq->csi_DataBuffer ) ) ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "Vendor Indentification: %s", SCSI_INQUIRY_DATA_VENDOR_IDENTIFICATION( MyCdScsiIOReq->csi_DataBuffer ) ) ) ;
    if( 1 )  
    //if( 0x05 == SCSI_INQUIRY_DATA_PERIPHERAL_DEVICE_TYPE( MyCdScsiIOReq->csi_DataBuffer ) )  
    {  /* it is a CD-ROM */
      MyDeviceUnit->du_CDInfo.PlaySpeed = 75 ;
      MyDeviceUnit->du_CDInfo.ReadSpeed = 75 ;
      MyDeviceUnit->du_CDInfo.ReadXLSpeed = 75 ;
      MyDeviceUnit->du_CDInfo.SectorSize = 2048 ;
      MyDeviceUnit->du_CDInfo.XLECC = 0 ;
      MyDeviceUnit->du_CDInfo.EjectReset = 0 ;
      MyDeviceUnit->du_CDInfo.MaxSpeed = 150 ;
      MyDeviceUnit->du_CDInfo.AudioPrecision = 0x7FFF ;
      MyDeviceUnit->du_CurrentVolume = MyDeviceUnit->du_TargetVolume = MyDeviceUnit->du_CDInfo.AudioPrecision ;
      MyDeviceUnit->du_CDInfo.Status = 0 ;
    
      Error = 0 ;
      
      ScsiModeSense06Command( MyCdScsiIOReq, 0 /* current values */, 0x0E /* CD-ROM audio control parameters page */ ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      if( SCSI_STATUS_GOOD == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
      {  /* scsi command result ok */
        MyModeParameterHeader06 = ( struct ModeParameterHeader06 * )MyCdScsiIOReq->csi_DataBuffer ;
        MyModeParameterBlockDescriptor = ( struct ModeParameterBlockDescriptor * )( MyModeParameterHeader06 + 1 ) ;
        MyCdromAudioControlParametersPage = ( struct CdromAudioControlParametersPage * ) ( ( MyModeParameterBlockDescriptor ) + ( MyModeParameterHeader06->BlockDescriptorLength / sizeof ( struct ModeParameterBlockDescriptor ) ) ) ;
        MyDeviceUnit->du_CDInfo.PlaySpeed = ( MyCdromAudioControlParametersPage->LogicalBlocksPerSecond1 << 8 ) | ( MyCdromAudioControlParametersPage->LogicalBlocksPerSecond0 << 0 ) ;
        MyDeviceUnit->du_CDInfo.SectorSize = ( MyModeParameterBlockDescriptor->BlockLength2 << 16 ) + ( MyModeParameterBlockDescriptor->BlockLength1 << 8 ) + ( MyModeParameterBlockDescriptor->BlockLength0 << 0 ) ;

        LOG_TEXT( ( _DEBUG_LEVEL, "Density code: %d", MyModeParameterBlockDescriptor->DensityCode ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Number of blocks: %d", ( MyModeParameterBlockDescriptor->NumberOfBlocks2 << 16 ) + ( MyModeParameterBlockDescriptor->NumberOfBlocks1 << 8 ) + ( MyModeParameterBlockDescriptor->NumberOfBlocks0 << 0 ) ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Block length: %d", ( MyModeParameterBlockDescriptor->BlockLength2 << 16 ) + ( MyModeParameterBlockDescriptor->BlockLength1 << 8 ) + ( MyModeParameterBlockDescriptor->BlockLength0 << 0 ) ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "PS: %d", MyCdromAudioControlParametersPage->PS ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Page Code: %d", MyCdromAudioControlParametersPage->PageCode ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Parameter Length: %d", MyCdromAudioControlParametersPage->ParameterLength ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Immed: %d", MyCdromAudioControlParametersPage->Immed ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "SOTC: %d", MyCdromAudioControlParametersPage->SOTC ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Logical Blocks Per Second: %d", ( MyCdromAudioControlParametersPage->LogicalBlocksPerSecond1 << 8 ) | ( MyCdromAudioControlParametersPage->LogicalBlocksPerSecond0 << 0 ) ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 0 Channel Selection: %d", MyCdromAudioControlParametersPage->OutputPort0ChannelSelection ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 0 Volume: %d", MyCdromAudioControlParametersPage->OutputPort0Volume ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 1 Channel Selection: %d", MyCdromAudioControlParametersPage->OutputPort1ChannelSelection ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 1 Volume: %d", MyCdromAudioControlParametersPage->OutputPort1Volume ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 2 Channel Selection: %d", MyCdromAudioControlParametersPage->OutputPort2ChannelSelection ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 2 Volume: %d", MyCdromAudioControlParametersPage->OutputPort2Volume ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 3 Channel Selection: %d", MyCdromAudioControlParametersPage->OutputPort3ChannelSelection ) )  ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Output Port 3 Volume: %d", MyCdromAudioControlParametersPage->OutputPort3Volume ) )  ;
        //MyDeviceUnit->du_TargetVolume = MyDeviceUnit->du_CurrentVolume = MyDeviceUnit->du_CDInfo.AudioPrecision * MyCdromAudioControlParametersPage->OutputPort0ChannelSelection / 0xFF ;
      }

      ScsiReadCdromCapacityCommand( MyCdScsiIOReq ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      if( SCSI_STATUS_GOOD == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
      {  /* scsi command result ok */
        MyData = ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer ;
        LogicalBlockAddress = ( MyData[ 0 ] << 24 ) + ( MyData[ 1 ] << 16 ) + ( MyData[ 2 ] << 8 ) + ( MyData[ 3 ] << 0 ) ;
        BlockLength = ( MyData[ 4 ] << 24 ) + ( MyData[ 5 ] << 16 ) + ( MyData[ 6 ] << 8 ) + ( MyData[ 7 ] << 0 ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "LogicalBlockAddress: %d", LogicalBlockAddress ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "BlockLength: %d", BlockLength ) ) ;
      }
      else
      {
        PrintSenseData( MyCdScsiIOReq ) ;
      }

      ScsiStartStopUnitCommand( MyCdScsiIOReq, 0, 0 ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      ScsiStartStopUnitCommand( MyCdScsiIOReq, 0, 1 ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
    }
    else
    {  /* it is no CD-ROM */

    }
  }
  else
  {  /* inquiry command result not ok */
    
  }

  return( Error ) ;
}


/*
 * get as much information as possible from the unit
 */
static void QueryCdMedium( struct DeviceUnit *MyDeviceUnit )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  UBYTE *MyData ;
  LONG Error ;

  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  MyCdScsiIOReq = MyDeviceUnit->du_QuickCdScsiIOReq ;
  
  MyData = ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer ;
  
  ScsiTestUnitReadyCommand( MyCdScsiIOReq ) ;
  DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;

  if( SCSI_STATUS_GOOD == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
  {  /* test unit ready completed ok */
    if( !( CDSTSF_DISK & MyDeviceUnit->du_CDInfo.Status ) )
    {  /* cd medium was inserted */
      LOG_TEXT( ( _INFO_LEVEL, "cd medium was inserted" ) ) ;
      MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_SPIN | CDSTSF_DISK | CDSTSF_CLOSED ) ;
    }
    if( ( CDSTSF_DISK & MyDeviceUnit->du_CDInfo.Status ) && ( !( CDSTSF_TOC & MyDeviceUnit->du_CDInfo.Status ) ) )
    {  /* try to get toc */
      MyDeviceUnit->du_FakeCdIOStdReq.io_Data = &MyDeviceUnit->du_CDTOC ;
      MyDeviceUnit->du_FakeCdIOStdReq.io_Length = 100 ;
      MyCdScsiIOReq->csi_CdIOStdReq = &MyDeviceUnit->du_FakeCdIOStdReq ;
      ScsiReadTocCommand( MyCdScsiIOReq, 0 ) ;
      DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
      Error = ScsiReadTocResult( MyCdScsiIOReq ) ;
      if( !( Error ) )
      {  /* toc available */
        LOG_TEXT( ( _DEBUG_LEVEL, "toc available" ) ) ;
        MyDeviceUnit->du_CDInfo.Status |= CDSTSF_TOC ;
        if( ( 1 < MyDeviceUnit->du_FakeCdIOStdReq.io_Actual ) && ( CTL_DATA & MyDeviceUnit->du_CDTOC[ 1 ].Entry.CtlAdr ) )
        {  /* first track is data track */
          LOG_TEXT( ( _INFO_LEVEL, "first track is data track" ) ) ;
          MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_CDROM ) ;
        }
        else
        {
          LOG_TEXT( ( _INFO_LEVEL, "first track is audio track" ) ) ;
        }
      }
      else
      {  /* toc not available */
        LOG_TEXT( ( _WARNING_LEVEL, "toc not available" ) ) ;
      }
    }
    if( ( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status ) )
    {  /* try to get current play status */
      switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
      {
        case 0:  /* cd-rom output */
          MyDeviceUnit->du_FakeCdIOStdReq.io_Data = &MyDeviceUnit->du_CurrentQCode ;
          MyDeviceUnit->du_FakeCdIOStdReq.io_Length = 0 ;
          MyCdScsiIOReq->csi_CdIOStdReq = &MyDeviceUnit->du_FakeCdIOStdReq ;
          ScsiReadSubChannelCommand( MyCdScsiIOReq, 0 ) ;
          DoIO( ( struct IORequest * )MyCdScsiIOReq ) ;
          Error = ScsiReadSubChannelResult( MyCdScsiIOReq ) ;
          if( !( Error ) )
          {  /* completed ok */
            MyDeviceUnit->du_PlayCdScsiIOReq->csi_CdIOStdReq->io_Actual = SCSI_SUB_CHANNEL_DATA_ABSOLUTE_ADDRESS( MyData ) - MyDeviceUnit->du_PlayCdScsiIOReq->csi_CdIOStdReq->io_Offset ;
            switch( SCSI_SUB_CHANNEL_DATA_AUDIO_STATUS( MyData ) )
            {
              case 0x11:  /* Audio play operation in progress */
                MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_PLAYING ) ;
                break ;
              case 0x12:  /* Audio play operation paused */
                MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_PAUSED | CDSTSF_PLAYING ) ;
                break ;
              default:
                LOG_TEXT( ( _DEBUG_LEVEL, "currently not playing" ) ) ;
                MyDeviceUnit->du_CDInfo.Status &= ~( CDSTSF_DIRECTION | CDSTSF_SEARCH | CDSTSF_PAUSED | CDSTSF_PLAYING ) ;
                break ;
            }
          }
          else
          {
            LOG_TEXT( ( _DEBUG_LEVEL, "could not get current play status" ) ) ;
          }
          if( ( MyDeviceUnit->du_PlayCdScsiIOReq ) && ( !( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status ) ) )
          {  /* a caller is waiting for the play to finish */
            LOG_TEXT( ( _DEBUG_LEVEL, "current play command finished" ) ) ;
            if( MyDeviceUnit->du_PlayCdScsiIOReq->csi_CdIOStdReq->io_Length > MyDeviceUnit->du_PlayCdScsiIOReq->csi_CdIOStdReq->io_Actual )
            {  /* unexpected end of play, so set abort flag */
              MyDeviceUnit->du_AbortPlay = 1 ;
            }
            FinishAsyncCdScsiIORequest( MyDeviceUnit->du_PlayCdScsiIOReq ) ;
          }
          break ;
      }
    }
  }
  else
  {  /* test unit ready completed not ok */
    if( CDSTSF_DISK & MyDeviceUnit->du_CDInfo.Status )
    {  /* cd medium was removed */
      LOG_TEXT( ( _INFO_LEVEL, "cd medium was removed" ) ) ;
      MyDeviceUnit->du_CDInfo.Status = 0 ;
    }
  }
}


/*
 * dynamically close ahi device
 */
void CloseAHI( struct DeviceUnit *MyDeviceUnit )
{
  struct ExecBase *SysBase ;
  struct DeviceCore *MyDeviceCore ;

  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;

  if( NULL != MyDeviceUnit->du_QuickAHIRequest->ahir_Std.io_Device )
  {
    CloseDevice( ( struct IORequest * )MyDeviceUnit->du_QuickAHIRequest ) ;
  }
}


/*
 * dynamically open ahi device
 */
LONG OpenAHI( struct DeviceUnit *MyDeviceUnit )
{
  struct ExecBase *SysBase ;
  struct DeviceCore *MyDeviceCore ;
  ULONG Count ;
  LONG Error ;

  Error = 1 ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;

  MyDeviceUnit->du_QuickAHIRequest->ahir_Version = 4 ;
  OpenDevice( "ahi.device",
              ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AhiUnit ),
              ( struct IORequest * )MyDeviceUnit->du_QuickAHIRequest, 0 ) ;
  if( !( MyDeviceUnit->du_QuickAHIRequest->ahir_Std.io_Error ) )
  {
    LOG_TEXT( ( _DEBUG_LEVEL, "ahi device ok: 0x%08X", MyDeviceUnit->du_QuickAHIRequest->ahir_Std.io_Device ) ) ;
    NewList( &MyDeviceUnit->du_FreeAHIBufferList ) ;
    for( Count = 0 ; Count < NUM_AHI_BUFFERS ; Count++ )
    {  /* copy io request details and append to free list */
      CopyMem( MyDeviceUnit->du_QuickAHIRequest, &MyDeviceUnit->du_AHIBuffer[ Count ], sizeof( struct AHIRequest ) ) ;
      MyDeviceUnit->du_AHIBuffer[ Count ].ab_AHIRequest.ahir_Std.io_Message.mn_ReplyPort = MyDeviceUnit->du_AHIMsgPort ;
      LOG_TEXT( ( _DEBUG_LEVEL, "adding free ahi buffer to list: 0x%08X", &MyDeviceUnit->du_AHIBuffer[ Count ] ) ) ;
      AddTail( &MyDeviceUnit->du_FreeAHIBufferList, ( struct Node * )&MyDeviceUnit->du_AHIBuffer[ Count ] ) ;
    }
    Error = 0 ;
  }
  
  return( Error ) ;
}



/*
 * the device unit thread 
 */
void DeviceUnitThread( void )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct ExecBase *SysBase ;
  struct ThreadMessage *MyThreadMessage, *MyExitMessage ;
  struct Process *MyProcess ;
  ULONG SignalMask, ReceivedSignalBits ;
  ULONG CdMask, CoreMask, ScsiMask, TimerMask, AHIMask ;
  ULONG Shutdown, ShutdownRequest ;
  struct CdScsiIOReq *MyCdScsiIOReq ;
  struct IOStdReq *MyIOStdReq ;
  struct Device *TimerBase ;
  struct timerequest *MyTimeRequest ;
  ULONG Count ;
  LONG Error ;
  struct timeval CdMediumPollInterval ;
  struct AHIBuffer *MyAHIBuffer ;

  SysBase = *( struct ExecBase ** )4 ;  /* SysBase still unknown */
  MyProcess = ( struct Process * )FindTask( NULL ) ;
  WaitPort( &MyProcess->pr_MsgPort ) ;
  MyThreadMessage = ( struct ThreadMessage * )GetMsg( &MyProcess->pr_MsgPort ) ;
  MyExitMessage = MyThreadMessage ;
  MyDeviceUnit = ( struct DeviceUnit * )MyThreadMessage->tm_Data ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  if( THREAD_CMD_STARTUP == MyThreadMessage->tm_Command )
  {  /* core has send startup message */    
    LOG_TEXT( ( _DEBUG_LEVEL, "core has send startup message" ) ) ;
    /* initialize the io request list access semaphore */
    InitSemaphore( &MyDeviceUnit->du_Semaphore ) ;
    /* initialize the io request lists */
    NewList( &MyDeviceUnit->du_FreeIOReqList ) ;
    /* reply default to error by now */
    MyThreadMessage->tm_Data = 1 ;

    LoadSettings( MyDeviceCore->dc_Settings ) ;
    ConfigureLogOutput( MyDeviceCore->dc_LogOutput ) ;
    
    /* create the timer message port */
    MyDeviceUnit->du_TimerMsgPort = CreateMsgPort( ) ;
    if( NULL != MyDeviceUnit->du_TimerMsgPort )
    {  /* timer message port ok */
      LOG_TEXT( ( _DEBUG_LEVEL, "timer message port ok" ) ) ;
      /* create the timer io request */
      MyDeviceUnit->du_PollTimeRequest = ( struct timerequest * )CreateIORequest( MyDeviceUnit->du_TimerMsgPort , sizeof ( struct timerequest ) ) ;
      if( MyDeviceUnit->du_PollTimeRequest )
      {  /* timer io request ok */
        LOG_TEXT( ( _DEBUG_LEVEL, "timer io request ok" ) ) ;
        /* open the timer device */
        OpenDevice( TIMERNAME, UNIT_WAITUNTIL, ( struct IORequest * )MyDeviceUnit->du_PollTimeRequest, 0 ) ;
        if( 0 == MyDeviceUnit->du_PollTimeRequest->tr_node.io_Error )
        {  /* timer device ok */
          LOG_TEXT( ( _DEBUG_LEVEL, "timer device ok" ) ) ;
          /* store timer base to call functions of the timer device */
          MyDeviceUnit->du_TimerBase = TimerBase = MyDeviceUnit->du_PollTimeRequest->tr_node.io_Device ;
          CopyMem( MyDeviceUnit->du_PollTimeRequest, &MyDeviceUnit->du_ReadTimeRequest, sizeof( struct timerequest ) ) ;
          CopyMem( MyDeviceUnit->du_PollTimeRequest, &MyDeviceUnit->du_VolumeTimeRequest, sizeof( struct timerequest ) ) ;
          /* create the unit message port on which messages from the device core arrive */
          MyDeviceUnit->du_UnitMsgPort = CreateMsgPort( ) ;
          if( NULL != MyDeviceUnit->du_UnitMsgPort )
          {  /* unit message port ok */
            LOG_TEXT( ( _DEBUG_LEVEL, "unit message port ok" ) ) ;
            /* create the scsi message port to which the scsi device replies requests */
            MyDeviceUnit->du_QuickScsiMsgPort = CreateMsgPort( ) ;
            if( NULL != MyDeviceUnit->du_QuickScsiMsgPort )
            {  /* scsi message port ok */
              LOG_TEXT( ( _DEBUG_LEVEL, "quick scsi message port ok" ) ) ;
              /* create the quick cd scsi io request */
              MyDeviceUnit->du_QuickCdScsiIOReq = ( struct CdScsiIOReq * )CreateIORequest( MyDeviceUnit->du_QuickScsiMsgPort, sizeof( struct CdScsiIOReq ) ) ;
              if( NULL != MyDeviceUnit->du_QuickCdScsiIOReq )
              {  /* quick scsi io request ok */
                LOG_TEXT( ( _DEBUG_LEVEL, "quick scsi io request ok, 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq ) ) ;
                /* open the scsi device */
                FindCdDrive( MyDeviceUnit ) ;
                OpenDevice( ( STRPTR )( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, ScsiDevice ) ),
                            ( ULONG )GetSetting( MyDeviceCore->dc_Settings, ScsiUnit ),
                            ( struct IORequest * )MyDeviceUnit->du_QuickCdScsiIOReq, 0 ) ;
                if( ( !( MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Error ) ) && ( NULL != MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Device ) )
                {  /* scsi device ok */
                  LOG_TEXT( ( _DEBUG_LEVEL, "scsi device ok, io_Device: 0x%08X, io_Unit: 0x%08X", MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Device, MyDeviceUnit->du_QuickCdScsiIOReq->csi_ScsiIOStdReq.io_Unit ) ) ;
                  /* prepare the async io requests */
                  MyDeviceUnit->du_ScsiMsgPort = CreateMsgPort( ) ;
                  if( NULL != MyDeviceUnit->du_ScsiMsgPort )
                  {  /* async scsi message port ok */
                    for( Count = 0 ; Count < NUM_CDSCSI_REQUESTS ; Count++ )
                    {  /* copy io request details and append to free list */
                      CopyMem( MyDeviceUnit->du_QuickCdScsiIOReq, &MyDeviceUnit->du_CdScsiIOReq[ Count ], sizeof( struct CdScsiIOReq ) ) ;
                      MyDeviceUnit->du_CdScsiIOReq[ Count ].csi_ScsiIOStdReq.io_Message.mn_ReplyPort = MyDeviceUnit->du_ScsiMsgPort ;
                      AddTail( &MyDeviceUnit->du_FreeIOReqList, ( struct Node * )&MyDeviceUnit->du_CdScsiIOReq[ Count ] ) ;
                    }
                    MyDeviceUnit->du_FakeDeviceOpener.do_DeviceUnit = MyDeviceUnit ;
                    MyDeviceUnit->du_FakeCdIOStdReq.io_Unit = ( struct Unit * )&MyDeviceUnit->du_FakeDeviceOpener ;
                    MyDeviceUnit->du_QuickCdScsiIOReq->csi_CdIOStdReq = &MyDeviceUnit->du_FakeCdIOStdReq ;
                    Error = QueryCdDrive( MyDeviceUnit ) ;
                    if( !( Error ) )
                    {  /* it is a CD-ROM drive */
                      AHIMask = 0 ;
                      Error = 1 ;
                      switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
                      {
                        case 0:  /* CD-ROM audio */
                          Error = 0 ;
                          break ;
                        case 1:  /* AHI */
                          /* initialize the io request lists */
                          MyDeviceUnit->du_QuickAHIMsgPort = CreateMsgPort( ) ;
                          if( NULL != MyDeviceUnit->du_QuickAHIMsgPort )
                          {
                            LOG_TEXT( ( _DEBUG_LEVEL, "quick ahi message port ok: 0x%08X", MyDeviceUnit->du_QuickAHIMsgPort ) ) ;
                            MyDeviceUnit->du_QuickAHIRequest = ( struct AHIRequest * )CreateIORequest( MyDeviceUnit->du_QuickAHIMsgPort, sizeof( struct AHIRequest ) ) ;
                            if( NULL != MyDeviceUnit->du_QuickAHIRequest )
                            {
                              LOG_TEXT( ( _DEBUG_LEVEL, "quick ahi request ok: 0x%08X", MyDeviceUnit->du_QuickAHIRequest ) ) ;
                              MyDeviceUnit->du_AHIMsgPort = CreateMsgPort( ) ;
                              if( NULL != MyDeviceUnit->du_AHIMsgPort )
                              {
                                LOG_TEXT( ( _DEBUG_LEVEL, "async ahi message port ok: 0x%08X", MyDeviceUnit->du_AHIMsgPort ) ) ;
                                AHIMask = ( 1UL << MyDeviceUnit->du_AHIMsgPort->mp_SigBit ) ;
                                Error = 0 ;
                              }
                            }
                          }
                          break ;
                        default:
                          break ;
                      }
                      if( !( Error ) )
                      {  /* audio output ok */
                        /* get initial status */
                        QueryCdMedium( MyDeviceUnit ) ;
                        CdMediumPollInterval.tv_secs = 0 ;
                        CdMediumPollInterval.tv_micro = 1000000 ;
                        /* start the medium poll timer */
                        GetSysTime( &MyDeviceUnit->du_PollTimeVal ) ;
                        MyDeviceUnit->du_PollTimeRequest->tr_time = MyDeviceUnit->du_PollTimeVal ;
                        MyDeviceUnit->du_PollTimeRequest->tr_node.io_Command = TR_ADDREQUEST ;
                        SendIO( ( struct IORequest * )MyDeviceUnit->du_PollTimeRequest ) ;
                        /* initialize the units message port to receive cd io requests */
                        NewList( &MyDeviceUnit->du_Unit.unit_MsgPort.mp_MsgList ) ;
                        MyDeviceUnit->du_Unit.unit_MsgPort.mp_SigBit = AllocSignal( -1 ) ;
                        MyDeviceUnit->du_Unit.unit_MsgPort.mp_SigTask = ( struct Task * )MyDeviceUnit->du_Process ;
                        MyDeviceUnit->du_Unit.unit_MsgPort.mp_Node.ln_Type = NT_MSGPORT ;
                        MyDeviceUnit->du_Unit.unit_MsgPort.mp_Flags = PA_SIGNAL ;
                        /* trigger signal for the core message port where core commands arrive */
                        CoreMask = ( 1UL << MyDeviceUnit->du_UnitMsgPort->mp_SigBit ) ;
                        /* trigger signal for the scsi message port where replies from the scsi device arrive */
                        ScsiMask = ( 1UL << MyDeviceUnit->du_ScsiMsgPort->mp_SigBit ) ;
                        /* trigger signal for the units message port where cd io requets arrive */
                        CdMask = 1UL << MyDeviceUnit->du_Unit.unit_MsgPort.mp_SigBit ;
                        /* trigger signal for timer timeouts */
                        TimerMask = ( 1UL << MyDeviceUnit->du_TimerMsgPort->mp_SigBit ) ;
                        /* set thread message to tell the core, all ok */
                        MyThreadMessage->tm_Data = 0 ;
                        ReplyMsg( ( struct Message * )MyThreadMessage ) ;
                        SignalMask = CoreMask | ScsiMask | CdMask | TimerMask | AHIMask ;
                        Shutdown = ShutdownRequest = 0 ;
                        while( !( Shutdown ) )
                        {  /* device unit main loop */
                          ReceivedSignalBits = Wait( SignalMask ) ;
                          if( TimerMask & ReceivedSignalBits )
                          {  /* timer device timeout */
                            while( MyTimeRequest = ( struct timerequest * )GetMsg( MyDeviceUnit->du_TimerMsgPort ) )
                            {
                              if( MyDeviceUnit->du_PollTimeRequest == MyTimeRequest )
                              {
                                QueryCdMedium( MyDeviceUnit ) ;
                                //LOG_TEXT( ( _DEBUG_LEVEL, "medium poll status: 0x%02X", MyDeviceUnit->du_CDInfo.Status ) ) ;
                                AddTime( &MyDeviceUnit->du_PollTimeVal, &CdMediumPollInterval ) ;
                                MyDeviceUnit->du_PollTimeRequest->tr_time = MyDeviceUnit->du_PollTimeVal ;
                                SendIO( ( struct IORequest * )MyDeviceUnit->du_PollTimeRequest ) ;
                              }
                              else if( &MyDeviceUnit->du_ReadTimeRequest == MyTimeRequest )
                              {
                                FinishAsyncCdScsiIORequest( MyDeviceUnit->du_ReadCdScsiIOReq ) ;
                              }
                              else if( &MyDeviceUnit->du_VolumeTimeRequest == MyTimeRequest )
                              {
                                UpdateVolume( MyDeviceUnit ) ;
                              }
                            }
                          }
                          if( CdMask & ReceivedSignalBits )
                          {  /* cd device sent a request */
                            while( MyIOStdReq = ( struct IOStdReq * )GetMsg( &MyDeviceUnit->du_Unit.unit_MsgPort ) )
                            {
                              HandleAsyncCdIORequest( MyIOStdReq ) ;
                            }
                          }
                          if( CoreMask & ReceivedSignalBits )
                          {  /* message from core */
                            while( MyThreadMessage = ( struct ThreadMessage * )GetMsg( MyDeviceUnit->du_UnitMsgPort ) )
                            {
                              switch( MyThreadMessage->tm_Command )
                              {
                                case THREAD_CMD_SHUTDOWN:
                                  LOG_TEXT( ( _DEBUG_LEVEL, "received shutdown request" ) ) ;
                                  Shutdown = 1 ;
                                  MyExitMessage = MyThreadMessage ;
                                  break ;
                                case THREAD_CMD_DOIO:
                                  HandleQuickCdIORequest( MyThreadMessage ) ;
                                  break ;
                                case THREAD_CMD_ABORTIO:
                                  LOG_TEXT( ( _DEBUG_LEVEL, "received abort io request" ) ) ;
                                  AbortCdIORequest( ( struct IOStdReq * )MyThreadMessage->tm_Data ) ;
                                  ReplyMsg( ( struct Message * )MyThreadMessage ) ;
                                  break ;
                                default:
                                  break ;
                              }
                            }
                          }
                          if( ScsiMask & ReceivedSignalBits )
                          {  /* scsi device replyed a request */
                            while( MyCdScsiIOReq = ( struct CdScsiIOReq * )GetMsg( MyDeviceUnit->du_ScsiMsgPort ) )
                            {
                              if( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error )
                              {
                                LOG_TEXT( ( _ERROR_LEVEL, "scsi device finished async io request %s(0x%02X) with error: %ld", ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? ScsiCommandToString( MyCdScsiIOReq->csi_CommandBuffer[ 0 ] ) : IOCommandToString( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? MyCdScsiIOReq->csi_CommandBuffer[ 0 ] : MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command, MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error ) ) ;
                              }
                              else
                              {
                                LOG_TEXT( ( _DEBUG_LEVEL, "scsi device finished async io request %s(0x%02X) with error: %ld", ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? ScsiCommandToString( MyCdScsiIOReq->csi_CommandBuffer[ 0 ] ) : IOCommandToString( MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ), ( HD_SCSICMD == MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command ) ? MyCdScsiIOReq->csi_CommandBuffer[ 0 ] : MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command, MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error ) ) ;
                              }
                              if( SCSI_STATUS_GOOD != MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
                              {  /* some problem, print sense data then */
                                PrintSenseData( MyCdScsiIOReq ) ;
                              }
                              FinishAsyncCdScsiIORequest( MyCdScsiIOReq ) ;
                            }
                          }
                          if( AHIMask & ReceivedSignalBits )
                          {  /* ahi device replyed a request */
                            while( MyAHIBuffer = ( struct AHIBuffer * )GetMsg( MyDeviceUnit->du_AHIMsgPort ) )
                            {
                              LOG_TEXT( ( _DEBUG_LEVEL, "ahi device finished async io request 0x%08X with error: %ld", MyAHIBuffer, MyAHIBuffer->ab_AHIRequest.ahir_Std.io_Error ) ) ;
                              AddTail( &MyDeviceUnit->du_FreeAHIBufferList, ( struct Node * )MyAHIBuffer ) ;
                              if( 8 == MyDeviceUnit->du_PlayCdScsiIOReq->csi_State )
                              {  /* only in this state we wait for ahi requests */
                                FinishAsyncCdScsiIORequest( MyDeviceUnit->du_PlayCdScsiIOReq ) ;
                              }
                            }
                          }
                        }
                        LOG_TEXT( ( _DEBUG_LEVEL, "device unit main loop did exit" ) ) ;
                        /* free all resources again */
                        AbortIO( ( struct IORequest * )MyDeviceUnit->du_PollTimeRequest ) ;
                        WaitIO( ( struct IORequest * )MyDeviceUnit->du_PollTimeRequest ) ;
                        /* uninitialize the units message port */
                        MyDeviceUnit->du_Unit.unit_MsgPort.mp_Flags = PA_IGNORE ;
                        MyDeviceUnit->du_Unit.unit_MsgPort.mp_SigTask = NULL ;
                        FreeSignal( MyDeviceUnit->du_Unit.unit_MsgPort.mp_SigBit ) ;
                        MyDeviceUnit->du_Unit.unit_MsgPort.mp_SigBit = ( UBYTE )-1 ;
                        
                        switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
                        {
                          case 0:  /* CD-ROM audio */
                            break ;
                          case 1:  /* AHI */
                            if( NULL != MyDeviceUnit->du_QuickAHIMsgPort )
                            {
                              if( NULL != MyDeviceUnit->du_QuickAHIRequest )
                              {
                                if( NULL != MyDeviceUnit->du_AHIMsgPort )
                                {
                                  DeleteMsgPort( MyDeviceUnit->du_AHIMsgPort ) ;
                                  MyDeviceUnit->du_AHIMsgPort = NULL ;
                                }
                                DeleteIORequest( ( struct IORequest * )MyDeviceUnit->du_QuickAHIRequest ) ;
                                MyDeviceUnit->du_QuickAHIRequest = NULL ;
                              }
                              DeleteMsgPort( MyDeviceUnit->du_QuickAHIMsgPort ) ;
                              MyDeviceUnit->du_QuickAHIMsgPort = NULL ;
                            }
                            break ;
                          default:
                            break ;
                        }
                      }
                      else
                      {  /* audio output not ok */
                        LOG_TEXT( ( _ERROR_LEVEL, "audio output not ok" ) ) ;
                      }
                    }
                    else
                    {  /* it is not a CD-ROM drive */
                      LOG_TEXT( ( _ERROR_LEVEL, "it is not a CD-ROM drive" ) ) ;
                    }
                    /* delete the async scsi message port */
                    DeleteMsgPort( MyDeviceUnit->du_ScsiMsgPort ) ;
                  }
                  CloseDevice( ( struct IORequest * )MyDeviceUnit->du_QuickCdScsiIOReq ) ;
                  LOG_TEXT( ( _DEBUG_LEVEL, "scsi device closed" ) ) ;            
                }
                else
                {  /* scsi device ok */
                  LOG_TEXT( ( _ERROR_LEVEL, "scsi device not ok" ) ) ;
                }
                /* delete the scsi io request again */
                DeleteIORequest( ( struct IORequest * )MyDeviceUnit->du_QuickCdScsiIOReq ) ;
                MyDeviceUnit->du_QuickCdScsiIOReq = NULL ;
                LOG_TEXT( ( _DEBUG_LEVEL, "quick scsi io request deleted" ) ) ;            
              }
              else
              {  /* quick scsi io request not ok */
                LOG_TEXT( ( _ERROR_LEVEL, "scsi io request not ok" ) ) ;
              }
              DeleteMsgPort( MyDeviceUnit->du_QuickScsiMsgPort ) ;
              MyDeviceUnit->du_QuickScsiMsgPort = NULL ;
              LOG_TEXT( ( _DEBUG_LEVEL, "scsi message port deleted" ) ) ;            
            }
            else
            {  /* scsi message port not ok */
              LOG_TEXT( ( _ERROR_LEVEL, "scsi message port not ok" ) ) ;
            }
            DeleteMsgPort( MyDeviceUnit->du_UnitMsgPort ) ;
            MyDeviceUnit->du_UnitMsgPort = NULL ;
            LOG_TEXT( ( _DEBUG_LEVEL, "unit message port deleted" ) ) ;            
          }
          else
          {  /* unit message port not ok */
            LOG_TEXT( ( _ERROR_LEVEL, "unit message port not ok" ) ) ;      
          }
          CloseDevice( ( struct IORequest * )MyDeviceUnit->du_PollTimeRequest ) ;
        }
        else
        {  /* timer device not ok */
          LOG_TEXT( ( _ERROR_LEVEL, "timer device not ok" ) ) ;
        }
        DeleteIORequest( ( struct IORequest * )MyDeviceUnit->du_PollTimeRequest ) ;
      }
      else
      {  /* timer io request ok */
        LOG_TEXT( ( _ERROR_LEVEL, "timer io request not ok" ) ) ;
      }
      DeleteMsgPort( MyDeviceUnit->du_TimerMsgPort ) ;
    }
    else
    {  /* timer message port not ok */
      LOG_TEXT( ( _ERROR_LEVEL, "timer message port not ok" ) ) ;
    }
  }
  else
  {  /* core has not send startup message */
    MyThreadMessage->tm_Data = 1 ;
  }
  LOG_TEXT( ( _INFO_LEVEL, "unit thread finished" ) ) ;  

  Forbid( ) ;
  ReplyMsg( ( struct Message *)MyExitMessage ) ;
}




/*
** delete unit
*/
void DeleteDeviceUnit( struct DeviceUnit *MyDeviceUnit, struct DeviceOpener *MyDeviceOpener )
{
  struct DeviceCore *MyDeviceCore ;
  struct ExecBase *SysBase ;

  if( ( NULL != MyDeviceUnit ) && ( NULL != MyDeviceOpener ) )
  {  /* device unit memory needs to be freed */
    MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
    SysBase = MyDeviceCore->dc_SysBase ;

    LOG_TEXT( ( _DEBUG_LEVEL, "DeleteDeviceUnit()" ) ) ;

    if( NULL != MyDeviceUnit->du_Process )
    {  /* process needs to be shutdown */
      LOG_TEXT( ( _DEBUG_LEVEL, "sending shutdown request to unit thread ..." ) ) ;
      MyDeviceOpener->do_ThreadMessage.tm_Command = THREAD_CMD_SHUTDOWN ;
      PutMsg( MyDeviceUnit->du_UnitMsgPort, &MyDeviceOpener->do_ThreadMessage.tm_Message ) ;
      LOG_TEXT( ( _DEBUG_LEVEL, "waiting for last message from unit thread ..." ) ) ;
      WaitPort( MyDeviceOpener->do_CoreMsgPort ) ;
      LOG_TEXT( ( _DEBUG_LEVEL, "last message from unit thread received" ) ) ;        
      GetMsg( MyDeviceOpener->do_CoreMsgPort ) ;
    }
    FreeVec( MyDeviceUnit ) ;
    LOG_TEXT( ( _DEBUG_LEVEL, "device unit memory freed" ) ) ;
  }
}


/*
** create unit
*/
struct DeviceUnit *CreateDeviceUnit( struct DeviceCore *MyDeviceCore, struct DeviceOpener *MyDeviceOpener )
{
  struct ExecBase *SysBase ;
  struct DosLibrary *DOSBase ;
  struct DeviceUnit *MyDeviceUnit ;

  MyDeviceUnit = NULL ;
  
  if( ( NULL != MyDeviceCore ) && ( NULL != MyDeviceOpener ) )
  {  /* requirements ok */
    SysBase = MyDeviceCore->dc_SysBase ;
    DOSBase = MyDeviceCore->dc_DOSBase ;
    MyDeviceUnit = AllocVec( sizeof( struct DeviceUnit ), ( MEMF_ANY | MEMF_CLEAR ) ) ;
    if( NULL != MyDeviceUnit )
    {  /* unit memory ok */
      LOG_TEXT( ( _DEBUG_LEVEL, "unit memory ok, 0x%08X", MyDeviceUnit ) ) ;
      MyDeviceUnit->du_DeviceCore = MyDeviceCore ;
      /* create the unit process */
      MyDeviceUnit->du_Process = CreateNewProcTags( NP_Entry, ( ULONG )DeviceUnitThread,
                                                    NP_Name, ( ULONG )"cd.device unit 0",
                                                    NP_Priority, 30,
                                                    TAG_END ) ;
      if( NULL != MyDeviceUnit->du_Process )
      {  /* unit process was created */
        LOG_TEXT( ( _DEBUG_LEVEL, "unit process was created" ) ) ;
        MyDeviceOpener->do_ThreadMessage.tm_Command = THREAD_CMD_STARTUP ;
        MyDeviceOpener->do_ThreadMessage.tm_Data = ( ULONG )MyDeviceUnit ;
        PutMsg( &MyDeviceUnit->du_Process->pr_MsgPort, &MyDeviceOpener->do_ThreadMessage.tm_Message ) ;
        WaitPort( MyDeviceOpener->do_CoreMsgPort ) ;
        GetMsg( MyDeviceOpener->do_CoreMsgPort ) ;
        if( THREAD_CMD_STARTUP == MyDeviceOpener->do_ThreadMessage.tm_Command )
        {  /* unit thread did reply the startup message */
          if( 0 == MyDeviceOpener->do_ThreadMessage.tm_Data )
          {  /* unit thread ok and running */
          }
          else
          {  /* unit thread not ok and finished itself */
            LOG_TEXT( ( _ERROR_LEVEL, "unit thread not ok and finished itself" ) ) ;
            MyDeviceUnit->du_Process = NULL ;
            DeleteDeviceUnit( MyDeviceUnit, MyDeviceOpener ) ;
            MyDeviceUnit = NULL ;
          }
        }
        else
        {  /* unit thread did not reply the startup message */
          LOG_TEXT( ( _ERROR_LEVEL, "unit thread did not reply the startup message" ) ) ;
          DeleteDeviceUnit( MyDeviceUnit, MyDeviceOpener ) ;
          MyDeviceUnit = NULL ;
        }
      }
      else
      {  /* unit process was not created */
        LOG_TEXT( ( _ERROR_LEVEL, "unit process was not created" ) ) ;
        DeleteDeviceUnit( MyDeviceUnit, MyDeviceOpener ) ;
        MyDeviceUnit = NULL ;
      }
    }
    else
    {  /* unit memory not ok */
      LOG_TEXT( ( _ERROR_LEVEL, "unit memory not ok" ) ) ;
    }
  }
  else
  {  /* requirements not ok */
  }

  return( MyDeviceUnit ) ;
}



/*
 * delete a device opener
 */
void DeleteDeviceOpener( struct DeviceCore *MyDeviceCore, struct DeviceOpener *MyDeviceOpener )
{
  struct ExecBase *SysBase ;
  
  if( NULL != MyDeviceOpener )
  {  /* device opener memory needs to be freed */
    SysBase = MyDeviceCore->dc_SysBase ;
    if( NULL != MyDeviceOpener->do_CoreMsgPort )
    {  /* device opener message port needs to be deleted */
      DeleteMsgPort( MyDeviceOpener->do_CoreMsgPort ) ;
    }
    FreeVec( MyDeviceOpener ) ;
  }
}



/*
 * create an opener
 */
struct DeviceOpener *CreateDeviceOpener( struct DeviceCore *MyDeviceCore )
{
  struct DeviceOpener *MyDeviceOpener ;
  struct ExecBase *SysBase ;
  
  MyDeviceOpener = NULL ;

  if( NULL != MyDeviceCore )
  {  /* requirements ok */
    SysBase = MyDeviceCore->dc_SysBase ;

    MyDeviceOpener = AllocVec( sizeof( struct DeviceOpener ), ( MEMF_ANY | MEMF_CLEAR ) ) ;
    if( NULL != MyDeviceOpener )
    {  /* opener memory ok */
      MyDeviceOpener->do_CoreMsgPort = CreateMsgPort( ) ;
      if( NULL != MyDeviceOpener->do_CoreMsgPort )
      {  /* opener core message port ok */
        MyDeviceOpener->do_ThreadMessage.tm_Message.mn_ReplyPort = MyDeviceOpener->do_CoreMsgPort ;
      }
      else
      {  /* opener core message port ok */
        DeleteDeviceOpener( MyDeviceCore, MyDeviceOpener ) ;
        MyDeviceOpener = NULL ;
      }
    }
    else
    {  /* opener memory not ok */
    }
  }
  else
  {  /* requirements not ok */
  }

  return( MyDeviceOpener ) ;

}
