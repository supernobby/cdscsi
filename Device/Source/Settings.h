/*
** Settings.h
*/


#ifndef _SETTINGS_H
#define _SETTINGS_H


#include <exec/types.h>


/*
** some forward declarations to avoid recursive inludes
*/
struct DeviceCore ;


/*
** the available setting index values
*/
enum SettingIndex
{
  ScsiDevice,
  ScsiUnit,
  AudioOutput,
  AhiUnit,
  LogLevel,
  NUM_SETTINGS  /* number of settings */
} ;


/*
** this defines a setting
*/
struct SettingDescriptor
{
  ULONG sd_Index ;  /* a index number */
  STRPTR sd_Name ;  /* a name string */
  ULONG sd_Type ;  /* the type information */
} ;


/*
** settings context
*/
struct Settings
{
  struct DeviceCore *s_DeviceCore ;  /* pointer to the context */
  LONG *s_Current ;  /* current setting values */
  const struct SettingDescriptor *s_Descriptors ;  /* description of settings */
  ULONG s_Changed : 1 ;
} ;


/*
** module functions
*/
struct Settings *CreateSettings( struct DeviceCore *MyDeviceCore ) ;
void DeleteSettings( struct Settings *OldSettings ) ;
void SetSetting( struct Settings *MySettings, enum SettingIndex Index, LONG Value ) ;
LONG GetSetting( struct Settings *MySettings, enum SettingIndex Index ) ;
void SetDefaultSettings( struct Settings *MySettings ) ;
void LoadSettings( struct Settings *MySettings ) ;


#endif   /* _SETTINGS_H */
