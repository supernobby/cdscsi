/*
** DeviceCore.c
*/


#include "DeviceCore.h"
#include "DeviceUnit.h"
#include "LogOutput.h"
#include "Settings.h"
#include "CompilerExtensions.h"
#include <proto/exec.h>
#include <exec/initializers.h>
#include <exec/resident.h>
#include <dos/dosextens.h>
#include <exec/errors.h>
#include <clib/alib_protos.h>


static void ToggleScreen( struct ExecBase *SysBase, ULONG Pen ) ;

/*
** mandatory OS library functions
*/
static BYTE DeviceOpen( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IORequest *MyIORequest ), REG( d0, ULONG UnitNumber ), REG( d1, ULONG Flags ) ) ;
static BPTR DeviceClose( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IORequest *MyIORequest ) ) ;
static BPTR DeviceExpunge( REG( a6, struct DeviceCore *MyDeviceCore ) ) ;
static ULONG DeviceReserved( void ) ;
static void DeviceBeginIO( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IOStdReq *MyIOStdReq ) ) ;
static void DeviceAbortIO( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IOStdReq *MyIOStdReq ) ) ;
static ULONG DeviceGetSCR( struct Unit * ) ;
static BOOL DeviceSetSCR( struct Unit *, ULONG ) ;



#define NAME_STRING "cd.device"


/*
** standard version tag
*/
static const char VersionTag[ ] = "\0$VER: " NAME_STRING " " MAJOR_STRING "." MINOR_STRING "." REVISION_STRING " (" DATE_STRING ") CdScsi"  ;



/*
** function tabel of the device, also used in InitTable[]
*/
static const APTR FunctionTable[ ] =
{
  ( APTR )DeviceOpen,
  ( APTR )DeviceClose,
  ( APTR )DeviceExpunge,
  ( APTR )DeviceReserved,
  ( APTR )DeviceBeginIO,
  ( APTR )DeviceAbortIO,
  ( APTR )DeviceSetSCR,
  ( APTR )DeviceGetSCR,
  ( APTR )-1
} ;


/*
** InitStruct() compatible data, required by InitTable[]
*/
#undef INITLONG
#define INITLONG( _offset, _value ) 0xc000, ( UWORD )_offset, _value
static const struct
{
  UWORD Command0 ; UWORD Offset0 ; ULONG Value0 ;
  UWORD Command1 ; UWORD Offset1 ; ULONG Value1 ;
  UWORD Command2 ; UWORD Offset2 ; UWORD Value2 ;
  UWORD Command3 ; UWORD Offset3 ; UWORD Value3 ;
  UWORD Command4 ; UWORD Offset4 ; UWORD Value4 ;
  UWORD Command5 ; UWORD Offset5 ; UWORD Value5 ;
  UWORD Command6 ; UWORD Offset6 ; UWORD Value6 ;
  UWORD End ;
} DataTable =
{
  INITLONG( OFFSET( Library, lib_IdString ), ( ULONG )&VersionTag[ 0 ] ),
  INITLONG( OFFSET( Node, ln_Name ), ( ULONG )NAME_STRING ) ,
  INITBYTE( OFFSET( Node, ln_Type ), NT_DEVICE ),
  INITBYTE( OFFSET( Library, lib_Flags ), LIBF_SUMUSED|LIBF_CHANGED ),
  INITWORD( OFFSET( Library, lib_Version ), CDSCSI_MAJOR ),
  INITWORD( OFFSET( Library, lib_Revision ), CDSCSI_MINOR ),
  INITWORD( OFFSET( Library, lib_OpenCnt ), 0 ),  /* set to 1 to permanently prevent expunge */
  0
} ;


/*
** called from the "ROMTag"/MakeLibrary(), required by InitTable[]
*/
static struct Library *InitFunc( REG( d0, struct DeviceCore *MyDeviceCore ), REG( a0, BPTR SegList ), REG( a6, struct ExecBase *SysBase ) )
{
  struct Library *Result ;

  Result = NULL ;

  /* store important values for later use */
  MyDeviceCore->dc_SysBase = SysBase ;
  MyDeviceCore->dc_SegList = SegList ;

  //ToggleScreen( SysBase, 1 ) ;
  
  /* all ok */
  Result =  ( struct Library * )MyDeviceCore ;

  /* when returning the pointer to the library base, things are ok */
  return( Result ) ;
}


/*
** table to be suitable for the RTF_AUTOINIT flag in the ROMTag
*/
static const ULONG InitTable[ 4 ] =
{
  sizeof( struct DeviceCore ),  /* size of device base structure */
  ( ULONG )FunctionTable,  /* pointer to function table */
  ( ULONG )&DataTable,  /* pointer to data table for InitStruct function */
  ( ULONG )InitFunc  /* pointer to initialization code */
} ;


/*
** the ROMTag structure that we can run from (FLASH) ROM
*/
const struct Resident ROMTag =
{
  RTC_MATCHWORD, /* rt_MatchWord word to match on (ILLEGAL) */
  ( struct Resident * )&ROMTag,  /* struct Resident *rt_MatchTag; pointer to the above */
  ( APTR )( ( &ROMTag ) + 1 ),  /* rt_EndSkip; address to continue scan */
  RTF_COLDSTART | RTF_AUTOINIT,  /* rt_Flags;  various tag flags */
  CDSCSI_MAJOR,  /* rt_Version; release version number */
  NT_DEVICE,  /* rt_Type; type of module (NT_XXXXXX) */
  0, /* rt_Pri; initialization priority */
  NAME_STRING,  /* *rt_Name; pointer to node name */
  ( char * )&VersionTag[ 7 ],  /* *rt_IdString; pointer to identification string */
  ( APTR )InitTable  /* rt_Init; pointer to init code */
} ;


/*
 * helper function to open some stuff
 */
static void CustomDeviceOpen( struct DeviceCore *MyDeviceCore )
{
  struct ExecBase *SysBase ;

  SysBase = MyDeviceCore->dc_SysBase ;
  
  MyDeviceCore->dc_DOSBase = ( struct DosLibrary * )OpenLibrary( "dos.library", 36 ) ;
  MyDeviceCore->dc_LogOutput = CreateLogOutput( MyDeviceCore ) ;
  MyDeviceCore->dc_Settings = CreateSettings( MyDeviceCore ) ;
  if( MyDeviceCore->dc_Settings )
  {
    SetDefaultSettings( MyDeviceCore->dc_Settings ) ;
    LoadSettings( MyDeviceCore->dc_Settings ) ;
    ConfigureLogOutput( MyDeviceCore->dc_LogOutput ) ;
  }
}


/*
 * helper function to close some stuff
 */
static void CustomDeviceClose( struct DeviceCore *MyDeviceCore )
{
  struct ExecBase *SysBase ;

  SysBase = MyDeviceCore->dc_SysBase ;

  if( NULL != MyDeviceCore->dc_Settings )
  {
    DeleteSettings( MyDeviceCore->dc_Settings ) ;
    MyDeviceCore->dc_Settings = NULL ;
  }
  if( NULL != MyDeviceCore->dc_LogOutput )
  {
    DeleteLogOutput( MyDeviceCore->dc_LogOutput ) ;
    MyDeviceCore->dc_LogOutput = NULL ;
  }
  if( NULL != MyDeviceCore->dc_DOSBase )
  {
    CloseLibrary( ( struct Library * )MyDeviceCore->dc_DOSBase ) ;
    MyDeviceCore->dc_DOSBase = NULL ;
  }
}


/*
** called via OpenDevice()
*/
static BYTE DeviceOpen( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IORequest *MyIORequest ), REG( d0, ULONG UnitNumber ), REG( d1, ULONG Flags ) )
{
  struct ExecBase *SysBase ;
  struct DeviceOpener *MyDeviceOpener ;

  SysBase = MyDeviceCore->dc_SysBase ;

  /* default to open failure */
  MyIORequest->io_Error = IOERR_OPENFAIL ;
  /* expunge protection */
  MyDeviceCore->dc_Library.lib_OpenCnt++ ;
  
  if( 0 == UnitNumber )
  {  /* unit number 0 is allowed */
    MyDeviceOpener = CreateDeviceOpener( MyDeviceCore ) ;
    if( NULL != MyDeviceOpener )
    {  /* device opener ok */
      if( NULL == MyDeviceCore->dc_DeviceUnit )
      {  /* unit not yet created */
        CustomDeviceOpen( MyDeviceCore ) ;
        MyDeviceCore->dc_DeviceUnit = CreateDeviceUnit( MyDeviceCore, MyDeviceOpener ) ;
      }
      if( NULL != MyDeviceCore->dc_DeviceUnit )
      {  /* device unit ok */
        LOG_TEXT( ( _INFO_LEVEL, "device unit ok" ) ) ;
        MyDeviceOpener->do_DeviceUnit = MyDeviceCore->dc_DeviceUnit ;
        /* increment open counter */
        MyDeviceCore->dc_DeviceUnit->du_Unit.unit_OpenCnt++ ;
        /* store the unit opener in this io request */
        MyIORequest->io_Unit = ( struct Unit * )MyDeviceOpener ;
        /* prevent delayed expunges */
        MyDeviceCore->dc_Library.lib_Flags &= ~( LIBF_DELEXP ) ;
        /* important to mark io request as complete */
        MyIORequest->io_Message.mn_Node.ln_Type = NT_REPLYMSG ;
        /* all good */
        MyIORequest->io_Error = 0 ;
      }
      else
      {  /* device unit not ok */
        LOG_TEXT( ( _FAILURE_LEVEL, "device unit not ok" ) ) ;
        DeleteDeviceOpener( MyDeviceCore, MyDeviceOpener ) ;
        CustomDeviceClose( MyDeviceCore ) ;
      }
    }
  }
    
  if( MyIORequest->io_Error )
  {
    /* important to trash the device node pointer */
    MyIORequest->io_Device = NULL ;
    /* end expunge protection */
    MyDeviceCore->dc_Library.lib_OpenCnt-- ;
  }
    
  return( MyIORequest->io_Error ) ;
}


/*
** called via CloseDevice()
*/
static BPTR DeviceClose( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IORequest *MyIORequest ) )
{
  struct ExecBase *SysBase ;
  struct DeviceOpener *MyDeviceOpener ;
  struct DeviceUnit *MyDeviceUnit ;
  BPTR Result ;

  SysBase = MyDeviceCore->dc_SysBase ;
  MyDeviceOpener = ( struct DeviceOpener * )MyIORequest->io_Unit ;
  MyDeviceUnit = ( struct DeviceUnit * )MyDeviceOpener->do_DeviceUnit ;
  Result = 0 ;

  LOG_TEXT( ( _DEBUG_LEVEL, "DeviceClose()" ) ) ;

  /* this makes sure, the IORequest will not work anymore */
  MyIORequest->io_Unit = ( APTR )-1 ;
  MyIORequest->io_Device = ( APTR )-1 ;

  /* decrement open counter of the unit */
  MyDeviceCore->dc_DeviceUnit->du_Unit.unit_OpenCnt-- ;
  if( 0 == MyDeviceCore->dc_DeviceUnit->du_Unit.unit_OpenCnt )
  {  /* unit not required any more */
    DeleteDeviceUnit( MyDeviceCore->dc_DeviceUnit, MyDeviceOpener ) ;
    MyDeviceCore->dc_DeviceUnit = NULL ;
  }

  /* opener not needed any more */
  DeleteDeviceOpener( MyDeviceCore, MyDeviceOpener ) ;
  
  /* decrement open counter of the device */
  MyDeviceCore->dc_Library.lib_OpenCnt-- ;
  if( 0 == MyDeviceCore->dc_Library.lib_OpenCnt )
  {  /* device is now unused */
    CustomDeviceClose( MyDeviceCore ) ; 

    if( LIBF_DELEXP & MyDeviceCore->dc_Library.lib_Flags )
    {  /* expunge pending */
      Result = DeviceExpunge( MyDeviceCore ) ;
    }
    else
    {  /* no expunge pending, so leave the device in the system */
    }
  }
  else
  {  /* device still in use */
  }

  return( Result ) ;
}


/*
** called via RemLibrary() or automatically if system is out of memory
*/
static BPTR DeviceExpunge( REG( a6, struct DeviceCore *MyDeviceCore ) )
{
  struct ExecBase *SysBase ;
  BPTR Result ;

  SysBase = MyDeviceCore->dc_SysBase ;

  if( 0 == MyDeviceCore->dc_Library.lib_OpenCnt )
  {  /* device unused, expunge can be done */
    /* segments can be unloaded afterwards */
    Result = MyDeviceCore->dc_SegList ;
    /* unlink from list before we free the node memory */
    Remove( &MyDeviceCore->dc_Library.lib_Node ) ;

    /* ... library specific closings would go here ... */

    /* free our memory */
    FreeMem( ( ( BYTE * )MyDeviceCore ) - MyDeviceCore->dc_Library.lib_NegSize, \
      ( ULONG )( MyDeviceCore->dc_Library.lib_NegSize + MyDeviceCore->dc_Library.lib_PosSize ) ) ;
  }
  else
  {  /* device still in use */
    /* set the delayed expunge flag */
    MyDeviceCore->dc_Library.lib_Flags |= ( LIBF_DELEXP ) ;
    /* segments can't be unloaded */
    Result = 0 ;
  }

  return( Result ) ;
}


/*
** must return 0
*/
static ULONG DeviceReserved( void )
{
  return( 0 ) ;
}



/*
** called via exec io functions
*/
static void DeviceBeginIO( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IOStdReq *MyIOStdReq ) )
{
  struct ExecBase *SysBase ;
  struct DeviceOpener *MyDeviceOpener ;
  struct DeviceUnit *MyDeviceUnit ;

  SysBase = MyDeviceCore->dc_SysBase ;
  MyDeviceOpener = ( struct DeviceOpener * )MyIOStdReq->io_Unit ;
  MyDeviceUnit = ( struct DeviceUnit * )MyDeviceOpener->do_DeviceUnit ;

  /* this makes sure that WaitIO() works */
  MyIOStdReq->io_Message.mn_Node.ln_Type = NT_MESSAGE ;
  
  if( IOF_QUICK & MyIOStdReq->io_Flags )
  {  /* perform command "inline" */
    MyDeviceOpener->do_ThreadMessage.tm_Command = THREAD_CMD_DOIO ;
    MyDeviceOpener->do_ThreadMessage.tm_Data = ( ULONG )MyIOStdReq ;
    PutMsg( MyDeviceUnit->du_UnitMsgPort, &MyDeviceOpener->do_ThreadMessage.tm_Message ) ;
    WaitPort( MyDeviceOpener->do_CoreMsgPort ) ;
    GetMsg( MyDeviceOpener->do_CoreMsgPort ) ;
  }
  else
  {  /* send to unit thread */
    PutMsg( &MyDeviceUnit->du_Unit.unit_MsgPort, &MyIOStdReq->io_Message ) ;
  }
}


/*
** called via AbortIO()
*/
static void DeviceAbortIO( REG( a6, struct DeviceCore *MyDeviceCore ), REG( a1, struct IOStdReq *MyIOStdReq ) )
{
  struct ExecBase *SysBase ;
  struct DeviceOpener *MyDeviceOpener ;
  struct DeviceUnit *MyDeviceUnit ;

  SysBase = MyDeviceCore->dc_SysBase ;
  MyDeviceOpener = ( struct DeviceOpener * )MyIOStdReq->io_Unit ;
  MyDeviceUnit = ( struct DeviceUnit * )MyDeviceOpener->do_DeviceUnit ;
  /* we assume, quick io requests will not be aborted via other tasks */
  MyDeviceOpener->do_ThreadMessage.tm_Command = THREAD_CMD_ABORTIO ;
  MyDeviceOpener->do_ThreadMessage.tm_Data = ( ULONG )MyIOStdReq ;
  PutMsg( MyDeviceUnit->du_UnitMsgPort, &MyDeviceOpener->do_ThreadMessage.tm_Message ) ;
  WaitPort( MyDeviceOpener->do_CoreMsgPort ) ;
  GetMsg( MyDeviceOpener->do_CoreMsgPort ) ;
}


/*
** not supported
*/
static ULONG DeviceGetSCR( struct Unit * MyUnit )
{
  return( 0 ) ;
}


/*
** not supported
*/
static BOOL DeviceSetSCR( struct Unit *MyUnit, ULONG Value )
{
  return( 0 ) ;
}





#include <proto/intuition.h>
#include <proto/graphics.h>

static void ToggleScreen( struct ExecBase *SysBase, ULONG Pen )
{
  struct IntuitionBase *IntuitionBase ;
  struct GfxBase *GfxBase ;
  struct Screen *BootScreen ; 
  volatile ULONG Count ; 
  struct NewScreen NewScr =
  {
    0, 0, 320, 256, 4,
    0, 1,
    0, CUSTOMSCREEN,
    NULL,
    NULL,
    NULL,
    NULL
  } ; 

  IntuitionBase = ( struct IntuitionBase * )OpenLibrary( "intuition.library", 0 ) ;
  if( NULL != IntuitionBase )
  {
    GfxBase = ( struct GfxBase * )OpenLibrary( "graphics.library", 0 ) ;
    if( NULL != GfxBase )
    {
      BootScreen = OpenScreen( &NewScr ) ;
      if( NULL != BootScreen )
      {
        SetRGB4( &BootScreen->ViewPort, 0, 0, 0, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 1, 15, 0, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 2, 0, 15, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 3, 0, 0, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 4, 15, 15, 0 ) ;
        SetRGB4( &BootScreen->ViewPort, 5, 15, 0, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 6, 0, 15, 15 ) ;
        SetRGB4( &BootScreen->ViewPort, 7, 7, 7, 7 ) ;
        SetRGB4( &BootScreen->ViewPort, 8, 15, 15, 15 ) ;
        for( Count = 0 ; Count < 0x4F ; Count++ )
        {
          SetRast( &BootScreen->RastPort, Pen ) ;
          //SetRast( &BootScreen->RastPort, ( unsigned char )( Count & 0xFF ) ) ;
        }
        SetRast( &BootScreen->RastPort, 0 ) ;             
        CloseScreen( BootScreen ) ;
      }
      CloseLibrary( ( struct Library * )GfxBase ) ;
    }
    CloseLibrary( ( struct Library * )IntuitionBase ) ;
  }             
}


