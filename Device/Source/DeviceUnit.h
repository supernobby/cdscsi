/*
** DeviceUnit.h
*/


#ifndef _DEVICEUNIT_H
#define _DEVICEUNIT_H


#include <exec/devices.h>
#include <exec/io.h>
#include <exec/semaphores.h>
#include <devices/scsidisk.h>
#include <devices/cd.h>
#include <devices/timer.h>
#include <devices/ahi.h>


/*
** some forward declarations to avoid recursive inludes
*/
struct DeviceCore ;


/*
 * message to be send around between core and unit threads
 */
struct ThreadMessage
{
  struct Message tm_Message ;
  ULONG tm_Command ;
  ULONG tm_Data ;
} ;


/*
 * possible thread message commands
 */
#define THREAD_CMD_STARTUP ( 0 )
#define THREAD_CMD_SHUTDOWN ( 1 )
#define THREAD_CMD_DOIO ( 2 )
#define THREAD_CMD_ABORTIO ( 3 )



/*
** opener context for each opener of a unit
*/
struct DeviceOpener
{
  struct MsgPort *do_CoreMsgPort ;  /* for inter-process communication from core to unit */
  struct ThreadMessage do_ThreadMessage ;  /* message to be send between core and unit */
  struct DeviceUnit *do_DeviceUnit ;  /* the actual device unit */
} ;


/*
 * cdda sector size is max 2352 bytes
 */
#define CDDA_SECTOR_SIZE ( 2352U )


/*
 * AHI buffer
 */
#define AHI_BUFFER_SIZE ( CDDA_SECTOR_SIZE * 16 )
struct AHIBuffer
{
  struct AHIRequest ab_AHIRequest ;
  UWORD ab_Data[ AHI_BUFFER_SIZE / sizeof( UWORD ) ] ;  /* the actual buffer */  
  //UBYTE ab_Data[ AHI_BUFFER_SIZE / sizeof( UBYTE ) ] ;  /* the actual buffer */  
} ;


/*
 * the io request we send to the scsi device
 */
#define SCSI_COMMAND_LENGTH ( 12UL )
#define SCSI_DATA_LENGTH ( 4096UL )
#define SCSI_SENSEDATA_LENGTH ( 128UL )
struct CdScsiIOReq
{
  struct IOStdReq csi_ScsiIOStdReq ;  /* the io request that gets send to the scsi device */
  struct SCSICmd csi_SCSICmd ;  /* the SCSI command for the scsi device */
  WORD csi_DataBuffer[ SCSI_DATA_LENGTH / sizeof( WORD ) ] ;  /* data related to the command */
  UBYTE csi_CommandBuffer[ SCSI_COMMAND_LENGTH ] ;  /* the actual scsi command */
  UBYTE csi_SenseDataBuffer[ SCSI_SENSEDATA_LENGTH ] ;  /* for automatic sense data */
  struct IOStdReq *csi_CdIOStdReq ;  /* the cd io request on which behalf the scsi io request is send */
  struct ThreadMessage *csi_ThreadMessage ;  /* the initiator of a quick io request */
  union
  {
    struct CDXL *cr_CDXL ;
    struct AHIBuffer *cr_AHIBuffer ;
  } csi_CurrentReader ;
  ULONG csi_State ;
} ;


/*
** unit context
*/
#define NUM_CDSCSI_REQUESTS ( 5 )
#define NUM_AHI_BUFFERS ( 2 )
struct DeviceUnit
{
  struct Unit du_Unit ;
  ULONG du_Number ;  /* the unit number */
  struct DeviceCore *du_DeviceCore ;  /* pointer to device core */
  struct MsgPort *du_UnitMsgPort ;  /* for inter-process communication from unit to core */
  struct Process *du_Process ;  /* the unit process */
  struct MsgPort *du_QuickScsiMsgPort ;  /* for own synchron io requests */
  struct CdScsiIOReq *du_QuickCdScsiIOReq ;  /* for quick io requests */
  struct MsgPort *du_ScsiMsgPort ;  /* for async io requests */
  struct CdScsiIOReq du_CdScsiIOReq[ NUM_CDSCSI_REQUESTS ] ;  /* for slow io requests */
  struct List du_FreeIOReqList ;
  struct SignalSemaphore du_Semaphore ;
  struct CdScsiIOReq *du_PlayCdScsiIOReq ;
  struct CdScsiIOReq *du_ReadCdScsiIOReq ;
  struct CDInfo du_CDInfo ;  /* current status of the cd device unit */
  union CDTOC du_CDTOC[ 100 ] ;  /* table of content of currently insert medium */
  struct QCode du_CurrentQCode ;
  struct DeviceOpener du_FakeDeviceOpener ;
  struct IOStdReq du_FakeCdIOStdReq ;  /* to run SCSI commands internally */
  struct MsgPort *du_TimerMsgPort ;  /* for replies from timer device */
  struct Device *du_TimerBase ;
  struct timerequest *du_PollTimeRequest ;  /* for request to the timer device */
  struct timeval du_PollTimeVal ;
  struct timerequest du_ReadTimeRequest ;  /* for controlling read timing */
  struct timeval du_ReadTimeVal ;
  struct MsgPort *du_QuickAHIMsgPort ;
  struct AHIRequest *du_QuickAHIRequest ;
  struct MsgPort *du_AHIMsgPort ;
  struct AHIBuffer du_AHIBuffer[ NUM_AHI_BUFFERS ] ;
  struct List du_FreeAHIBufferList ;
  struct AHIBuffer *du_LastAHIBuffer ;
  LONG du_TargetVolume ;
  LONG du_CurrentVolume ;
  LONG du_VolumeFadeTime ;
  struct timerequest du_VolumeTimeRequest ;  /* for controlling volume fades */
  struct timeval du_VolumeTimeVal ;
  ULONG du_AbortPlay : 1 ;
} ;


/*
** module functions
*/
struct DeviceOpener *CreateDeviceOpener( struct DeviceCore *MyDeviceCore ) ;
void DeleteDeviceOpener( struct DeviceCore *MyDeviceCore, struct DeviceOpener *MyDeviceOpener ) ;
struct DeviceUnit *CreateDeviceUnit( struct DeviceCore *MyDeviceCore, struct DeviceOpener *MyDeviceOpener ) ;
void DeleteDeviceUnit( struct DeviceUnit *MyDeviceUnit, struct DeviceOpener *MyDeviceOpener ) ;
LONG OpenAHI( struct DeviceUnit *MyDeviceUnit ) ;
void CloseAHI( struct DeviceUnit *MyDeviceUnit ) ;
void UpdateVolume( struct DeviceUnit *MyDeviceUnit ) ;
STRPTR IOCommandToString( ULONG IOCommand ) ;


#endif   /* _DEVICEUNIT_H */
