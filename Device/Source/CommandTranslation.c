/*
 * CommandTranslation.c
 */


#include "CommandTranslation.h"
#include "DeviceUnit.h"
#include "DeviceCore.h"
#include "ScsiCommands.h"
#include "Settings.h"
#include "LogOutput.h"
#include "CompilerExtensions.h"
#include <proto/exec.h>
#include <proto/timer.h>
#include <devices/scsidisk.h>
#include <devices/trackdisk.h>
#include <devices/cd.h>
#include <utility/tagitem.h>
#include <exec/errors.h>


/*
 * helper to print scsi command
 */
static void PrintScsiCommand( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct IOStdReq *MyCdIOStdReq ;
  struct SCSICmd *MySCSICmd ;
  
  MyCdIOStdReq = MyCdScsiIOReq->csi_CdIOStdReq ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;

  MySCSICmd = MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data ;
  
  if( 0x45 != MySCSICmd->scsi_Command[ 0 ] )
  {
    //return ;
  }
  
#if 0
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Data: 0x%08X", MySCSICmd->scsi_Data ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Length: %ld", MySCSICmd->scsi_Length ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Actual: %ld", MySCSICmd->scsi_Actual ) ) ;
#endif
  if( 10 == MySCSICmd->scsi_CmdLength )
  {
    LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Command: 0x%08X [ %02X, %02X, %02X, %02X, %02X, %02X, %02X, %02X, %02X, %02X", \
      MySCSICmd->scsi_Command, \
      MySCSICmd->scsi_Command[ 0 ], \
      MySCSICmd->scsi_Command[ 1 ], \
      MySCSICmd->scsi_Command[ 2 ], \
      MySCSICmd->scsi_Command[ 3 ], \
      MySCSICmd->scsi_Command[ 4 ], \
      MySCSICmd->scsi_Command[ 5 ], \
      MySCSICmd->scsi_Command[ 6 ], \
      MySCSICmd->scsi_Command[ 7 ], \
      MySCSICmd->scsi_Command[ 8 ], \
      MySCSICmd->scsi_Command[ 9 ] \
    ) ) ;
  }
  else
  {
    LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Command: 0x%08X [ %02X, %02X, %02X, %02X, %02X, %02X", \
      MySCSICmd->scsi_Command, \
      MySCSICmd->scsi_Command[ 0 ], \
      MySCSICmd->scsi_Command[ 1 ], \
      MySCSICmd->scsi_Command[ 2 ], \
      MySCSICmd->scsi_Command[ 3 ], \
      MySCSICmd->scsi_Command[ 4 ], \
      MySCSICmd->scsi_Command[ 5 ] \
    ) ) ;
  }
#if 0
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_CmdLength: %d", MySCSICmd->scsi_CmdLength ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_CmdActual: %d", MySCSICmd->scsi_CmdActual ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Flags: %02X", MySCSICmd->scsi_Flags ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Status: %02X", MySCSICmd->scsi_Status ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_SenseData: 0x%08X [ %02X, %02X, %02X, %02X, %02X, %02X, ...", MySCSICmd->scsi_SenseData, MySCSICmd->scsi_SenseData[ 0 ], MySCSICmd->scsi_SenseData[ 1 ], MySCSICmd->scsi_SenseData[ 2 ], MySCSICmd->scsi_SenseData[ 3 ], MySCSICmd->scsi_SenseData[ 4 ], MySCSICmd->scsi_SenseData[ 5 ] ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_SenseLength: %d", MySCSICmd->scsi_SenseLength ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_SenseActual: %d", MySCSICmd->scsi_SenseActual ) ) ;
#endif
}



static ULONG LsnToMsf( ULONG LsnValue )
{
  ULONG Minutes, Seconds, Frames ;
  ULONG MsfValue ;
  
  Minutes = LsnValue / ( 60 * 75 ) ;
  LsnValue -= Minutes * ( 60 * 75 ) ;
  Seconds = LsnValue / ( 75 ) ;
  LsnValue -= Seconds * ( 75 ) ;
  Frames = LsnValue ;
  
  MsfValue = ( Minutes << 16 ) + ( Seconds << 8 ) + ( Frames << 0 ) ;
  
  return( MsfValue ) ;
}



static ULONG MsfToLsn( ULONG MsfValue )
{
  ULONG Minutes, Seconds, Frames ;
  ULONG LsnValue ;
  
  Minutes = ( ( MsfValue >> 16 ) && 0xFF ) ;
  Seconds = ( ( MsfValue >> 8 ) && 0xFF ) ;
  Frames = ( ( MsfValue >> 0 ) && 0xFF ) ;
  
  LsnValue = Minutes * ( 60 * 75 ) ;
  LsnValue += Seconds * ( 75 ) ;
  LsnValue += Frames ;

  return( LsnValue ) ;
}



/*
 * translate a cd io request into a suitable scsi io request
 */
LONG TranslateCdToScsi( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct ExecBase *SysBase ;
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct IOStdReq *MyCdIOStdReq, *MyScsiIOStdReq ;
  LONG Error, ScsiDeviceNext ;
  struct TagItem *MyTagItem ;
  struct Device *TimerBase ;
  struct CDXL *CurrentCDXL ;
  ULONG StartLSN, LengthLSN ;
  struct AHIRequest *MyAHIRequest ;
  struct AHIBuffer *CurrentAHIBuffer ;
  ULONG AlreadyFading ;
  
  MyScsiIOStdReq = &MyCdScsiIOReq->csi_ScsiIOStdReq ;
  MyCdIOStdReq = MyCdScsiIOReq->csi_CdIOStdReq ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  TimerBase = MyDeviceUnit->du_TimerBase ;
  ScsiDeviceNext = 0 ;
  Error = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Status = 0 ;

  /* command depending things */
  switch( MyCdIOStdReq->io_Command )
  {
    case CD_RESET:  /* 1 */
      MyScsiIOStdReq->io_Command = MyCdIOStdReq->io_Command ;
      MyScsiIOStdReq->io_Error = MyCdIOStdReq->io_Error ;
      MyScsiIOStdReq->io_Actual = MyCdIOStdReq->io_Actual ;
      MyScsiIOStdReq->io_Length = MyCdIOStdReq->io_Length ;
      MyScsiIOStdReq->io_Data = MyCdIOStdReq->io_Data ;
      MyScsiIOStdReq->io_Offset = MyCdIOStdReq->io_Offset ;
      ScsiDeviceNext = 1 ;
      break ;
    case CD_READ:  /* 2 */
      if( 0 == MyCdScsiIOReq->csi_State )
      {  /* start command sequence */
        LOG_TEXT( ( _INFO_LEVEL, "CD_READ, length: %u, offset: %u", MyCdIOStdReq->io_Length,  MyCdIOStdReq->io_Offset ) ) ;
        if( ( MyCdIOStdReq->io_Data ) && ( 0 != MyCdIOStdReq->io_Length ) )
        {  /* io data parameter valid */
          MyCdScsiIOReq->csi_State = 1 ;
          MyCdIOStdReq->io_Actual = 0 ;
        }
        else
        {  /* io data parameter not valid */
          MyCdIOStdReq->io_Error = CDERR_BADLENGTH ;
        }
      }
      switch( MyCdScsiIOReq->csi_State )
      {
        case 1:
          MyScsiIOStdReq->io_Command = CD_READ ;
          MyScsiIOStdReq->io_Error = 0 ;
          MyScsiIOStdReq->io_Actual = 0 ;
          MyScsiIOStdReq->io_Length = MyCdIOStdReq->io_Length - MyCdIOStdReq->io_Actual ;
          MyScsiIOStdReq->io_Length = ( MyScsiIOStdReq->io_Length / MyDeviceUnit->du_CDInfo.SectorSize ) * MyDeviceUnit->du_CDInfo.SectorSize ;
          if( MyScsiIOStdReq->io_Length )
          {  /* use align sector size and user supplid buffer */
            MyScsiIOStdReq->io_Data = MyCdIOStdReq->io_Data ;
          }
          else
          {  /* read minimal 1 sector and private buffer */
            MyScsiIOStdReq->io_Length = MyDeviceUnit->du_CDInfo.SectorSize ;
            MyScsiIOStdReq->io_Data = MyCdScsiIOReq->csi_DataBuffer ;        
          }
          MyScsiIOStdReq->io_Offset = MyCdIOStdReq->io_Offset + MyCdIOStdReq->io_Actual ;
          ScsiDeviceNext = 1 ;
          break ;
      }
      break ;
    case CD_WRITE:  /* 3 */
    case CD_UPDATE:  /* 4 */
    case CD_CLEAR:  /* 5 */
    case CD_STOP:  /* 6 */
    case CD_START:  /* 7 */
    case CD_FLUSH:  /* 8 */
    case CD_MOTOR:  /* 9 */
    case CD_SEEK:  /* 10 */
    case CD_FORMAT:  /* 11 */
    case CD_REMOVE:  /* 12 */
    case CD_CHANGENUM:  /* 13 */
    case CD_CHANGESTATE:  /* 14 */
    case CD_PROTSTATUS:  /* 15 */
    case CD_GETDRIVETYPE:  /* 18 */
    case CD_GETNUMTRACKS:  /* 19 */
    case CD_ADDCHANGEINT:  /* 20 */
    case CD_REMCHANGEINT:  /* 21 */
    case CD_GETGEOMETRY:  /* 22 */
    case CD_EJECT:  /* 23 */
      /* for that command, we can just copy things */
      MyScsiIOStdReq->io_Command = MyCdIOStdReq->io_Command ;
      MyScsiIOStdReq->io_Error = MyCdIOStdReq->io_Error ;
      MyScsiIOStdReq->io_Actual = MyCdIOStdReq->io_Actual ;
      MyScsiIOStdReq->io_Length = MyCdIOStdReq->io_Length ;
      MyScsiIOStdReq->io_Data = MyCdIOStdReq->io_Data ;
      MyScsiIOStdReq->io_Offset = MyCdIOStdReq->io_Offset ;
      ScsiDeviceNext = 1 ;
      break ;
    case HD_SCSICMD:  /* 28 */
      /* for that command, we can just copy things */
      MyScsiIOStdReq->io_Command = MyCdIOStdReq->io_Command ;
      MyScsiIOStdReq->io_Error = MyCdIOStdReq->io_Error ;
      MyScsiIOStdReq->io_Actual = MyCdIOStdReq->io_Actual ;
      MyScsiIOStdReq->io_Length = MyCdIOStdReq->io_Length ;
      MyScsiIOStdReq->io_Data = MyCdIOStdReq->io_Data ;
      MyScsiIOStdReq->io_Offset = MyCdIOStdReq->io_Offset ;
      PrintScsiCommand( MyCdScsiIOReq ) ;
      ScsiDeviceNext = 1 ;
      break ;
    case CD_INFO:  /* 32 */
      //LOG_TEXT( ( _DEBUG_LEVEL, "cd info request 0x%08X, %d (%d)", MyCdIOStdReq->io_Data, MyCdIOStdReq->io_Length, sizeof( struct CDInfo ) ) ) ;
      if( ( ( sizeof( struct CDInfo ) ) - ( 4 * sizeof( UWORD ) /* is Reserved2 at the end of CDInfo */ ) ) <= MyCdIOStdReq->io_Length )
      {  /* correct length for cd info data */
        CopyMem( &MyDeviceUnit->du_CDInfo, MyCdIOStdReq->io_Data, MyCdIOStdReq->io_Length ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "PlaySpeed: %d", MyDeviceUnit->du_CDInfo.PlaySpeed ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "ReadSpeed: %d", MyDeviceUnit->du_CDInfo.ReadSpeed ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "ReadXLSpeed: %d", MyDeviceUnit->du_CDInfo.ReadXLSpeed ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "SectorSize: %d", MyDeviceUnit->du_CDInfo.SectorSize ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "XLECC: %d", MyDeviceUnit->du_CDInfo.XLECC ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "EjectReset: %d", MyDeviceUnit->du_CDInfo.EjectReset ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "MaxSpeed: %d", MyDeviceUnit->du_CDInfo.MaxSpeed ) ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "Status: 0x%02X", MyDeviceUnit->du_CDInfo.Status ) ) ;
      }
      else
      {  /* not correct length for cd info data */
        MyCdIOStdReq->io_Error = CDERR_BADLENGTH ;
      }
      break ;
    case CD_CONFIG:  /* 33 */
      if( 0 == MyCdScsiIOReq->csi_State )
      {  /* start command sequence */
        if( ( MyCdIOStdReq->io_Data ) && ( 0 == MyCdIOStdReq->io_Length ) )
        {  /* io data parameter valid */
          MyCdScsiIOReq->csi_State = 2 ;
          MyCdIOStdReq->io_Actual = 0 ;
        }
        else
        {  /* io data parameter not valid */
          MyCdIOStdReq->io_Error = CDERR_BADLENGTH ;
        }
      }
      MyTagItem = &( ( ( struct TagItem * )MyCdIOStdReq->io_Data )[ MyCdIOStdReq->io_Actual ] ) ;
      LOG_TEXT( ( _INFO_LEVEL, "CD_CONFIG, tagcd: 0x%04X, data: %d", MyTagItem->ti_Tag, MyTagItem->ti_Data ) ) ;
      switch( MyCdScsiIOReq->csi_State )
      {
        case 2:  /* get current related mode page or set parameter directly */
          switch( MyTagItem->ti_Tag )
          {
            case TAGCD_PLAYSPEED:
              if( ( 75 == MyTagItem->ti_Data ) || ( 150 == MyTagItem->ti_Data ) )
              {  /* valid parameter */
                ScsiModeSense06Command( MyCdScsiIOReq, 0, 0x0E ) ;
                ScsiDeviceNext = 1 ;
              }
              else
              {  /* invalid parameter */
                MyCdIOStdReq->io_Error = CDERR_NotSpecified ;                
              }
              break ;
            case TAGCD_READSPEED:
              if( ( 75 == MyTagItem->ti_Data ) || ( 150 == MyTagItem->ti_Data ) )
              {  /* valid parameter */
                MyDeviceUnit->du_CDInfo.ReadSpeed = MyTagItem->ti_Data ;
              }
              else
              {  /* invalid parameter */
                MyCdIOStdReq->io_Error = CDERR_NotSpecified ;                
              }
              break ;
            case TAGCD_READXLSPEED:
              if( ( 75 == MyTagItem->ti_Data ) || ( 150 == MyTagItem->ti_Data ) )
              {  /* valid parameter */
                MyDeviceUnit->du_CDInfo.ReadXLSpeed = MyTagItem->ti_Data ;
              }
              else
              {  /* invalid parameter */
                MyCdIOStdReq->io_Error = CDERR_NotSpecified ;                
              }
              break ;
            case TAGCD_SECTORSIZE:
              ScsiModeSense06Command( MyCdScsiIOReq, 0, 0x0E ) ;
              ScsiDeviceNext = 1 ;
              break ;
            case TAGCD_XLECC:  /* ignore for now */
              break ;
            case TAGCD_EJECTRESET:  /* ignore for now */
              break ;
            case TAG_END:  /* exit the config loop */
              MyCdScsiIOReq->csi_State = 0 ;             
            default:
              break ;
          }
          if( !( ScsiDeviceNext ) )
          {
            MyCdIOStdReq->io_Actual++ ;
          }
          break ;
        case 1: /* set the new parameters */
          switch( MyTagItem->ti_Tag )
          {
            case TAGCD_PLAYSPEED:
              ScsiModeSelect06Command( MyCdScsiIOReq, MyCdScsiIOReq->csi_SCSICmd.scsi_Actual ) ;
              ScsiDeviceNext = 1 ;
              break ;
            case TAGCD_READSPEED:
              break ;
            case TAGCD_READXLSPEED:
              break ;
            case TAGCD_SECTORSIZE:
              ScsiModeSelect06Command( MyCdScsiIOReq, MyCdScsiIOReq->csi_SCSICmd.scsi_Actual ) ;
              ScsiDeviceNext = 1 ;
              break ;
            case TAGCD_XLECC:
              break ;
            case TAGCD_EJECTRESET:
              break ;
            default:
              break ;
          }
      }
      break ;
    case CD_TOCMSF:  /* 34 */
      Error = 1 ;
      break ;
    case CD_TOCLSN:  /* 35 */
      ScsiReadTocCommand( MyCdScsiIOReq, 0 ) ;
      ScsiDeviceNext = 1 ;
      break ;
    case CD_READXL:  /* 36 */
      if( 0 == MyCdScsiIOReq->csi_State )
      {  /* start command sequence */
        LOG_TEXT( ( _INFO_LEVEL, "CD_READXL, length: %u, offset: %u", MyCdIOStdReq->io_Length,  MyCdIOStdReq->io_Offset ) ) ;
        MyCdScsiIOReq->csi_State = 2 ;
        CurrentCDXL = ( struct CDXL * )( ( struct List * )MyCdIOStdReq->io_Data )->lh_Head ;
        MyCdIOStdReq->io_Actual = 0 ;
        CurrentCDXL->Actual = 0 ;
        GetSysTime( &MyDeviceUnit->du_ReadTimeVal ) ;
        MyCdScsiIOReq->csi_CurrentReader.cr_CDXL = CurrentCDXL ;
      }
      CurrentCDXL = MyCdScsiIOReq->csi_CurrentReader.cr_CDXL ;
      switch( MyCdScsiIOReq->csi_State )
      {
        case 1:  /* timeout for read timer not handled here, so just fall through */
          break ;
        case 2:  /* fill the CDXL nodes */
          //LOG_TEXT( ( _DEBUG_LEVEL, "cdxl length: %d (%d), IntCode: %s", CurrentCDXL->Length, CurrentCDXL->Actual, CurrentCDXL->IntCode ? "yes" : "no" ) ) ;
          MyScsiIOStdReq->io_Command = CD_READ ;
          MyScsiIOStdReq->io_Error = 0 ;
          MyScsiIOStdReq->io_Actual = 0 ;
          MyScsiIOStdReq->io_Length = CurrentCDXL->Length - CurrentCDXL->Actual ;
          MyScsiIOStdReq->io_Length = ( MyScsiIOStdReq->io_Length / MyDeviceUnit->du_CDInfo.SectorSize ) * MyDeviceUnit->du_CDInfo.SectorSize ;
          if( MyScsiIOStdReq->io_Length )
          {  /* use align sector size and user supplid buffer */
            MyScsiIOStdReq->io_Data = CurrentCDXL->Buffer + CurrentCDXL->Actual ;
          }
          else
          {  /* read minimal 1 sector and private buffer */
            MyScsiIOStdReq->io_Length = MyDeviceUnit->du_CDInfo.SectorSize ;
            MyScsiIOStdReq->io_Data = MyCdScsiIOReq->csi_DataBuffer ;        
          }
          MyScsiIOStdReq->io_Offset = MyCdIOStdReq->io_Offset + MyCdIOStdReq->io_Actual ;
          ScsiDeviceNext = 1 ;
          break ;
        default:
          break ;
      }
      break ;
    case CD_PLAYTRACK:  /* 37 */
    case CD_PLAYMSF:  /* 38 */
    case CD_PLAYLSN:  /* 39 */
      if( 0 == MyCdScsiIOReq->csi_State )
      {  /* start command sequence */
        LOG_TEXT( ( _INFO_LEVEL, "%s, offset: %d, length: %d", IOCommandToString( MyCdIOStdReq->io_Command ), MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ) ;
        if( !( ( ( CD_PLAYTRACK == MyCdIOStdReq->io_Command ) && ( 0 == MyCdIOStdReq->io_Offset ) ) || ( 0 == MyCdIOStdReq->io_Length ) ) )
        {  /* parameter valid */
          if( !( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status ) )
          {  /* not already playing */
            MyDeviceUnit->du_AbortPlay = 0 ;
            switch( MyCdIOStdReq->io_Command )
            {  /* get the LSN values */
              case CD_PLAYTRACK:  /* 37 */
                StartLSN = MyDeviceUnit->du_CDTOC[ MyCdIOStdReq->io_Offset ].Entry.Position.LSN ;
                if( ( MyCdIOStdReq->io_Offset + ( MyCdIOStdReq->io_Length - 1 ) ) < MyDeviceUnit->du_CDTOC[ 0 ].Summary.LastTrack )
                {  /* not playing up to the last track */
                  LengthLSN = MyDeviceUnit->du_CDTOC[ MyCdIOStdReq->io_Offset + MyCdIOStdReq->io_Length ].Entry.Position.LSN - StartLSN ;
                }
                else
                {  /* playing up to the last track */
                  LengthLSN = MyDeviceUnit->du_CDTOC[ 0 ].Summary.LeadOut.LSN - StartLSN ;
                }
                LOG_TEXT( ( _DEBUG_LEVEL, "calculated LSNs, start: %d, length: %d", StartLSN, LengthLSN ) ) ;
                MyCdIOStdReq->io_Offset = StartLSN ;
                MyCdIOStdReq->io_Length = LengthLSN ;
                break ;
              case CD_PLAYMSF:  /* 38 */
                MyCdIOStdReq->io_Offset = MsfToLsn( MyCdIOStdReq->io_Offset ) ;
                MyCdIOStdReq->io_Offset -= ( 2 * 75 ) ;  /* substract the pregap offset */
                MyCdIOStdReq->io_Length = MsfToLsn( MyCdIOStdReq->io_Offset ) ;
                break ;
              case CD_PLAYLSN:  /* 39 */
                /* nothing to do here as values are in LSN already */
                break ;
            }
            switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
            {  /* next state depends also on audio output */
              case 0:  /* cd-rom output */
                if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
                {  /* start playing now */
                  MyCdScsiIOReq->csi_State = 3 ;
                }
                else
                {  /* start playing paused */
                  MyCdScsiIOReq->csi_State = 1 ;
                }
                break ;
              case 1:  /* ahi output */
                Error = OpenAHI( MyDeviceUnit ) ;
                if( !( Error ) )
                {  /* ahi device ok */
                  MyCdIOStdReq->io_Actual = 0 ;
                  MyCdScsiIOReq->csi_CurrentReader.cr_AHIBuffer = ( struct AHIBuffer * )RemHead( &MyDeviceUnit->du_FreeAHIBufferList ) ;
                  if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
                  {  /* start playing now */
                    MyCdScsiIOReq->csi_State = 6 ;
                  }
                  else
                  {  /* start playing paused */
                    MyCdScsiIOReq->csi_State = 5 ;
                  }
                }
                else
                {  /* ahi device not ok */
                  MyCdIOStdReq->io_Error = CDERR_NotSpecified ;
                }
                break ;
            }
          }
          else
          {  /* already playing */
            MyCdIOStdReq->io_Error = CDERR_InvalidState ;
          }
        }
        else
        {  /* parameter not valid */
          MyCdIOStdReq->io_Error = CDERR_BADLENGTH ;
        }
      }
      switch( MyCdScsiIOReq->csi_State )
      {
        case 1:  /* cd-rom output, start play paused, first the play command */
          ScsiPlayAudioCommand( MyCdScsiIOReq, MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ;
          ScsiDeviceNext = 1 ;
          break ;
        case 2:  /* cd-rom output, start play paused, second the pause command */
          ScsiPauseResumeCommand( MyCdScsiIOReq, 0 ) ;
          ScsiDeviceNext = 1 ;
          break ;
        case 3:  /* cd-rom output, play now */
          ScsiPlayAudioCommand( MyCdScsiIOReq, MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ;
          ScsiDeviceNext = 1 ;
          break ;
        case 5:  /* ahi output, start paused */
          MyAHIRequest = MyDeviceUnit->du_QuickAHIRequest ;
          MyAHIRequest->ahir_Std.io_Command = CMD_STOP ;
          DoIO( ( struct IORequest * )MyAHIRequest ) ;
          MyCdScsiIOReq->csi_State = 6 ;
        case 6:  /* ahi output play now and fill the ahi buffer */
        case 7:
          CurrentAHIBuffer = MyCdScsiIOReq->csi_CurrentReader.cr_AHIBuffer ;
          if( ( MyCdIOStdReq->io_Length ) > ( MyCdIOStdReq->io_Actual + ( AHI_BUFFER_SIZE / CDDA_SECTOR_SIZE ) ) )
          {  /* still more than 1 ahi buffer required */
            ScsiReadCdCommand( MyCdScsiIOReq,
                               ( MyCdIOStdReq->io_Offset + MyCdIOStdReq->io_Actual ),
                               ( AHI_BUFFER_SIZE / CDDA_SECTOR_SIZE ), 
                               CurrentAHIBuffer->ab_Data ) ;                  
          }
          else
          {  /* less than 1 ahi buffer required */
            ScsiReadCdCommand( MyCdScsiIOReq,
                               ( MyCdIOStdReq->io_Offset + MyCdIOStdReq->io_Actual ),
                               ( MyCdIOStdReq->io_Length - MyCdIOStdReq->io_Actual ),
                               CurrentAHIBuffer->ab_Data ) ; 
          }
          //LOG_TEXT( ( _DEBUG_LEVEL, "reading from scsi with: 0x%08X", MyScsiIOStdReq ) ) ;
          //LOG_TEXT( ( _DEBUG_LEVEL, "io_Device: 0x%08X", MyScsiIOStdReq->io_Device ) ) ;
          //LOG_TEXT( ( _DEBUG_LEVEL, "mn_ReplyPort: 0x%08X", MyScsiIOStdReq->io_Message.mn_ReplyPort ) ) ;
          ScsiDeviceNext = 1 ;
          break ;
        case 8:  /* get / wait for free ahi buffer, not handled here */
          break ;
        default:
          break ;
      }
      break ;
#if 0
    case CD_PLAYTRACK:  /* 37 */
      if( 0 == MyCdScsiIOReq->csi_State )
      {  /* start command sequence */
        LOG_TEXT( ( _DEBUG_LEVEL, "play track, offset: %d, length: %d", MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ) ;
        if( !( ( 0 == MyCdIOStdReq->io_Offset ) || ( 0 == MyCdIOStdReq->io_Length ) ) )
        {  /* parameter valid */
          if( !( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status ) )
          {  /* not already playing */
            ULONG StartLSN, LengthLSN ;
            StartLSN = MyDeviceUnit->du_CDTOC[ MyCdIOStdReq->io_Offset ].Entry.Position.LSN ;
            if( ( MyCdIOStdReq->io_Offset + ( MyCdIOStdReq->io_Length - 1 ) ) < MyDeviceUnit->du_CDTOC[ 0 ].Summary.LastTrack )
            {  /* not playing up to the last track */
              LengthLSN = MyDeviceUnit->du_CDTOC[ MyCdIOStdReq->io_Offset + MyCdIOStdReq->io_Length ].Entry.Position.LSN - StartLSN ;
            }
            else
            {  /* playing up to the last track */
              LengthLSN = MyDeviceUnit->du_CDTOC[ 0 ].Summary.LeadOut.LSN - StartLSN ;
            }
            LOG_TEXT( ( _DEBUG_LEVEL, "calculated LSNs, start: %d, length: %d", StartLSN, LengthLSN ) ) ;
            MyCdIOStdReq->io_Offset = StartLSN ;
            MyCdIOStdReq->io_Length = LengthLSN ;
            if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
            {  /* start playing now */
              MyCdScsiIOReq->csi_State = 1 ;
            }
            else
            {  /* start playing paused */
              MyCdScsiIOReq->csi_State = 2 ;
            }
          }
          else
          {  /* already playing */
            //MyCdIOStdReq->io_Error = CDERR_InvalidState ;
          }
        }
        else
        {  /* parameter not valid */
          MyCdIOStdReq->io_Error = CDERR_BADLENGTH ;
        }
      }
      switch( MyCdScsiIOReq->csi_State )
      {
        case 2:  /* first play, followed by pause */
          ScsiPlayAudioCommand( MyCdScsiIOReq, MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ;
          ScsiDeviceNext = 1 ;
          break ;
        case 1:  /* only play or pause */
          if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
          {  /* start playing now */
            ScsiPlayAudioCommand( MyCdScsiIOReq, MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ;
          }
          else
          {  /* start playing paused */
            ScsiPauseResumeCommand( MyCdScsiIOReq, 0 ) ;
          }
          ScsiDeviceNext = 1 ;
          break ;
        default:
          break ;
      }
      break ;
    case CD_PLAYMSF:  /* 38 */
      Error = 1 ;
      LOG_TEXT( ( _DEBUG_LEVEL, "start play msf" ) ) ;
      if( !( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status ) )
      {  /* not already playing */
        if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
        {  /* start playing now */
          USHORT Sum ;
          UBYTE Carry ;
          
          /* sum up tracks */
          Sum = ( ( MyCdIOStdReq->io_Offset >> 0 ) && 0xFF ) + ( ( MyCdIOStdReq->io_Length >> 0 ) && 0xFF ) ;
          if( 75 <= Sum )
          {
            Sum -= 75 ;
            Carry = 1 ;
          }
          else
          {
            Carry = 0 ;
          }
          MyCdIOStdReq->io_Length &= ~( 0xFF << 0 ) ;
          MyCdIOStdReq->io_Length |= ( Sum << 0 ) ;
          /* sum up seconds */
          Sum = ( ( MyCdIOStdReq->io_Offset >> 8 ) && 0xFF ) + ( ( MyCdIOStdReq->io_Length >> 8 ) && 0xFF ) + Carry ;
          if( 60 <= Sum )
          {
            Sum -= 60 ;
            Carry = 1 ;
          }
          else
          {
            Carry = 0 ;
          }
          MyCdIOStdReq->io_Length &= ~( 0xFF << 8 ) ;
          MyCdIOStdReq->io_Length |= ( Sum << 8 ) ;
          /* sum up minutes */
          Sum = ( ( MyCdIOStdReq->io_Offset >> 16 ) && 0xFFFF ) + ( ( MyCdIOStdReq->io_Length >> 16 ) && 0xFFFF ) + Carry ;
          MyCdIOStdReq->io_Length &= ~( 0xFFFF << 16 ) ;
          MyCdIOStdReq->io_Length |= ( Sum << 16 ) ;
          /* MyCdIOStdReq->io_Length has now the ending fields */
          ScsiPlayAudioMsfCommand( MyCdScsiIOReq, MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ;
          ScsiDeviceNext = 1 ;
        }
        else
        {  /* start playing later */
        }
      }
      else
      {  /* already playing */
        //MyCdIOStdReq->io_Error = CDERR_InvalidState ;
      }
      break ;
    case CD_PLAYLSN:  /* 39 */
      if( 0 == MyCdScsiIOReq->csi_State )
      {  /* start command sequence */
        LOG_TEXT( ( _DEBUG_LEVEL, "start play lsn, offset: %d, length: %d", MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ) ;
        if( !( ( 0 == MyCdIOStdReq->io_Offset ) || ( 0 == MyCdIOStdReq->io_Length ) ) )
        {  /* parameter valid */
          if( !( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status ) )
          {  /* not already playing */
            if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
            {  /* start playing now */
              MyCdScsiIOReq->csi_State = 1 ;
            }
            else
            {  /* start playing paused */
              MyCdScsiIOReq->csi_State = 2 ;
            }
          }
          else
          {  /* already playing */
            //MyCdIOStdReq->io_Error = CDERR_InvalidState ;
          }
        }
        else
        {  /* parameter not valid */
          MyCdIOStdReq->io_Error = CDERR_BADLENGTH ;
        }
      }
      switch( MyCdScsiIOReq->csi_State )
      {
        case 2:  /* first play, followed by pause */
          ScsiPlayAudioCommand( MyCdScsiIOReq, MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ;
          ScsiDeviceNext = 1 ;
          break ;
        case 1:  /* only play or pause */
          if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
          {  /* start playing now */
            ScsiPlayAudioCommand( MyCdScsiIOReq, MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ;
          }
          else
          {  /* start playing paused */
            ScsiPauseResumeCommand( MyCdScsiIOReq, 0 ) ;
          }
          ScsiDeviceNext = 1 ;
          break ;
        default:
          break ;
      }
      break ;
#endif
    case CD_PAUSE:  /* 40 */
      if( MyCdIOStdReq->io_Length )
      {  /* 1 = pause play */
        LOG_TEXT( ( _INFO_LEVEL, "CD_PAUSE: pause play" ) ) ;
        if( !( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) )
        {  /* not already paused */
          if( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status )
          {  /* audio is playing */
            switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
            {  /* next state depends also on audio output */
              case 0:  /* cd-rom output */
                ScsiPauseResumeCommand( MyCdScsiIOReq, 0 ) ;
                ScsiDeviceNext = 1 ;
                break ;
              case 1:  /* ahi output */ 
                MyAHIRequest = MyDeviceUnit->du_QuickAHIRequest ;
                MyAHIRequest->ahir_Std.io_Command = CMD_STOP ;
                MyCdIOStdReq->io_Error = DoIO( ( struct IORequest * )MyAHIRequest ) ;
                if( !( MyCdIOStdReq->io_Error ) )
                {
                  TranslateScsiToCd( MyCdScsiIOReq ) ;
                }
                break ;
            }
          }
          else
          {  /* audio is not playing */
            MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_PAUSED ) ;
          }
        }
        else
        {  /* already paused */
          //MyCdIOStdReq->io_Error = CDERR_InvalidState ;
        }
      }
      else
      {  /* 0 = do not pause play */
        LOG_TEXT( ( _INFO_LEVEL, "CD_PAUSE: do not pause play" ) ) ;
        if( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status )
        {  /* not already unpaused */
          if( CDSTSF_PLAYING & MyDeviceUnit->du_CDInfo.Status )
          {  /* continue playing */
            switch( ( ULONG )GetSetting( MyDeviceCore->dc_Settings, AudioOutput ) )
            {  /* next state depends also on audio output */
              case 0:  /* cd-rom output */
                ScsiPauseResumeCommand( MyCdScsiIOReq, 1 ) ;
                ScsiDeviceNext = 1 ;
                break ;
              case 1:  /* ahi output */ 
                MyAHIRequest = MyDeviceUnit->du_QuickAHIRequest ;
                MyAHIRequest->ahir_Std.io_Command = CMD_START ;
                MyCdIOStdReq->io_Error = DoIO( ( struct IORequest * )MyAHIRequest ) ;
                if( !( MyCdIOStdReq->io_Error ) )
                {
                  TranslateScsiToCd( MyCdScsiIOReq ) ;
                }
                break ;
            }
          }
          else
          {  /* audio is not playing */
            MyDeviceUnit->du_CDInfo.Status &= ~( CDSTSF_PAUSED ) ;
          }
        }
        else
        {  /* already unpaused */
          //MyCdIOStdReq->io_Error = CDERR_InvalidState ;
        }
      }
      break ;
    case CD_SEARCH:  /* 41 */
      Error = 1 ;
      break ;
    case CD_QCODEMSF:  /* 42 */
      Error = 1 ;
      break ;
    case CD_QCODELSN:  /* 43 */
      ScsiReadSubChannelCommand( MyCdScsiIOReq, 0 ) ;
      ScsiDeviceNext = 1 ;
      break ;
    case CD_ATTENUATE:  /* 44 */
      LOG_TEXT( ( _INFO_LEVEL, "CD_ATTENUATE, offset: %d, length %d", MyCdIOStdReq->io_Offset, MyCdIOStdReq->io_Length ) ) ;
      if( -1 != MyCdIOStdReq->io_Offset )
      {  /* set the volume */
        if( MyDeviceUnit->du_TargetVolume != MyDeviceUnit->du_CurrentVolume )
        {
          AlreadyFading = 1 ;
          LOG_TEXT( ( _DEBUG_LEVEL, "already fading" ) ) ;
        }
        else
        {
          AlreadyFading = 0 ;
          LOG_TEXT( ( _DEBUG_LEVEL, "not already fading" ) ) ;
        }
        MyDeviceUnit->du_TargetVolume = MyCdIOStdReq->io_Offset ;
        MyDeviceUnit->du_VolumeFadeTime = MyCdIOStdReq->io_Length ;
        if( 0 == MyDeviceUnit->du_VolumeFadeTime )
        {  /* change immediately */
          MyDeviceUnit->du_VolumeFadeTime = 1 ;
        }
        if( !( AlreadyFading ) )
        {  /* start fading */
          GetSysTime( &MyDeviceUnit->du_VolumeTimeVal ) ;
          LOG_TEXT( ( _DEBUG_LEVEL, "start fading" ) ) ;
          UpdateVolume( MyDeviceUnit ) ;
        }
      }
      MyCdIOStdReq->io_Actual = MyDeviceUnit->du_CurrentVolume ;
      break ;
    case CD_ADDFRAMEINT:  /* 45 */
      Error = 1 ;
      break ;
    case CD_REMFRAMEINT:  /* 46 */
      Error = 1 ;
      break ;
    default:  /* command not supported */
      Error = 1 ;
      break ;
  }
  if( Error )
  {
    MyCdIOStdReq->io_Error = CDERR_NOCMD ;
    ScsiDeviceNext = 0 ;
    LOG_TEXT( ( _WARNING_LEVEL, "command not supported" ) ) ;
  }

  return( ScsiDeviceNext ) ;
}



/*
 * 
 */
typedef void ( *CdxlInterrupt )( REG( a1, APTR IntData ), REG( a2, struct CDXL *CompletedNode ) ) ;
void CdxlInterruptEntry( REG( a1, struct CDXL *CompletedNode ) )
{
  //( ( CdxlInterrupt )CompletedNode->IntCode)( CompletedNode->IntData, CompletedNode ) ;
  __asm( "MOVE a1, a2" ) ;
  __asm( "LEA (20,a2), a1" ) ;
  __asm( "LEA (24,a2), a0" ) ;
  __asm( "JMP (a0)" ) ;
}


/*
 * translate a scsi io request response into a suitable cd io request response
 */
LONG TranslateScsiToCd( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct ExecBase *SysBase ;
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct IOStdReq *MyCdIOStdReq, *MyScsiIOStdReq ;
  struct SCSICmd *MySCSICmd ;
  UBYTE *MyData ;
  LONG Finished ;
  struct ModeParameterHeader06 *MyModeParameterHeader06 ;
  struct ModeParameterBlockDescriptor *MyModeParameterBlockDescriptor ;
  struct CdromAudioControlParametersPage *MyCdromAudioControlParametersPage ;
  struct CDXL *CurrentCDXL, *NextCDXL ;
  struct TagItem *MyTagItem ;
  struct Interrupt MyInterrupt ;
  struct Device *TimerBase ;
  struct timeval MyTimeVal ;
  struct AHIBuffer *CurrentAHIBuffer ;
  struct Node *MyNode ;
  ULONG Count ;
  UBYTE Temp ;
  
  MyScsiIOStdReq = &MyCdScsiIOReq->csi_ScsiIOStdReq ;
  MyCdIOStdReq = MyCdScsiIOReq->csi_CdIOStdReq ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  SysBase = MyDeviceCore->dc_SysBase ;
  TimerBase = MyDeviceUnit->du_TimerBase ;
  MySCSICmd = &MyCdScsiIOReq->csi_SCSICmd ;
  MyData = ( UBYTE * )MySCSICmd->scsi_Data ;
  Finished = 1 ;
  
  /* command depending things */
  switch( MyCdIOStdReq->io_Command )
  {
    case CD_RESET:  /* 1 */
      /* things we can just copy */
      MyCdIOStdReq->io_Error = MyScsiIOStdReq->io_Error ;
      MyCdIOStdReq->io_Actual = MyScsiIOStdReq->io_Actual ;
      break ;
    case CD_READ:  /* 2 */
      if( 0 == MyScsiIOStdReq->io_Error )
      {  /* cd read command completed ok */
        switch( MyCdScsiIOReq->csi_State )
        {
          case 1:
            if( MyCdScsiIOReq->csi_DataBuffer == MyScsiIOStdReq->io_Data )
            {  /* private buffer means minimal 1 sector was read */
              MyScsiIOStdReq->io_Actual = MyCdIOStdReq->io_Length - MyCdIOStdReq->io_Actual ;
              CopyMem( ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer, 
                        MyCdIOStdReq->io_Data + MyCdIOStdReq->io_Actual, 
                        MyScsiIOStdReq->io_Actual ) ;
            }
            MyCdIOStdReq->io_Actual += MyScsiIOStdReq->io_Actual ;
            //LOG_TEXT( ( _DEBUG_LEVEL, "cd read actual: %d (%d)", MyCdIOStdReq->io_Actual, MyCdIOStdReq->io_Length ) ) ;
            if( MyCdIOStdReq->io_Actual == MyCdIOStdReq->io_Length ) 
            {  /* requested amount of data read */
              MyCdScsiIOReq->csi_State = 0 ;
            }
            break ;
        }
      }
      break ;
    case CD_WRITE:  /* 3 */
    case CD_UPDATE:  /* 4 */
    case CD_CLEAR:  /* 5 */
    case CD_STOP:  /* 6 */
    case CD_START:  /* 7 */
    case CD_FLUSH:  /* 8 */
    case CD_MOTOR:  /* 9 */
    case CD_SEEK:  /* 10 */
    case CD_FORMAT:  /* 11 */
    case CD_REMOVE:  /* 12 */
    case CD_CHANGENUM:  /* 13 */
    case CD_CHANGESTATE:  /* 14 */
    case CD_PROTSTATUS:  /* 15 */
    case CD_GETDRIVETYPE:  /* 18 */
    case CD_GETNUMTRACKS:  /* 19 */
    case CD_ADDCHANGEINT:  /* 20 */
    case CD_REMCHANGEINT:  /* 21 */
    case CD_GETGEOMETRY:  /* 22 */
    case CD_EJECT:  /* 23 */
      /* things we can just copy */
      MyCdIOStdReq->io_Error = MyScsiIOStdReq->io_Error ;
      MyCdIOStdReq->io_Actual = MyScsiIOStdReq->io_Actual ;
      break ;
    case HD_SCSICMD:  /* 28 */
      /* things we can just copy */
      MyCdIOStdReq->io_Error = MyScsiIOStdReq->io_Error ;
      MyCdIOStdReq->io_Actual = MyScsiIOStdReq->io_Actual ;
      break ;
    case CD_INFO:  /* 32 */
      break ;
    case CD_CONFIG:  /* 33 */
      if( 0 == MySCSICmd->scsi_Status )
      {
        MyTagItem = &( ( ( struct TagItem * )MyCdIOStdReq->io_Data )[ MyCdIOStdReq->io_Actual ] ) ;
        LOG_TEXT( ( _DEBUG_LEVEL, "config state: %d, tag index: %d, tag: %d", MyCdScsiIOReq->csi_State, MyCdIOStdReq->io_Actual, MyTagItem->ti_Tag ) ) ; 
        switch( MyCdScsiIOReq->csi_State )
        {
          case 2:  /* got current mode page */
            MyModeParameterHeader06 = ( struct ModeParameterHeader06 * )MyCdScsiIOReq->csi_DataBuffer ;
            MyModeParameterBlockDescriptor = ( struct ModeParameterBlockDescriptor * )( MyModeParameterHeader06 + 1 ) ;
            MyCdromAudioControlParametersPage = ( struct CdromAudioControlParametersPage * ) ( ( MyModeParameterBlockDescriptor ) + ( MyModeParameterHeader06->BlockDescriptorLength / sizeof ( struct ModeParameterBlockDescriptor ) ) ) ;
            switch( MyTagItem->ti_Tag )
            {  /* change the settings in the realted mode page */
              case TAGCD_PLAYSPEED:
                MyCdromAudioControlParametersPage->LogicalBlocksPerSecond1 = ( UBYTE )( MyTagItem->ti_Data >> 8 ) ;
                MyCdromAudioControlParametersPage->LogicalBlocksPerSecond0 = ( UBYTE )( MyTagItem->ti_Data >> 0 ) ;
                LOG_TEXT( ( _DEBUG_LEVEL, "new Logical Blocks Per Second: %d", ( MyCdromAudioControlParametersPage->LogicalBlocksPerSecond1 << 8 ) | ( MyCdromAudioControlParametersPage->LogicalBlocksPerSecond0 << 0 ) ) )  ;
                break ;
              case TAGCD_READSPEED:
                break ;
              case TAGCD_READXLSPEED:
                break ;
              case TAGCD_SECTORSIZE:
                MyModeParameterBlockDescriptor->BlockLength2 = ( UBYTE )( MyTagItem->ti_Data >> 16 ) ;
                MyModeParameterBlockDescriptor->BlockLength1 = ( UBYTE )( MyTagItem->ti_Data >> 8 ) ;
                MyModeParameterBlockDescriptor->BlockLength0 = ( UBYTE )( MyTagItem->ti_Data >> 0 ) ;
                break ;
              case TAGCD_XLECC:
                break ;
              case TAGCD_EJECTRESET:
                break ;
              default:
                break ;
            }
            MyCdScsiIOReq->csi_State-- ;
            break ;
          case 1:  /* new values are set */
            switch( MyTagItem->ti_Tag )
            {  /* take over the values in the CDInfo structure */
              case TAGCD_PLAYSPEED:
                MyDeviceUnit->du_CDInfo.PlaySpeed = MyTagItem->ti_Data ;
                break ;
              case TAGCD_READSPEED:
                break ;
              case TAGCD_READXLSPEED:
                break ;
              case TAGCD_SECTORSIZE:
                MyDeviceUnit->du_CDInfo.SectorSize = MyTagItem->ti_Data ;
                break ;
              case TAGCD_XLECC:
                break ;
              case TAGCD_EJECTRESET:
                break ;
              default:
                break ;
            }
            MyCdScsiIOReq->csi_State++ ;
            MyCdIOStdReq->io_Actual++ ;
            break ;
        }
      }
      else
      {  /* scsi command completed not ok */
        MyCdScsiIOReq->csi_State = 0 ;
        MyCdIOStdReq->io_Error = CDERR_NotSpecified ;
      }
      break ;
    case CD_TOCMSF:  /* 34 */
      break ;
    case CD_TOCLSN:  /* 35 */
      MyCdIOStdReq->io_Error = ScsiReadTocResult( MyCdScsiIOReq ) ;
      break ;
    case CD_READXL:  /* 36 */
      if( 0 == MyScsiIOStdReq->io_Error )
      {  /* cd read command completed ok */
        CurrentCDXL = MyCdScsiIOReq->csi_CurrentReader.cr_CDXL ;
        switch( MyCdScsiIOReq->csi_State )
        {
          case 1:  /* read timer timed out */
            NextCDXL = ( struct CDXL * )( ( struct Node * )CurrentCDXL )->ln_Succ ;
            if( CurrentCDXL->IntCode )
            {  /* call the interrupt */
              ( ( CdxlInterrupt )CurrentCDXL->IntCode )( CurrentCDXL->IntData, CurrentCDXL ) ;
            }
            if( ( ( MyCdIOStdReq->io_Length ) && ( MyCdIOStdReq->io_Actual < MyCdIOStdReq->io_Length ) ) ||
                ( !( MyCdIOStdReq->io_Length ) && ( ( ( struct Node *)NextCDXL )->ln_Succ ) ) )
              {  /* there are more cdxl nodes to go */
                //LOG_TEXT( ( _DEBUG_LEVEL, "switching to next CDXL" ) ) ;
                CurrentCDXL = NextCDXL ;
                if( MyCdScsiIOReq->csi_DataBuffer == MyScsiIOStdReq->io_Data )
                {  /* we might have some data left in the private buffer, copy to next cdxl buffer */
                  CopyMem( ( ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer ) + MyScsiIOStdReq->io_Actual , 
                           CurrentCDXL->Buffer, 
                           MyDeviceUnit->du_CDInfo.SectorSize - MyScsiIOStdReq->io_Actual ) ;
                  CurrentCDXL->Actual = MyDeviceUnit->du_CDInfo.SectorSize - MyScsiIOStdReq->io_Actual ;
                  MyCdIOStdReq->io_Actual += CurrentCDXL->Actual ;
                }
                else
                {  /* no data left in the private buffer */
                  CurrentCDXL->Actual = 0 ;
                }
                MyCdScsiIOReq->csi_CurrentReader.cr_CDXL = CurrentCDXL ;
                MyCdScsiIOReq->csi_State++ ;  /* read more in the next round */
              }
              else
              {  /* there is not more to read */
                //LOG_TEXT( ( _DEBUG_LEVEL, "CD_READXL actual: %d (%d)", MyCdIOStdReq->io_Actual, MyCdIOStdReq->io_Length ) ) ;
                MyCdScsiIOReq->csi_State = 0 ;
              }
            break ;
          case 2:  /* read command finished */
            if( MyCdScsiIOReq->csi_DataBuffer == MyScsiIOStdReq->io_Data )
            {  /* private buffer means minimal 1 sector was read */
              MyScsiIOStdReq->io_Actual = CurrentCDXL->Length - CurrentCDXL->Actual ;
              CopyMem( ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer, 
                       CurrentCDXL->Buffer + CurrentCDXL->Actual, 
                       MyScsiIOStdReq->io_Actual ) ;
            }
            CurrentCDXL->Actual += MyScsiIOStdReq->io_Actual ;
            LOG_TEXT( ( _DEBUG_LEVEL, "CDXL actual: %d (%d)", CurrentCDXL->Actual, CurrentCDXL->Length ) ) ;
            MyCdIOStdReq->io_Actual += MyScsiIOStdReq->io_Actual ;
            if( CurrentCDXL->Actual == CurrentCDXL->Length )
            {  /* cdxl node read completed */
              MyDeviceUnit->du_ReadCdScsiIOReq = MyCdScsiIOReq ;
              MyTimeVal.tv_secs = 0 ;
              MyTimeVal.tv_micro = 1000000UL / ( MyDeviceUnit->du_CDInfo.ReadXLSpeed ) * CurrentCDXL->Actual / MyDeviceUnit->du_CDInfo.SectorSize ;
              AddTime( &MyDeviceUnit->du_ReadTimeVal, &MyTimeVal ) ;
              MyDeviceUnit->du_ReadTimeRequest.tr_time = MyDeviceUnit->du_ReadTimeVal ;
              MyDeviceUnit->du_ReadTimeRequest.tr_node.io_Command = TR_ADDREQUEST ;
              SendIO( ( struct IORequest * )&MyDeviceUnit->du_ReadTimeRequest ) ;  /* sync with readxl read speed */
              MyCdScsiIOReq->csi_State-- ;  /* finish the cdxl node next */
              Finished = 0 ;
            }
            else
            {  /* cdxl node read not yet completed */
              /* just continue reading in the next round */
            }
            break ;
          default:
            break ;
        }
      }
      else
      {  /* cd read command completed not ok */
        MyCdScsiIOReq->csi_State = 0 ;
        MyCdIOStdReq->io_Error = CDERR_NotSpecified ;
      }
      break ;
    case CD_PLAYTRACK:  /* 37 */
    case CD_PLAYMSF:  /* 38 */
    case CD_PLAYLSN:  /* 39 */
      //LOG_TEXT( ( _DEBUG_LEVEL, "play cdscsi io request state: %d", MyCdScsiIOReq->csi_State ) ) ;
      //LOG_TEXT( ( _DEBUG_LEVEL, "scsi_Status: %d, io_Error: %d", MySCSICmd->scsi_Status, MyScsiIOStdReq->io_Error ) ) ;      
      if( ( 0 == MySCSICmd->scsi_Status ) || ( MyDeviceUnit->du_AbortPlay ) )
      {  /* start play or read command completed ok */
        switch( MyCdScsiIOReq->csi_State )
        {
          case 1:  /* start play paused, pause command next */
            MyCdScsiIOReq->csi_State = 2 ;
          case 2:  /* pause command finished */
          case 3:  /* play command finished */
            Finished = 0 ;
            MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_PLAYING ) ;
            MyDeviceUnit->du_PlayCdScsiIOReq = MyCdScsiIOReq ;
            MyCdScsiIOReq->csi_State = 4 ;
            break ;
          case 4:  /* wait for play to finish */
            LOG_TEXT( ( _DEBUG_LEVEL, "cdrom play actual: %d (%d)", MyCdIOStdReq->io_Actual, MyCdIOStdReq->io_Length ) ) ;
            if( ( MyCdIOStdReq->io_Length ) <= ( MyCdIOStdReq->io_Actual ) || ( MyDeviceUnit->du_AbortPlay ) )
            {  /* play finished */
              MyDeviceUnit->du_PlayCdScsiIOReq = NULL ;
              MyDeviceUnit->du_CDInfo.Status &= ~( CDSTSF_PLAYING ) ;
              MyCdScsiIOReq->csi_State = 0 ;
            }
            else
            {  /* play not finished */
              Finished = 0 ;
            }
            break ;
          case 5:
          case 6:  /* read cd command finished, feed ahi with the data */
            LOG_TEXT( ( _DEBUG_LEVEL, "now playing via ahi" ) ) ;
            MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_PLAYING ) ;
            MyDeviceUnit->du_PlayCdScsiIOReq = MyCdScsiIOReq ;
            MyCdScsiIOReq->csi_State = 7 ;  /* next is feeding ahi */
          case 7:
            if( !( MyDeviceUnit->du_AbortPlay ) )
            {  /* only feed ahi if play was not aborted */
              CurrentAHIBuffer = MyCdScsiIOReq->csi_CurrentReader.cr_AHIBuffer ;
              MyCdIOStdReq->io_Actual += MyCdScsiIOReq->csi_SCSICmd.scsi_Actual / CDDA_SECTOR_SIZE ;
              LOG_TEXT( ( _DEBUG_LEVEL, "ahi audio read actual: %d (%d)", MyCdIOStdReq->io_Actual, MyCdIOStdReq->io_Length ) ) ;
              LOG_TEXT( ( _DEBUG_LEVEL, "current: 0x%08X, last: 0x%08X ahi buffers", CurrentAHIBuffer, MyDeviceUnit->du_LastAHIBuffer ) ) ;
              CurrentAHIBuffer->ab_AHIRequest.ahir_Std.io_Command = CMD_WRITE ;
              CurrentAHIBuffer->ab_AHIRequest.ahir_Std.io_Data = CurrentAHIBuffer->ab_Data ;
              CurrentAHIBuffer->ab_AHIRequest.ahir_Std.io_Length = MyCdScsiIOReq->csi_SCSICmd.scsi_Actual ;
              MyData = ( UBYTE * )CurrentAHIBuffer->ab_Data ;
              for( Count = 0 ; Count < CurrentAHIBuffer->ab_AHIRequest.ahir_Std.io_Length ; Count += 2 )
              {  /* swap endianess from littel to big */
                Temp = MyData[ Count ] ;
                MyData[ Count ] = MyData[ Count + 1 ] ;
                MyData[ Count + 1 ] = Temp ;
              }
              CurrentAHIBuffer->ab_AHIRequest.ahir_Std.io_Offset = 0 ;
              CurrentAHIBuffer->ab_AHIRequest.ahir_Type = AHIST_S16S ;
              CurrentAHIBuffer->ab_AHIRequest.ahir_Frequency = 44100 ;
              CurrentAHIBuffer->ab_AHIRequest.ahir_Volume = 0x10000 * MyDeviceUnit->du_CurrentVolume / MyDeviceUnit->du_CDInfo.AudioPrecision ;
              CurrentAHIBuffer->ab_AHIRequest.ahir_Position = 0x8000 ;  /* centered */
              CurrentAHIBuffer->ab_AHIRequest.ahir_Link = ( struct AHIRequest * )MyDeviceUnit->du_LastAHIBuffer ;
              //LOG_TEXT( ( _DEBUG_LEVEL, "feeding ahi with: 0x%08X", CurrentAHIBuffer ) ) ;
              //LOG_TEXT( ( _DEBUG_LEVEL, "io_Device: 0x%08X", CurrentAHIBuffer->ab_AHIRequest.ahir_Std.io_Device ) ) ;
              //LOG_TEXT( ( _DEBUG_LEVEL, "mn_ReplyPort: 0x%08X", CurrentAHIBuffer->ab_AHIRequest.ahir_Std.io_Message.mn_ReplyPort ) ) ;
              SendIO( ( struct IORequest * )CurrentAHIBuffer ) ;
              MyCdScsiIOReq->csi_CurrentReader.cr_AHIBuffer = NULL ;
              MyDeviceUnit->du_LastAHIBuffer = CurrentAHIBuffer ;
            }
            MyCdScsiIOReq->csi_State = 8 ;  /* next is get next ahi buffer */
          case 8:
            if( ( ( MyCdIOStdReq->io_Length ) <= ( MyCdIOStdReq->io_Actual ) ) || ( MyDeviceUnit->du_AbortPlay ) )
            {  /* already read what was requested */
              Count = ( NULL != MyCdScsiIOReq->csi_CurrentReader.cr_AHIBuffer ) ;
              for( MyNode = ( &MyDeviceUnit->du_FreeAHIBufferList )->lh_Head ; NULL != MyNode->ln_Succ ; MyNode = MyNode->ln_Succ )
              {  /* count free ahi buffers */
                Count++ ;
              }
              LOG_TEXT( ( _DEBUG_LEVEL, "free ahi buffers: %d", Count ) ) ;
              if( NUM_AHI_BUFFERS == Count )
              {  /* all ahi buffers returned, so play has finished */
                MyDeviceUnit->du_PlayCdScsiIOReq = NULL ;
                MyDeviceUnit->du_CDInfo.Status &= ~( CDSTSF_PLAYING ) ;
                CloseAHI( MyDeviceUnit ) ;
                MyCdScsiIOReq->csi_State = 0 ;
              }
              else
              {  /* wait for remaing ahi buffers to be returned */
                Finished = 0 ;
              }
            }
            else
            {  /* still more requested than read */
              MyCdScsiIOReq->csi_CurrentReader.cr_AHIBuffer = ( struct AHIBuffer * )RemHead( &MyDeviceUnit->du_FreeAHIBufferList ) ;
              if( NULL != MyCdScsiIOReq->csi_CurrentReader.cr_AHIBuffer )
              {  /* there is still some free ahi buffer */
                LOG_TEXT( ( _DEBUG_LEVEL, "switching to next ahi buffer" ) ) ;
                MyCdScsiIOReq->csi_State = 7 ;  /* next is fill the buffer */
              }
              else
              {  /* there was no free ahi buffer */
                LOG_TEXT( ( _DEBUG_LEVEL, "waiting for next ahi buffer" ) ) ;
                Finished = 0 ;  /* wait for ahi to return a buffer */
              }
            }
            break ;
          default:
            break ;
        }
      }
      else
      {  /* start play command completed not ok */
        MyCdScsiIOReq->csi_State = 0 ;
        MyCdIOStdReq->io_Error = CDERR_NotSpecified ;
      }
      break ;
    case CD_PAUSE:  /* 40 */
      MyCdIOStdReq->io_Actual = 1U && ( CDSTSF_PAUSED & MyDeviceUnit->du_CDInfo.Status ) ;
      if( 0 == MySCSICmd->scsi_Status )
      {  /* pause command completed ok */
        if( MyCdIOStdReq->io_Length )
        {  /* 1 = pause play */
          MyDeviceUnit->du_CDInfo.Status |= ( CDSTSF_PAUSED ) ;
        }
        else
        {  /* 0 = do not pause play */
          MyDeviceUnit->du_CDInfo.Status &= ~( CDSTSF_PAUSED ) ;
        }
      }
      else
      {  /* pause command completed not ok */
        MyCdIOStdReq->io_Error = CDERR_NotSpecified ;
      }
      break ;
    case CD_SEARCH:  /* 41 */
      break ;
    case CD_QCODEMSF:  /* 42 */
      break ;
    case CD_QCODELSN:  /* 43 */
      MyCdIOStdReq->io_Error = ScsiReadSubChannelResult( MyCdScsiIOReq ) ;
      break ;
    case CD_ATTENUATE:  /* 44 */
      break ;
    case CD_ADDFRAMEINT:  /* 45 */
      break ;
    case CD_REMFRAMEINT:  /* 46 */
      break ;
    default:  /* command not supported */
      break ;
  }
  
  return( Finished ) ;
}

