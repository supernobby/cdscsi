/*
** ScsiCommands.h
*/


#ifndef _SCSICOMMANDS_H
#define _SCSICOMMANDS_H


#include <exec/types.h>


/*
 * SCSI opperation codes
 */
#define SCSI_TEST_UNIT_READY ( 0x00 )
#define SCSI_REQUEST_SENSE ( 0x03 )
#define SCSI_INQUIRY ( 0x12 )
#define SCSI_MODE_SELECT06 ( 0x15 )
#define SCSI_MODE_SENSE06 ( 0x1A )
#define SCSI_START_STOP_UNIT ( 0x1B )
#define SCSI_READ_CDROM_CAPACITY ( 0x25 )
#define SCSI_READ10 ( 0x28 )
#define SCSI_SEEK10 (0x2B )
#define SCSI_READ_SUBCHANNEL ( 0x42 )
#define SCSI_READ_TOC ( 0x43 )
#define SCSI_READ_HEADER ( 0x44 )
#define SCSI_PLAY_AUDIO10 ( 0x45 )
#define SCSI_PLAY_AUDIO_MSF ( 0x47 )
#define SCSI_PLAY_AUDIO_TRACK_INDEX ( 0x48 )
#define SCSI_PLAY_TRACK_REALIVE10 ( 0x49 )
#define SCSI_PAUSE_RESUME ( 0x4B )
#define SCSI_PLAY_AUDIO12 ( 0xA5 )
#define SCSI_PLAY_TRACK_REALIVE12 ( 0xA9 )
#define SCSI_READ_CD ( 0xBE )


/*
 * SCSI status codes
 */
#define SCSI_STATUS_GOOD ( 0x00U )
#define SCSI_STATUS_CHECK_CONDITION ( 0x02U )
#define SCSI_STATUS_CONDITION_MET ( 0x04U )
#define SCSI_STATUS_BUSY ( 0x08U )
#define SCSI_STATUS_INTERMEDIATE ( 0x10U )
#define SCSI_STATUS_INTERMEDIATE_CONDITION_MET ( 0x14U )
#define SCSI_STATUS_RESERVATION_CONFLICT ( 0x18U )
#define SCSI_STATUS_COMMAND_TERMINATED ( 0x22U )
#define SCSI_STATUS_QUEUE_FULL ( 0x28U )



/*
 * sense data value macros
 */
#define SCSI_SENSE_DATA_VALID( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 0 ] ) & 0x80 ) >> 7 ) 
#define SCSI_SENSE_DATA_ERROR_CODE( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 0 ] ) & 0x7F ) >> 0 ) 
#define SCSI_SENSE_DATA_SEGMENT_NUMBER( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 1 ] ) & 0xFF ) >> 0 ) 
#define SCSI_SENSE_DATA_ILI( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 2 ] ) & 0x20 ) >> 5 ) 
#define SCSI_SENSE_DATA_SENSE_KEY( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 2 ] ) & 0x0F ) >> 0 ) 
#define SCSI_SENSE_DATA_INFORMATION( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 3 ] ) << 24 ) | \
                                                       ( ( ( ( UBYTE * )_Buffer )[ 4 ] ) << 16 ) | \
                                                       ( ( ( ( UBYTE * )_Buffer )[ 5 ] ) << 8 ) | \
                                                       ( ( ( ( UBYTE * )_Buffer )[ 6 ] ) << 0 ) )
#define SCSI_SENSE_DATA_ADDITIONAL_SENSE_LENGTH( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 7 ] ) & 0xFF ) >> 0 ) 
#define SCSI_SENSE_DATA_COMMAND_SPECIFIC_INFORMTION( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 8 ] ) << 24 ) | \
                                                                 ( ( ( ( UBYTE * )_Buffer )[ 9 ] ) << 16 ) | \
                                                                 ( ( ( ( UBYTE * )_Buffer )[ 10 ] ) << 8 ) | \
                                                                 ( ( ( ( UBYTE * )_Buffer )[ 11 ] ) << 0 ) )
#define SCSI_SENSE_DATA_ADDITIONAL_SENSE_CODE( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 12 ] ) & 0xFF ) >> 0 ) 
#define SCSI_SENSE_DATA_ADDITIONAL_SENSE_CODE_QUALIFIER( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 13 ] ) & 0xFF ) >> 0 ) 
#define SCSI_SENSE_DATA_SKSV( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 15 ] ) & 0x80 ) >> 7 ) 


/*
 * inquiry data value macros
 */
#define SCSI_INQUIRY_DATA_PERIPHERAL_DEVICE_TYPE( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 0 ] ) & 0x1F ) >> 0 ) 
#define SCSI_INQUIRY_DATA_VENDOR_IDENTIFICATION( _Buffer ) ( &( ( ( UBYTE * )_Buffer )[ 8 ] ) )


/*
 * sub channel data value macros
 */
#define SCSI_SUB_CHANNEL_DATA_AUDIO_STATUS( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 1 ] )
#define SCSI_SUB_CHANNEL_DATA_FORMAT_CODE( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 4 ] ) & 0x0F ) >> 0 )
#define SCSI_SUB_CHANNEL_DATA_ADR( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 5 ] ) & 0xF0 ) >> 4 ) 
#define SCSI_SUB_CHANNEL_DATA_CONTROL( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 5 ] ) & 0x0F ) >> 0 ) 
#define SCSI_SUB_CHANNEL_DATA_TRACK_NUMBER( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 6 ] )
#define SCSI_SUB_CHANNEL_DATA_INDEX_NUMBER( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 7 ] )
#define SCSI_SUB_CHANNEL_DATA_ABSOLUTE_ADDRESS( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 8 ] ) << 24 ) | \
                                                            ( ( ( ( UBYTE * )_Buffer )[ 9 ] ) << 16 ) | \
                                                            ( ( ( ( UBYTE * )_Buffer )[ 10 ] ) << 8 ) | \
                                                            ( ( ( ( UBYTE * )_Buffer )[ 11 ] ) << 0 ) )
#define SCSI_SUB_CHANNEL_DATA_TRACK_RELATIVE_ADDRESS( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 12 ] ) << 24 ) | \
                                                                  ( ( ( ( UBYTE * )_Buffer )[ 13 ] ) << 16 ) | \
                                                                  ( ( ( ( UBYTE * )_Buffer )[ 14 ] ) << 8 ) | \
                                                                  ( ( ( ( UBYTE * )_Buffer )[ 15 ] ) << 0 ) )





#define SCSI_MODE_PARAMETER_HEADER_MODE_DATA_LENGTH( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 0 ] )
#define SCSI_MODE_PARAMETER_HEADER_MEDIUM_TYPE( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 1 ] )
#define SCSI_MODE_PARAMETER_HEADER_DEVICE_SPECIFIC_PARAMETER( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 2 ] )
#define SCSI_MODE_PARAMETER_HEADER_BLOCK_DESCRIPTOR_LENGTH( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 3 ] )

#define SCSI_BLOCK_DESCRIPTOR_DENSITY_CODE( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 0 ] )
#define SCSI_BLOCK_DESCRIPTOR_NUMBER_OF_BLOCKS( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 1 ] ) << 16 ) | \
                                                            ( ( ( ( UBYTE * )_Buffer )[ 2 ] ) << 8 ) | \
                                                            ( ( ( ( UBYTE * )_Buffer )[ 3 ] ) << 0 ) )
#define SCSI_BLOCK_DESCRIPTOR_BLOCK_LENGTH( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 5 ] ) << 16 ) | \
                                                      ( ( ( ( UBYTE * )_Buffer )[ 6 ] ) << 8 ) | \
                                                      ( ( ( ( UBYTE * )_Buffer )[ 7 ] ) << 0 ) )


#define SCSI_PAGE_AUDIO_CONTOL_PS( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 0 ] ) & 0x80 ) >> 7 )
#define SCSI_PAGE_AUDIO_CONTOL_PAGE_CODE( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 0 ] ) & 0x3F ) >> 0 )
#define SCSI_PAGE_AUDIO_CONTOL_PARAMETER_LENGTH( _Buffer ) ( ( ( UBYTE * )_Buffer )[ 1 ] )
#define SCSI_PAGE_AUDIO_CONTOL_IMMED( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 2 ] ) & 0x04 ) >> 2 )
#define SCSI_PAGE_AUDIO_CONTOL_SOTC( _Buffer ) ( ( ( ( ( UBYTE * )_Buffer )[ 2 ] ) & 0x02 ) >> 1 )
#define SCSI_PAGE_AUDIO_CONTOL_LOGICAL_BLOCKS_PER_SECOND( _Buffer )  ( ( ( ( ( UBYTE * )_Buffer )[ 6 ] ) << 8 ) | \
                                                                       ( ( ( ( UBYTE * )_Buffer )[ 7 ] ) << 0 ) )



struct ModeParameterHeader06
{
  /* byte 0 */
  UBYTE ModeDataLength ;
  /* byte 1 */
  UBYTE MediumType ;
  /* byte 2 */
  UBYTE DeviceSpecificParameter ;
  /* byte 3 */
  UBYTE BlockDescriptorLength ;
} ;


struct ModeParameterBlockDescriptor
{
  /* byte 0 */
  UBYTE DensityCode ;
  /* byte 1 */
  UBYTE NumberOfBlocks2 ;
  /* byte 2 */
  UBYTE NumberOfBlocks1 ;
  /* byte 3 */
  UBYTE NumberOfBlocks0 ;
  /* byte 4 */
  UBYTE Reserved0 ;
  /* byte 5 */
  UBYTE BlockLength2 ;
  /* byte 6 */
  UBYTE BlockLength1 ;
  /* byte 7 */
  UBYTE BlockLength0 ;
} ;


struct CdromAudioControlParametersPage
{
  /* byte 0 */
  UBYTE PS : 1 ;
  UBYTE Reserved0 : 1 ;
  UBYTE PageCode : 6 ;
  /* byte 1 */
  UBYTE ParameterLength ;
  /* byte 2 */
  UBYTE Reserved1 : 5 ;
  UBYTE Immed : 1 ;
  UBYTE SOTC : 1 ;
  UBYTE Reserved2 : 1 ;
  /* byte 3 */
  UBYTE Reserved3 ;
  /* byte 4 */
  UBYTE Reserved4 ;
  /* byte 5 */
  UBYTE APRVal : 1 ;
  UBYTE Reserved5 : 3 ;
  UBYTE FormatOfLogicalBlocksAddresesPerSec : 4 ;
  /* byte 6 */
  UBYTE LogicalBlocksPerSecond1 ;
  /* byte 7 */
  UBYTE LogicalBlocksPerSecond0 ;
  /* byte 8 */
  UBYTE Reserved6 : 4 ;
  UBYTE OutputPort0ChannelSelection : 4 ;
  /* byte 9 */
  UBYTE OutputPort0Volume ;
  /* byte 10 */
  UBYTE Reserved7 : 4 ;
  UBYTE OutputPort1ChannelSelection : 4 ;
  /* byte 11 */
  UBYTE OutputPort1Volume ;
  /* byte 12 */
  UBYTE Reserved8 : 4 ;
  UBYTE OutputPort2ChannelSelection : 4 ;
  /* byte 13 */
  UBYTE OutputPort2Volume ;
  /* byte 14 */
  UBYTE Reserved9 : 4 ;
  UBYTE OutputPort3ChannelSelection : 4 ;
  /* byte 15 */
  UBYTE OutputPort3Volume ;  
} ;





/*
 * some forward declarations to avoid recursive inludes
 */
struct CdScsiIOReq ;


/*
 * module functions
 */
STRPTR ScsiCommandToString( ULONG ScsiCommand ) ;
void PrintSenseData( struct CdScsiIOReq *MyCdScsiIOReq ) ;
void ScsiInquiryCommand( struct CdScsiIOReq *MyCdScsiIOReq ) ;
void ScsiModeSense06Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG PageControl, ULONG PageCode ) ;
void ScsiModeSelect06Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG ParameterListLength ) ;
void ScsiReadCdromCapacityCommand( struct CdScsiIOReq *MyCdScsiIOReq ) ;
void ScsiRead10Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG LogicalBlockAddress, ULONG TransferLength ) ;
void ScsiSeek10Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG LogicalBlockAddress ) ;
void ScsiPauseResumeCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG Resume ) ;
void ScsiPlayAudioCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartAddress, ULONG TransferLength ) ;
void ScsiPlayAudioMsfCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartingFields, ULONG EndingFields ) ;
void ScsiPlayAudioTrackIndexCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartingTrack, ULONG EndingTrack ) ;
void ScsiReadSubChannelCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG MSF ) ;
LONG ScsiReadSubChannelResult( struct CdScsiIOReq *MyCdScsiIOReq ) ;
void ScsiReadTocCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG MSF ) ;
LONG ScsiReadTocResult( struct CdScsiIOReq *MyCdScsiIOReq ) ;
void ScsiStartStopUnitCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG LoEj, ULONG Start ) ;
void ScsiTestUnitReadyCommand( struct CdScsiIOReq *MyCdScsiIOReq ) ;
void ScsiReadCdCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartAddress, ULONG TransferLength, UWORD *Buffer ) ;


#endif   /* _SCSICOMMANDS_H */
