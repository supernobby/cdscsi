/*
** DeviceCore.c
*/


#ifndef _DEVICECORE_H
#define _DEVICECORE_H


#include "Version.h"
#include <exec/execbase.h>
#include <exec/lists.h>
#include <dos/dos.h>


/*
** some forward declarations to avoid recursive inludes
*/
struct DeviceUnit ;
struct Settings ;
struct LogOutput ;


/*
** the private base structure
*/
struct DeviceCore
{
  struct Library dc_Library ;  /* we are actually a library */
  /* private data */
  struct ExecBase *dc_SysBase ;  /* pointer to exec libaray */
  BPTR dc_SegList ;  /* pointer to our segment list */
  struct DosLibrary *dc_DOSBase ;  /* pointer to dos library */
  struct DeviceUnit *dc_DeviceUnit ;  /* list of units */
  struct Settings *dc_Settings ;  /* settings */
  struct LogOutput *dc_LogOutput ;  /* log output */
} ;


#endif   /* _DEVICECORE_H */
