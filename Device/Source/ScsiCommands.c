/*
 * ScsiCommands.c
 */


#include "ScsiCommands.h"
#include "DeviceUnit.h"
#include "DeviceCore.h"
#include "LogOutput.h"
#include <devices/scsidisk.h>



STRPTR ScsiCommandToString( ULONG ScsiCommand )
{
  STRPTR CommandString ;

  switch( ScsiCommand )
  {
    case SCSI_TEST_UNIT_READY:  /* ( 0x00 ) */
      CommandString = "SCSI_TEST_UNIT_READY" ;
      break;
    case SCSI_REQUEST_SENSE:  /* ( 0x03 ) */
      CommandString = "SCSI_REQUEST_SENSE" ;
      break;
    case SCSI_INQUIRY:  /* ( 0x12 ) */
      CommandString = "SCSI_INQUIRY" ;
      break;
    case SCSI_MODE_SELECT06:  /* ( 0x15 ) */
      CommandString = "SCSI_MODE_SELECT06" ;
      break;
    case SCSI_MODE_SENSE06:  /* ( 0x1A ) */
      CommandString = "SCSI_MODE_SENSE06" ;
      break;
    case SCSI_START_STOP_UNIT:  /* ( 0x1B ) */
      CommandString = "SCSI_START_STOP_UNIT" ;
      break ;
    case SCSI_READ_CDROM_CAPACITY:  /* ( 0x25 ) */
      CommandString = "SCSI_READ_CDROM_CAPACITY" ;
      break ;
    case SCSI_READ10:  /* ( 0x28 ) */
      CommandString = "SCSI_READ10" ;
      break ;
    case SCSI_SEEK10:  /* ( 0x2B ) */
      CommandString = "SCSI_SEEK10" ;
      break ;
    case SCSI_READ_SUBCHANNEL:  /* ( 0x42 ) */
      CommandString = "SCSI_READ_SUBCHANNEL" ;
      break;
    case SCSI_READ_TOC:  /* ( 0x43 ) */
      CommandString = "SCSI_READ_TOC" ;
      break;
    case SCSI_PLAY_AUDIO10:  /* ( 0x45 ) */
      CommandString = "SCSI_PLAY_AUDIO10" ;
      break;
    case SCSI_PLAY_AUDIO_MSF:  /* ( 0x47 ) */
      CommandString = "SCSI_PLAY_AUDIO_MSF" ;
      break;
    case SCSI_PLAY_AUDIO_TRACK_INDEX:  /* ( 0x48 ) */
      CommandString = "SCSI_PLAY_AUDIO_TRACK_INDEX" ;
      break;
    case SCSI_PAUSE_RESUME:  /* ( 0x4B ) */
      CommandString = "SCSI_PAUSE_RESUME" ;
      break;
    case SCSI_READ_CD:  /* ( 0xBE ) */
      CommandString = "SCSI_READ_CD" ;
      break;
    default:
      CommandString = "unknown" ;
      break ;
  }
  
  return( CommandString ) ;
}


STRPTR ScsiSenseKeyToString( ULONG ScsiCommand )
{
  STRPTR SenseKeyString ;

  switch( ScsiCommand )
  {
    case 0x00:
      SenseKeyString = "NO SENSE" ;
      break;
    case 0x01:
      SenseKeyString = "RECOVERED ERROR" ;
      break;
    case 0x02:
      SenseKeyString = "NOT READY" ;
      break;
    case 0x03:
      SenseKeyString = "MEDIUM ERROR" ;
      break;
    case 0x04:
      SenseKeyString = "HARDWARE ERROR" ;
      break;
    case 0x05:
      SenseKeyString = "ILLEGAL REQUEST" ;
      break;
    case 0x06:
      SenseKeyString = "UNIT ATTENTION" ;
      break;
    case 0x07:
      SenseKeyString = "DATA PROTECT" ;
      break;
    case 0x08:
      SenseKeyString = "BLANK CHECK" ;
      break;
    case 0x09:
      SenseKeyString = "VENDOR-SPECIFIC" ;
      break;
    case 0x0A:
      SenseKeyString = "COPY ABORTED" ;
      break;
    case 0x0B:
      SenseKeyString = "ABORTED COMMAND" ;
      break;
    case 0x0C:
      SenseKeyString = "EQUAL" ;
      break;
    case 0x0D:
      SenseKeyString = "VOLUME OVERFLOW" ;
      break;
    case 0x0E:
      SenseKeyString = "MISCOMPARE" ;
      break;
    case 0x0F:
      SenseKeyString = "RESERVED" ;
      break;
    default:
      SenseKeyString = "unknown" ;
      break ;
  }
  
  return( SenseKeyString ) ;
}

      
/*
 * log sense data
 */
void PrintSenseData( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct DeviceCore *MyDeviceCore ;
  struct DeviceUnit *MyDeviceUnit ;
  struct IOStdReq *MyCdIOStdReq, *MyScsiIOStdReq ;
  struct SCSICmd *MySCSICmd ;
  UBYTE *MyData ;

  MyScsiIOStdReq = &MyCdScsiIOReq->csi_ScsiIOStdReq ;
  MyCdIOStdReq = MyCdScsiIOReq->csi_CdIOStdReq ;
  MyDeviceUnit = ( ( struct DeviceOpener *)MyCdIOStdReq->io_Unit )->do_DeviceUnit ;
  MyDeviceCore = MyDeviceUnit->du_DeviceCore ;
  MySCSICmd = &MyCdScsiIOReq->csi_SCSICmd ;
  MyData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  
  LOG_TEXT( ( _DEBUG_LEVEL, "scsi_SenseActual: %d", MySCSICmd->scsi_SenseActual ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "Valid: %d", SCSI_SENSE_DATA_VALID( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "ILI: %d", SCSI_SENSE_DATA_ILI( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "Sense key: %s(0x%02X)", ScsiSenseKeyToString( ( ULONG )SCSI_SENSE_DATA_SENSE_KEY( MyData ) ), SCSI_SENSE_DATA_SENSE_KEY( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "Information: %d", SCSI_SENSE_DATA_INFORMATION( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "Additional sense length: %d", SCSI_SENSE_DATA_ADDITIONAL_SENSE_LENGTH( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "Command-specific information: %d", SCSI_SENSE_DATA_COMMAND_SPECIFIC_INFORMTION( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "Additional sense code: 0x%02X", SCSI_SENSE_DATA_ADDITIONAL_SENSE_CODE( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "Additional sense code qualifier: 0x%02X", SCSI_SENSE_DATA_ADDITIONAL_SENSE_CODE_QUALIFIER( MyData ) ) ) ;
  LOG_TEXT( ( _DEBUG_LEVEL, "SKSV: %d", SCSI_SENSE_DATA_SKSV( MyData ) ) ) ;

}


/*
 * common setup for SCSI IO Requests
 */
static void ScsiIORequest( struct CdScsiIOReq *MyCdScsiIOReq, ULONG CmdLength, ULONG DataLength, ULONG Flags )
{
  if( DataLength )
  {
    MyCdScsiIOReq->csi_SCSICmd.scsi_Data = MyCdScsiIOReq->csi_DataBuffer ;
  }
  else
  {
    MyCdScsiIOReq->csi_SCSICmd.scsi_Data = NULL ;
  }
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = DataLength ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Actual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Command = MyCdScsiIOReq->csi_CommandBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdLength = CmdLength ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdActual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Flags = Flags | SCSIF_AUTOSENSE ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseLength = SCSI_SENSEDATA_LENGTH ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseActual = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Error = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command = HD_SCSICMD ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length = sizeof( struct SCSICmd ) ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data = ( APTR )&MyCdScsiIOReq->csi_SCSICmd ;
}


/*
 * TEST UNIT READY command (00h)
 */
void ScsiTestUnitReadyCommand( struct CdScsiIOReq *MyCdScsiIOReq )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x00 ;  /* TEST UNIT READY command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  ScsiIORequest( MyCdScsiIOReq, 6, 0, 0 ) ;
}


/*
 * TREAD command (08h)
 */
void ScsiReadCommand( struct CdScsiIOReq *MyCdScsiIOReq )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x08 ;  /* READ command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  ScsiIORequest( MyCdScsiIOReq, 6, 0, 0 ) ;
}


/*
 * INQUIRY command (12h)
 */
void ScsiInquiryCommand( struct CdScsiIOReq *MyCdScsiIOReq )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x12 ;  /* INQUIRY command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 << 0 /* EVPD */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = 0x00 ;  /* Page code */
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = 40 ;  /* Allocation length: only standard data */
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  ScsiIORequest( MyCdScsiIOReq, 6, 40, SCSIF_READ ) ;
}


#if 0
/*
 * INQUIRY command (12h)
 */
LONG ScsiInquiryResult( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct DriveData *MyDriveData ;
  UBYTE *MyData ;
  ULONG Address ;
  LONG Error ;
  ULONG Count ;

  MyDriveData = ( struct DriveData * )MyCdScsiIOReq->csi_CdIOStdReq->io_Data ;
  MyData = ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer ;
  
  if( 0 == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
  {  /* completed ok */
    MyDriveData->dd_PeripheralDeviceType = ( ( MyData[ 0 ] >> 0 ) & 0x1F ) ;
    for( Count = 0 ; Count < 8 ; Count++ )
    {
      MyDriveData->dd_VendorIdentification[ Count ] = MyData[ Count + 8 ] ;
    }
    for( Count = 0 ; Count < 16 ; Count++ )
    {
      MyDriveData->dd_ProductIdentification[ Count ] = MyData[ Count + 16 ] ;
    }
    Error = 0 ;
  }
  else
  {  /* comleted not ok */
    Error = CDERR_NotSpecified ;
  }

  return( Error ) ;

}
#endif


/*
 * MODE SELECT(6) command (15h)
 */
void ScsiModeSelect06Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG ParameterListLength )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x15 ;  /* MODE SELECT(6) command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( ( 1 << 4 /* PF */ ) | ( 0 << 0 /* SP */ )  );
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ParameterListLength ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  ScsiIORequest( MyCdScsiIOReq, 6, ParameterListLength, SCSIF_WRITE ) ;
}


/*
 * MODE SENSE(6) command (1Ah)
 */
void ScsiModeSense06Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG PageControl, ULONG PageCode )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x1A ;  /* MODE SENSE(6) command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( ( 0xC0 & ( PageControl << 6 ) ) |  ( 0x3F & ( PageCode << 0 ) ) ) ;  /* Page control and Page code */
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = 255 ;  /* Allocation length: only standard data */
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  ScsiIORequest( MyCdScsiIOReq, 6, 255, SCSIF_READ ) ;
}


/*
 * START STOP UNIT command (1Bh)
 */
void ScsiStartStopUnitCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG LoEj, ULONG Start )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x1B ;  /* START STOP UNIT command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 1 << 0 /* Immed */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( LoEj << 1 /* LoEj */ ) | ( Start << 0 /* Start */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  ScsiIORequest( MyCdScsiIOReq, 6, 0, SCSIF_WRITE ) ;
}





/*
 * READ CD-ROM CAPACITY command (25h)
 */
void ScsiReadCdromCapacityCommand( struct CdScsiIOReq *MyCdScsiIOReq )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x25 ;  /* READ CD-ROM CAPACITY command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = ( 0 ) ;
  ScsiIORequest( MyCdScsiIOReq, 10, 8, SCSIF_READ ) ;
}




/*
 * READ(10) command (28h)
 */
void ScsiRead10Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG LogicalBlockAddress, ULONG TransferLength )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x28 ;  /* READ(10) command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( 0xFF & ( LogicalBlockAddress >> 24 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( 0xFF & ( LogicalBlockAddress >> 16 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( 0xFF & ( LogicalBlockAddress >> 8 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = ( 0xFF & ( LogicalBlockAddress >> 0 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] = ( 0xFF & ( TransferLength >> 8 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] = ( 0xFF & ( TransferLength >> 0 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = ( 0 ) ;
  ScsiIORequest( MyCdScsiIOReq, 10, TransferLength * 2048, SCSIF_READ ) ;
}


/*
 * SEEK(10) command (2Bh)
 */
void ScsiSeek10Command( struct CdScsiIOReq *MyCdScsiIOReq, ULONG LogicalBlockAddress )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x2B ;  /* SEEK(10) command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( 0xFF & ( LogicalBlockAddress >> 24 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( 0xFF & ( LogicalBlockAddress >> 16 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( 0xFF & ( LogicalBlockAddress >> 8 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = ( 0xFF & ( LogicalBlockAddress >> 0 ) ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = ( 0 ) ;
  ScsiIORequest( MyCdScsiIOReq, 10, 0, SCSIF_WRITE ) ;
}








/*
 * PAUSE RESUME command (4Bh)
 */
void ScsiPauseResumeCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG Resume )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x4B ;  /* PAUSE RESUME command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] = ( Resume << 0 /* Resume */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = 0x00 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Data = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Actual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Command = MyCdScsiIOReq->csi_CommandBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdLength = 10 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdActual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Flags = SCSIF_AUTOSENSE ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseLength = SCSI_SENSEDATA_LENGTH ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseActual = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command = HD_SCSICMD ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length = sizeof( struct SCSICmd ) ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data = ( APTR )&MyCdScsiIOReq->csi_SCSICmd ;
}


/*
 * PLAY AUDIO(10) command (45h)
 */
void ScsiPlayAudioCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartAddress, ULONG TransferLength )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x45 ;  /* PLAY AUDIO(10) command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 << 0 /* RelAdr */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( ( StartAddress >> 24 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( ( StartAddress >> 16 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( ( StartAddress >> 8 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = ( ( StartAddress >> 0 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] =  ( ( TransferLength >> 8 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] =  ( ( TransferLength >> 0 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = 0x00 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Data = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Actual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Command = MyCdScsiIOReq->csi_CommandBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdLength = 10 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdActual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Flags = SCSIF_AUTOSENSE ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseLength = SCSI_SENSEDATA_LENGTH ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseActual = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command = HD_SCSICMD ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length = sizeof( struct SCSICmd ) ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data = ( APTR )&MyCdScsiIOReq->csi_SCSICmd ;
}


/*
 * PLAY AUDIO MSF command (47h)
 */
void ScsiPlayAudioMsfCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartingFields, ULONG EndingFields )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x47 ;  /* PLAY AUDIO MSF command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( ( StartingFields >> 16 ) & 0xFF ) ;  /* Starting M field */
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( ( StartingFields >> 8 ) & 0xFF ) ;  /* Starting S field */
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = ( ( StartingFields >> 0 ) & 0xFF ) ;  /* Starting F field */
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] = ( ( EndingFields >> 16 ) & 0xFF ) ;  /* Ending M field */
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] = ( ( EndingFields >> 8 ) & 0xFF ) ;  /* Ending S field */
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] = ( ( EndingFields >> 0 ) & 0xFF ) ;  /* Ending F field */
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = 0x00 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Data = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Actual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Command = MyCdScsiIOReq->csi_CommandBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdLength = 10 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdActual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Flags = SCSIF_AUTOSENSE ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseLength = SCSI_SENSEDATA_LENGTH ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseActual = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command = HD_SCSICMD ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length = sizeof( struct SCSICmd ) ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data = ( APTR )&MyCdScsiIOReq->csi_SCSICmd ;
}


/*
 * PLAY AUDIO TRACK INDEX command (48h)
 */
void ScsiPlayAudioTrackIndexCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartingTrack, ULONG EndingTrack )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x48 ;  /* PLAY AUDIO TRACK INDEX command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( StartingTrack ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = ( 1 ) ;  /* start index 1 means: start with the first audio sector */
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] =  ( EndingTrack ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] =  ( 99 ) ;  /* ending index 99 means: continues through the last sector of the track */
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = 0x00 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Data = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Actual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Command = MyCdScsiIOReq->csi_CommandBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdLength = 10 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdActual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Flags = SCSIF_AUTOSENSE ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseLength = SCSI_SENSEDATA_LENGTH ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseActual = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command = HD_SCSICMD ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length = sizeof( struct SCSICmd ) ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data = ( APTR )&MyCdScsiIOReq->csi_SCSICmd ;
}


/*
 * READ SUB-CHANNEL command (42h)
 */
void ScsiReadSubChannelCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG MSF )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x42 ;  /* READ SUB-CHANNEL command */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( MSF << 1 /* MSF */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( 1 << 6 /* SubQ */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x01 ;  /* Sub-channel data format: CD-ROM current position */
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] = 0x00 ;  /* Track number */
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] = 0xFF & ( 16 >> 8 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] = 0xFF & ( 16 >> 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = 0x00 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Data = MyCdScsiIOReq->csi_DataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = 16 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Actual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Command = MyCdScsiIOReq->csi_CommandBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdLength = 10 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdActual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Flags = SCSIF_READ | SCSIF_AUTOSENSE ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseLength = SCSI_SENSEDATA_LENGTH ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseActual = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command = HD_SCSICMD ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length = sizeof( struct SCSICmd ) ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data = ( APTR )&MyCdScsiIOReq->csi_SCSICmd ;
}


/*
 * READ SUB-CHANNEL result (42h)
 */
LONG ScsiReadSubChannelResult( struct CdScsiIOReq *MyCdScsiIOReq )
{
  struct QCode *MyQCode ;
  UBYTE *MyData ;
  LONG Error ;

  MyQCode = ( struct QCode * )MyCdScsiIOReq->csi_CdIOStdReq->io_Data ;
  MyData = ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer ;
  
  if( 0 == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
  {  /* completed ok */
    MyQCode->CtlAdr = ( MyData[ 5 ] << 4 )  | ( MyData[ 5 ] >> 4 ) ;
    MyQCode->Track = MyData[ 6 ] ;
    MyQCode->Index = MyData[ 7 ] ;
    MyQCode->TrackPosition.LSN = ( MyData[ 12 ] << 24 ) | ( MyData[ 13 ] << 16 )  | ( MyData[ 14 ] << 8 )  | ( MyData[ 15 ] << 0 ) ;
    MyQCode->DiskPosition.LSN = ( MyData[ 8 ] << 24 ) | ( MyData[ 9 ] << 16 )  | ( MyData[ 10 ] << 8 )  | ( MyData[ 11 ] << 0 ) ;
    Error = 0 ;
  }
  else
  {  /* comleted not ok */
    Error = CDERR_NotSpecified ;
  }

  return( Error ) ;
}


/*
 * READ TOC command (43h)
 */
void ScsiReadTocCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG MSF )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0x43 ;  /* READ TOC comand */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( MSF << 1 /* MSF */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] = 0x00 ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] = 0xFF & ( 804 >> 8 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] = 0xFF & ( 804 >> 0 ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = 0x00 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Data = MyCdScsiIOReq->csi_DataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = 804 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Actual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Command = MyCdScsiIOReq->csi_CommandBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdLength = 10 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_CmdActual = 0 ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Flags = SCSIF_READ | SCSIF_AUTOSENSE ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseData = MyCdScsiIOReq->csi_SenseDataBuffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseLength = SCSI_SENSEDATA_LENGTH ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_SenseActual = 0 ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Command = HD_SCSICMD ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Length = sizeof( struct SCSICmd ) ;
  MyCdScsiIOReq->csi_ScsiIOStdReq.io_Data = ( APTR )&MyCdScsiIOReq->csi_SCSICmd ;
}


/*
 * READ TOC result (43h)
 */
LONG ScsiReadTocResult( struct CdScsiIOReq *MyCdScsiIOReq )
{
  union CDTOC *MyCDTOC ;
  UBYTE *MyData ;
  ULONG Address ;
  LONG Error ;

  MyCDTOC = ( union CDTOC * )MyCdScsiIOReq->csi_CdIOStdReq->io_Data ;
  MyData = ( UBYTE * )MyCdScsiIOReq->csi_DataBuffer ;
  
  if( 0 == MyCdScsiIOReq->csi_SCSICmd.scsi_Status )
  {  /* read toc completed ok */    
    if( MyCdScsiIOReq->csi_CdIOStdReq->io_Length )
    {  /* some entries can be processed */
      MyCDTOC[ 0 ].Summary.FirstTrack = MyData[ 2 ] ;
      MyCDTOC[ 0 ].Summary.LastTrack = MyData[ 3 ] ;
      MyData += 4 ;
      MyCdScsiIOReq->csi_CdIOStdReq->io_Actual = 1 ;
      while( MyCdScsiIOReq->csi_CdIOStdReq->io_Actual < MyCdScsiIOReq->csi_CdIOStdReq->io_Length )
      {  /* copy TOC data */
        Address = ( MyData[ 4 ] << 24 ) | ( MyData[ 5 ] << 16 )  | ( MyData[ 6 ] << 8 )  | ( MyData[ 7 ] << 0 ) ; 
        if( 0xAA == MyData[ 2 ] )
        {  /* track descriptor is for the start of the lead-out area */
          MyCDTOC[ 0 ].Summary.LeadOut.LSN = Address ;
          break ;
        }
        else
        {  /* normal track */
          MyCDTOC[ MyCdScsiIOReq->csi_CdIOStdReq->io_Actual ].Entry.CtlAdr = ( MyData[ 1 ] << 4 )  | ( MyData[ 1 ] >> 4 ) ;  /* Q-Code info */
          MyCDTOC[ MyCdScsiIOReq->csi_CdIOStdReq->io_Actual ].Entry.Track = MyData[ 2 ] ;  /* track number */
          MyCDTOC[ MyCdScsiIOReq->csi_CdIOStdReq->io_Actual ].Entry.Position.LSN = Address ;  /* LSN position */
          MyCdScsiIOReq->csi_CdIOStdReq->io_Actual++ ;
          MyData += 8 ;
        }
      }
    }
    else
    {  /* no entries can be processed */
    }
    Error = 0 ;
  }
  else
  {  /* read toc comleted not ok */
    Error = CDERR_NotSpecified ;
  }
  
  return( Error ) ;
}


/*
 * READ CD command (BEh)
 */
void ScsiReadCdCommand( struct CdScsiIOReq *MyCdScsiIOReq, ULONG StartAddress, ULONG TransferLength, UWORD *Buffer )
{
  MyCdScsiIOReq->csi_CommandBuffer[ 0 ] = 0xBE ;  /* READ CD comand */
  MyCdScsiIOReq->csi_CommandBuffer[ 1 ] = ( 1 << 2 /* Expected Sector Type = CD-DA only */ ) | ( 0 << 1 /* DAP = no modification */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 2 ] = ( ( StartAddress >> 24 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 3 ] = ( ( StartAddress >> 16 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 4 ] = ( ( StartAddress >> 8 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 5 ] = ( ( StartAddress >> 0 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 6 ] =  ( ( TransferLength >> 16 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 7 ] =  ( ( TransferLength >> 8 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 8 ] =  ( ( TransferLength >> 0 ) & 0xFF ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 9 ] = ( 0 << 7 /* no SYNC */ ) | 
                                          ( 0 << 5 /* no headers */ ) |
                                          ( 1 << 4 /* User Data */ ) |
                                          ( 0 << 3 /* no EDC or ECC */ ) |
                                          ( 0 << 1 /* no C2 Error Information */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 10 ] = ( 0 << 0 /* no sub channel data */ ) ;
  MyCdScsiIOReq->csi_CommandBuffer[ 11 ] = 0 ;
  ScsiIORequest( MyCdScsiIOReq, 12, 0, SCSIF_READ ) ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Data = Buffer ;
  MyCdScsiIOReq->csi_SCSICmd.scsi_Length = TransferLength * CDDA_SECTOR_SIZE ;
}

