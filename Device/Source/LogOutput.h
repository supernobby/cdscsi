/*
** LogOutput.h
*/


#ifndef _LOGOUTPUT_H
#define _LOGOUTPUT_H


#include <exec/types.h>


/*
** possible log levels
*/
enum LogLevel
{
  OFF_LEVEL,
  FAILURE_LEVEL,
  ERROR_LEVEL,
  WARNING_LEVEL,
  INFO_LEVEL,
  DEBUG_LEVEL,
  NUM_LEVELS
} ;


/*
** some forward declarations to avoid recursive inludes
*/
struct DeviceCore ;


/*
** context data for the log output
*/
#define LOGOUTPUT_LOGSTRING_LENGTH ( 1024UL )
struct LogOutput
{
  struct DeviceCore *lo_DeviceCore ;  /* to where we belong */
  enum LogLevel lo_LogLevel ;
  UBYTE lo_LogString[ LOGOUTPUT_LOGSTRING_LENGTH ] ;
} ;


/*
** functions of this module
*/
struct LogOutput *CreateLogOutput( struct DeviceCore *MyDeviceCore ) ;
void ConfigureLogOutput( struct LogOutput *MyLogOutput ) ;
void DeleteLogOutput( struct LogOutput *MyLogOutput ) ;
void LogText( struct LogOutput *MyLogOutput, enum LogLevel MyLogLevel, STRPTR MyFormat, ... ) ;


/*
**
*/
#define LOG_MACROS_ENABLED ( 1 )
#if LOG_MACROS_ENABLED
  #define LOG_TEXT( _parameter ) do{ LogText _parameter ; }while( 0 )
#else
  #define LOG_TEXT( _parameter )
#endif


#define _FAILURE_LEVEL MyDeviceCore->dc_LogOutput, FAILURE_LEVEL
#define _ERROR_LEVEL MyDeviceCore->dc_LogOutput, ERROR_LEVEL
#define _WARNING_LEVEL MyDeviceCore->dc_LogOutput, WARNING_LEVEL
#define _INFO_LEVEL MyDeviceCore->dc_LogOutput, INFO_LEVEL
#define _DEBUG_LEVEL MyDeviceCore->dc_LogOutput, DEBUG_LEVEL


#endif  /* !_LOGOUTPUT_H */
