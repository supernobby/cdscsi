/*
 * Settings.h
 */


#ifndef _SETTINGS_H
#define _SETTINGS_H


/*
 * settings context
 */
struct Settings
{
  struct Context *s_Context ;  /* pointer to the context */
} ;


/*
 * module functions
 */
struct Settings *CreateSettings( struct Context *MyContext ) ;
void DeleteSettings( struct Settings *MySettings ) ;


#endif   /* !_SETTINGS_H */
