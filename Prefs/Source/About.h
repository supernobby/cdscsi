/*
 * About.h
 */


#ifndef _ABOUT_H
#define _ABOUT_H


#include "Context.h"


void ShowAbout( struct Context *MyContext ) ;


#endif  /* !_ABOUT_H */
