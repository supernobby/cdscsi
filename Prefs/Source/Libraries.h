/*
 * Libraries.h
 */


#ifndef _LIBRARIES_H
#define _LIBRARIES_H


#include <exec/types.h>


LONG OpenLibraries( void ) ;
void CloseLibraries( void ) ;


#endif  /* !_LIBRARIES_H */
