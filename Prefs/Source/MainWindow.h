/*
 * MainWindow.h
 */


#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H


#include "Dispatcher.h"


/*
 * gadget id values for the main window
 */
enum MainWindowGadgetIds
{
  GID_SCSIDEVICE = 0,
  GID_SCSIUNIT,
  GID_AUDIOOUTPUT,
  GID_AHIUNIT,
  GID_LOGLEVEL,
  GID_BOOTPRIORITY,
  GID_BOOTDELAY,
  GID_SAVE,
  GID_USE,
  GID_CANCEL,
  GID_MAINWINDOW_LAST
} ;


/*
 * main window context
 */
struct MainWindow
{
  struct Dispatchee mw_Dispatchee ;
  struct Context *mw_Context ;  /* pointer to context */
  struct MsgPort *mw_AppPort ;
  APTR mw_WindowObject ;  /* the window object */
  APTR mw_Gadgets[ GID_MAINWINDOW_LAST ] ;
  struct List mw_ScsiUnitChooserLabels, mw_AudioOutputChooserLabels, mw_AhiUnitChooserLabels, mw_LogLevelChooserLabels ;
  struct Screen *mw_Screen ;
  APTR mw_VisualInfo ;
  struct Window *mw_Window ;
  struct Menu *mw_MenuStrip ;
  ULONG mw_SignalMask ;
} ;


/*
 * main window module functions
 */
struct MainWindow *CreateMainWindow( struct Context *MyContext ) ;
void DeleteMainWindow( struct MainWindow *MyMainWindow ) ;
ULONG DoMainWindow( struct MainWindow *MyMainWindow, ULONG TriggerSignals ) ;


#endif  /* _MAINWINDOW_H */
