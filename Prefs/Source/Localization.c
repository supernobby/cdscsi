/*
 * Localization.c
 */


#include "Localization.h"
#include <proto/exec.h>
#include <proto/locale.h>


/*
 * now only get the string array
 */
#undef CdScsi_STRINGS_H
#define CdScsi_ARRAY
#include "CatComp.h"


/*
 * get a string from the catalog or the builtin version
 */
STRPTR GetLocalizedString( struct Localization *MyLocalization, LONG StringID )
{
  STRPTR LocalizedString ;
  LONG Count, NumberOfStrings ;

  /* in case string was not found */
  LocalizedString = "string error" ;

  NumberOfStrings = ( sizeof( CdScsi_Array ) / sizeof( struct CdScsi_ArrayType ) ) ;
  for( Count = 0 ; Count < NumberOfStrings ; Count++ )
  {  /* look for the requested sting identifier */
    if( CdScsi_Array[ Count ].cca_ID == StringID )
    {  /* string identifier match */
      LocalizedString = CdScsi_Array[ Count ].cca_Str ;
      break ;
    }
  }

  if( ( NULL != MyLocalization ) && ( NULL != MyLocalization->l_Catalog ) )
  {  /* requirements ok, get string from catalog */
    LocalizedString = GetCatalogStr( MyLocalization->l_Catalog, StringID, LocalizedString ) ;
  }
  else
  {  /* requirements not ok, so use internal strings */
  }

  return( LocalizedString ) ;
}


/*
 * delete localization context
 */
void DeleteLocalization( struct Localization *MyLocalization )
{
  if( NULL != MyLocalization )
  {  /* localization context needs to be freed */
    if( NULL != MyLocalization->l_Locale )
    {  /* locale needs to be closed */
      if( NULL != MyLocalization->l_Catalog )
      {  /* catalog needs to be closed */
        CloseCatalog( MyLocalization->l_Catalog ) ;
      }
      CloseLocale( MyLocalization->l_Locale ) ;
    }
    FreeVec( MyLocalization ) ;
  }
}


/*
 * create context for the localization stuff 
 */
struct Localization *CreateLocalization( struct Context *MyContext )
{
  struct Localization *MyLocalization ;
  
  MyLocalization = NULL ;
  
  if( ( NULL != MyContext ) && ( NULL != LocaleBase ) )
  {  /* requirements seem ok */
    MyLocalization = AllocVec( sizeof( struct Localization ), MEMF_ANY | MEMF_CLEAR ) ;
    if( NULL != MyLocalization )
    {  /* memory for localization context ok */
      MyLocalization->l_Context = MyContext ;
      MyLocalization->l_Locale = OpenLocale( NULL ) ;
      if( NULL != MyLocalization->l_Locale )
      {  /* locale ok */
        MyLocalization->l_Catalog = OpenCatalog( MyLocalization->l_Locale,
          "CdScsi.catalog",
          OC_BuiltInLanguage, ( ULONG )"english",
          OC_Version, ( ULONG )0,
          TAG_END ) ;
        if( NULL != MyLocalization->l_Catalog )
        {  /* catalog ok */
        }
        else
        {  /* catalog not ok */
          DeleteLocalization( MyLocalization ) ;
          MyLocalization = NULL ;
        }
      }
      else
      {  /* locale not ok */
        DeleteLocalization( MyLocalization ) ;
        MyLocalization = NULL ;
      }
    }
    else
    {  /* memory for localization context not ok */
    }
  }
  else
  {  /* requirements not ok */
  }

  return( MyLocalization ) ;
}
