/*
 * About.c
 */


#include "About.h"
#include "Version.h"
#include "MainWindow.h"
#include "Localization.h"
#include <proto/intuition.h>



struct EasyStruct AboutRequester =
{
  sizeof( struct EasyStruct ),
  0,
  NULL,
  "%s\n\n%s\n\n%s\n\n%s",
  "Ok",
} ;



void ShowAbout( struct Context *MyContext )
{
  AboutRequester.es_Title = GetLocalizedString( MyContext->c_Localization, STR_ABOUT_CDSCSI ) ;
  EasyRequest( MyContext->c_MainWindow->mw_Window, &AboutRequester, NULL, 
    GetVersionString( ),
    GetLocalizedString( MyContext->c_Localization, STR_CDSCSI_DESCRIPTION ),
    GetCopyrightString( ),
    GetLocalizedString( MyContext->c_Localization, STR_CDSCSI_TRANSLATOR ) ) ;
}
