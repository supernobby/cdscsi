/*
 * Dispatcher.c
 */


#include "Dispatcher.h"
#include <proto/exec.h>
#include <proto/dos.h>
#include <clib/alib_protos.h>


/*
 * create the dispatcher
 */
struct Dispatcher *CreateDispatcher( struct Context *MyContext )
{
  struct Dispatcher *MyDispatcher ;
  
  MyDispatcher = NULL ;

  if( NULL != MyContext )
  {  /* requirements ok */
    MyDispatcher = AllocVec( sizeof( struct Dispatcher ), MEMF_ANY | MEMF_CLEAR ) ;
    if( NULL != MyDispatcher )
    {  /* dispatcher context ok */
      MyDispatcher->d_Context = MyContext ;
      NewList( &MyDispatcher->d_DispatcheeList ) ;
      MyDispatcher->d_SignalMask = 0 ;
      MyDispatcher->d_SignalMaskDiff = 0 ;
    }
    else
    {  /* dispatcher context not ok */
    }
  }
  else
  {  /* requirements not ok */
  }
  
  return( MyDispatcher ) ;
}


/*
** delete the dispatcher
*/
void DeleteDispatcher( struct Dispatcher *MyDispatcher )
{
  if( NULL != MyDispatcher )
  {  /* dispatcher context needs to be freed */
    FreeVec( MyDispatcher ) ;
  }
}


/*
** add a dispatchee to the list of installed handlers
*/
void AddDispatchee( struct Dispatcher *MyDispatcher, struct Dispatchee *MyDispatchee )
{
  if( NULL != MyDispatcher )
  {
    if( NULL != MyDispatchee )
    {
      AddHead( &MyDispatcher->d_DispatcheeList, ( struct Node * )MyDispatchee ) ;
      MyDispatcher->d_SignalMaskDiff |= MyDispatchee->d_Signals ;
    }
  }
}


/*
** remove a dispatchee from list of installed handlers
*/
void RemDispatchee( struct Dispatcher *MyDispatcher, struct Dispatchee *MyDispatchee )
{
  if( NULL != MyDispatcher )
  {
    if( NULL != MyDispatchee )
    {
      Remove( ( struct Node *)MyDispatchee ) ;
      MyDispatcher->d_SignalMaskDiff |= MyDispatchee->d_Signals ;
    }
  }
}


/*
** run the dispatcher
*/
void DoDispatcher( struct Dispatcher *MyDispatcher, enum DispatchMode MyMode )
{
  ULONG ReceivedSignals, NewSignals ;
  struct Node *NextNode ;
  struct Dispatchee *MyDispatchee ;

  if( NULL != MyDispatcher )
  {
    if( MyDispatcher->d_SignalMaskDiff )
    {  /* mask updated required */
      MyDispatcher->d_SignalMask = 0 ;
      NextNode = MyDispatcher->d_DispatcheeList.lh_Head ;
      while( NULL != NextNode->ln_Succ )
      {  /* check all handlers */
        MyDispatchee = ( struct Dispatchee * )NextNode ;
        NextNode = NextNode->ln_Succ ;
        MyDispatcher->d_SignalMask |= ( MyDispatchee->d_Signals ) ;
      }
      MyDispatcher->d_SignalMaskDiff = 0 ;
    }
    
    if( DISPATCH_BLOCKING == MyMode )
    {  /* wait for signals to arrive */
      ReceivedSignals = Wait( MyDispatcher->d_SignalMask ) ;
    }
    else if( DISPATCH_NONBLOCKING == MyMode )
    {  /* just check signals */
      ReceivedSignals = SetSignal( 0L, 0L ) ;  /* just get pending signals */
      if( 0 != ReceivedSignals )
      {  /* clear the signals this way, as doing this with SetSignal is considered dangerous */
        ReceivedSignals = Wait( ReceivedSignals ) ; 
      }
      else
      {  /* nothing to do */
      }
    }
    if( ReceivedSignals )
    {  /* find the dispatchee(s) */
      NextNode = MyDispatcher->d_DispatcheeList.lh_Head ;
      while( NULL != NextNode->ln_Succ )
      {  /* check all handlers */
        MyDispatchee = ( struct Dispatchee * )NextNode ;
        NextNode = NextNode->ln_Succ ;
        if( ReceivedSignals & MyDispatchee->d_Signals ) 
        {  /* action for this handler */
          if( NULL != MyDispatchee->d_DoFunction )
          {  /* handler can be called */
            NewSignals = ( MyDispatchee->d_DoFunction )( MyDispatchee->d_DoData, ( ReceivedSignals & MyDispatchee->d_Signals ) ) ;
            if( 0 != NewSignals )
            {  /* node did not kill itself */
              MyDispatcher->d_SignalMaskDiff |= ( MyDispatchee->d_Signals ^ NewSignals ) ;
              MyDispatchee->d_Signals = NewSignals ;
            }
            else
            {  /* node did kill itself */
              MyDispatcher->d_SignalMaskDiff |= 1 ;
            }
          }
        }
      }
    }
  }
}
