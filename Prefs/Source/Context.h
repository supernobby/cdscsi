/*
 * Context.h
 */


#ifndef _CONTEXT_H
#define _CONTEXT_H


#include <exec/execbase.h>


/*
 * forward declarations
 */
struct Dispatcher ;
struct Localization ;
struct Settings ;
struct ConfigFile ;
struct MainWindow ;


/*
 * the main context
 */
struct Context
{
  struct Dispatcher *c_Dispatcher ;
  struct Localization *c_Localization ;
  struct Settings *c_Settings ;
  struct ConfigFile *c_ConfigFile ;
  struct MainWindow *c_MainWindow ;
  struct DiskObject *c_DiskObject ;  /* our icon */
  ULONG c_ShutdownRequest : 1 ;
  ULONG c_Shutdown : 1 ;
} ;


/*
 * context module functions
 */
struct Context *CreateAppContext( void ) ;
void DeleteAppContext( struct Context *MyContext ) ;


#endif  /* !_CONTEXT_H */
