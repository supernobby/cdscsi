/*
 * Localization.h
 */


#ifndef _LOCALIZATION_H
#define _LOCALIZATION_H


#include "Context.h"
#include <libraries/locale.h>


/*
** only let them know the string identifiers
*/
#define CdScsi_NUMBERS
#include "CatComp.h"
#undef CdScsi_NUMBERS


/*
 * localization context
 */
struct Localization
{
  struct Context *l_Context ;  /* pointer to context */
  struct Locale *l_Locale ;  /* the locale */
  struct Catalog *l_Catalog ;  /* our localized catalog */
} ;


/*
 * localization functions
 */
struct Localization *CreateLocalization( struct Context *MyContext ) ;
void DeleteLocalization( struct Localization *OldLocalization ) ;
STRPTR GetLocalizedString( struct Localization *MyLocalization, LONG StringID ) ;


#endif  /* !_LOCALIZATION_H */
