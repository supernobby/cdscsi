/*
 * Context.c
 */


#include "Context.h"
#include <proto/exec.h>


/*
 * delete the context
 */
void DeleteAppContext( struct Context *MyContext )
{
  if( NULL != MyContext )
  {
    FreeVec( MyContext ) ;
  }
}


/*
 * create the context
 */
struct Context *CreateAppContext( void )
{
  struct Context *MyContext ;

  MyContext = NULL ;
  
  MyContext = AllocVec( sizeof( struct Context ), MEMF_ANY | MEMF_CLEAR ) ;
  if( NULL != MyContext )
  {  /* context ok */
  }
  else
  {  /* context not ok */
  }

  return( MyContext ) ;
}
