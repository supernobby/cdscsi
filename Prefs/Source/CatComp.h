
/****************************************************************

   This file was created automatically by `FlexCat 2.16'
   from "//Source/CdScsi.cd"

   using CatComp.sd 1.2 (24.09.1999)

   Do NOT edit by hand!

****************************************************************/

#ifndef CdScsi_STRINGS_H
#define CdScsi_STRINGS_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#ifdef  CdScsi_BASIC_CODE
#undef  CdScsi_BASIC
#undef  CdScsi_CODE
#define CdScsi_BASIC
#define CdScsi_CODE
#endif

#ifdef  CdScsi_BASIC
#undef  CdScsi_ARRAY
#undef  CdScsi_BLOCK
#define CdScsi_ARRAY
#define CdScsi_BLOCK
#endif

#ifdef  CdScsi_ARRAY
#undef  CdScsi_NUMBERS
#undef  CdScsi_STRINGS
#define CdScsi_NUMBERS
#define CdScsi_STRINGS
#endif

#ifdef  CdScsi_BLOCK
#undef  CdScsi_STRINGS
#define CdScsi_STRINGS
#endif


#ifdef CdScsi_CODE
#include <proto/locale.h>
extern struct Library *LocaleBase;
#endif

#ifdef CdScsi_NUMBERS

#define STR_MAINWINDOW_TITLE 0
#define STR_MENU_PROJECT 1
#define STR_MENU_PROJECT_OPEN 2
#define STR_MENU_PROJECT_OPEN_KEY 3
#define STR_MENU_PROJECT_SAVEAS 4
#define STR_MENU_PROJECT_SAVEAS_KEY 5
#define STR_MENU_PROJECT_ABOUT 6
#define STR_MENU_PROJECT_ABOUT_KEY 7
#define STR_MENU_PROJECT_QUIT 8
#define STR_MENU_PROJECT_QUIT_KEY 9
#define STR_MENU_EDIT 10
#define STR_MENU_EDIT_DEFAULTS 11
#define STR_MENU_EDIT_DEFAULTS_KEY 12
#define STR_MENU_EDIT_SAVED 13
#define STR_MENU_EDIT_SAVED_KEY 14
#define STR_MENU_EDIT_RESTORE 15
#define STR_MENU_EDIT_RESTORE_KEY 16
#define STR_MENU_SETTINGS 17
#define STR_MENU_SETTINGS_ICONS 18
#define STR_MENU_SETTINGS_ICONS_KEY 19
#define STR_GROUP_DEVICE 20
#define STR_SETTING_SCSIDEVICE 21
#define STR_SETTING_SCSIUNIT 22
#define STR_SETTING_AUDIOOUTPUT 23
#define STR_SETTING_AHIUNIT 24
#define STR_SETTING_LOGLEVEL 25
#define STR_BUTTON_SAVE 26
#define STR_BUTTON_USE 27
#define STR_BUTTON_CANCEL 28
#define STR_LOGLEVEL_OFF 29
#define STR_LOGLEVEL_FAILURE 30
#define STR_LOGLEVEL_ERROR 31
#define STR_LOGLEVEL_WARNING 32
#define STR_LOGLEVEL_INFORMATION 33
#define STR_LOGLEVEL_DEBUG 34
#define STR_GROUP_MOUNT 35
#define STR_SETTING_BOOTPRIORITY 36
#define STR_SETTING_BOOTDELAY 37
#define STR_ABOUT_CDSCSI 38
#define STR_CDSCSI_DESCRIPTION 39
#define STR_CDSCSI_TRANSLATOR 40

#endif /* CdScsi_NUMBERS */


/****************************************************************************/


#ifdef CdScsi_STRINGS

#define STR_MAINWINDOW_TITLE_STR "CdScsi-Preferences"
#define STR_MENU_PROJECT_STR "Project"
#define STR_MENU_PROJECT_OPEN_STR "Open ..."
#define STR_MENU_PROJECT_OPEN_KEY_STR "O"
#define STR_MENU_PROJECT_SAVEAS_STR "Save As ..."
#define STR_MENU_PROJECT_SAVEAS_KEY_STR "A"
#define STR_MENU_PROJECT_ABOUT_STR "About ..."
#define STR_MENU_PROJECT_ABOUT_KEY_STR "?"
#define STR_MENU_PROJECT_QUIT_STR "Quit"
#define STR_MENU_PROJECT_QUIT_KEY_STR "Q"
#define STR_MENU_EDIT_STR "Edit"
#define STR_MENU_EDIT_DEFAULTS_STR "Reset To Defaults"
#define STR_MENU_EDIT_DEFAULTS_KEY_STR "D"
#define STR_MENU_EDIT_SAVED_STR "Last Saved"
#define STR_MENU_EDIT_SAVED_KEY_STR "L"
#define STR_MENU_EDIT_RESTORE_STR "Restore"
#define STR_MENU_EDIT_RESTORE_KEY_STR "R"
#define STR_MENU_SETTINGS_STR "Settings"
#define STR_MENU_SETTINGS_ICONS_STR "Create Icons?"
#define STR_MENU_SETTINGS_ICONS_KEY_STR "I"
#define STR_GROUP_DEVICE_STR "Device"
#define STR_SETTING_SCSIDEVICE_STR "SCSI Device:"
#define STR_SETTING_SCSIUNIT_STR "SCSI Unit:"
#define STR_SETTING_AUDIOOUTPUT_STR "Audio Output:"
#define STR_SETTING_AHIUNIT_STR "AHI Unit:"
#define STR_SETTING_LOGLEVEL_STR "Log Level:"
#define STR_BUTTON_SAVE_STR "Save"
#define STR_BUTTON_USE_STR "Use"
#define STR_BUTTON_CANCEL_STR "Cancel"
#define STR_LOGLEVEL_OFF_STR "Off"
#define STR_LOGLEVEL_FAILURE_STR "Failure"
#define STR_LOGLEVEL_ERROR_STR "Error"
#define STR_LOGLEVEL_WARNING_STR "Warning"
#define STR_LOGLEVEL_INFORMATION_STR "Information"
#define STR_LOGLEVEL_DEBUG_STR "Debug"
#define STR_GROUP_MOUNT_STR "Mount"
#define STR_SETTING_BOOTPRIORITY_STR "Boot Priority:"
#define STR_SETTING_BOOTDELAY_STR "Boot Delay:"
#define STR_ABOUT_CDSCSI_STR "About CdScsi"
#define STR_CDSCSI_DESCRIPTION_STR "boot from a CD-ROM drive"
#define STR_CDSCSI_TRANSLATOR_STR "English translation by program author"

#endif /* CdScsi_STRINGS */


/****************************************************************************/


#ifdef CdScsi_ARRAY

struct CdScsi_ArrayType
{
    LONG   cca_ID;
    STRPTR cca_Str;
};

static const struct CdScsi_ArrayType CdScsi_Array[] =
{
    { STR_MAINWINDOW_TITLE, (STRPTR)STR_MAINWINDOW_TITLE_STR },
    { STR_MENU_PROJECT, (STRPTR)STR_MENU_PROJECT_STR },
    { STR_MENU_PROJECT_OPEN, (STRPTR)STR_MENU_PROJECT_OPEN_STR },
    { STR_MENU_PROJECT_OPEN_KEY, (STRPTR)STR_MENU_PROJECT_OPEN_KEY_STR },
    { STR_MENU_PROJECT_SAVEAS, (STRPTR)STR_MENU_PROJECT_SAVEAS_STR },
    { STR_MENU_PROJECT_SAVEAS_KEY, (STRPTR)STR_MENU_PROJECT_SAVEAS_KEY_STR },
    { STR_MENU_PROJECT_ABOUT, (STRPTR)STR_MENU_PROJECT_ABOUT_STR },
    { STR_MENU_PROJECT_ABOUT_KEY, (STRPTR)STR_MENU_PROJECT_ABOUT_KEY_STR },
    { STR_MENU_PROJECT_QUIT, (STRPTR)STR_MENU_PROJECT_QUIT_STR },
    { STR_MENU_PROJECT_QUIT_KEY, (STRPTR)STR_MENU_PROJECT_QUIT_KEY_STR },
    { STR_MENU_EDIT, (STRPTR)STR_MENU_EDIT_STR },
    { STR_MENU_EDIT_DEFAULTS, (STRPTR)STR_MENU_EDIT_DEFAULTS_STR },
    { STR_MENU_EDIT_DEFAULTS_KEY, (STRPTR)STR_MENU_EDIT_DEFAULTS_KEY_STR },
    { STR_MENU_EDIT_SAVED, (STRPTR)STR_MENU_EDIT_SAVED_STR },
    { STR_MENU_EDIT_SAVED_KEY, (STRPTR)STR_MENU_EDIT_SAVED_KEY_STR },
    { STR_MENU_EDIT_RESTORE, (STRPTR)STR_MENU_EDIT_RESTORE_STR },
    { STR_MENU_EDIT_RESTORE_KEY, (STRPTR)STR_MENU_EDIT_RESTORE_KEY_STR },
    { STR_MENU_SETTINGS, (STRPTR)STR_MENU_SETTINGS_STR },
    { STR_MENU_SETTINGS_ICONS, (STRPTR)STR_MENU_SETTINGS_ICONS_STR },
    { STR_MENU_SETTINGS_ICONS_KEY, (STRPTR)STR_MENU_SETTINGS_ICONS_KEY_STR },
    { STR_GROUP_DEVICE, (STRPTR)STR_GROUP_DEVICE_STR },
    { STR_SETTING_SCSIDEVICE, (STRPTR)STR_SETTING_SCSIDEVICE_STR },
    { STR_SETTING_SCSIUNIT, (STRPTR)STR_SETTING_SCSIUNIT_STR },
    { STR_SETTING_AUDIOOUTPUT, (STRPTR)STR_SETTING_AUDIOOUTPUT_STR },
    { STR_SETTING_AHIUNIT, (STRPTR)STR_SETTING_AHIUNIT_STR },
    { STR_SETTING_LOGLEVEL, (STRPTR)STR_SETTING_LOGLEVEL_STR },
    { STR_BUTTON_SAVE, (STRPTR)STR_BUTTON_SAVE_STR },
    { STR_BUTTON_USE, (STRPTR)STR_BUTTON_USE_STR },
    { STR_BUTTON_CANCEL, (STRPTR)STR_BUTTON_CANCEL_STR },
    { STR_LOGLEVEL_OFF, (STRPTR)STR_LOGLEVEL_OFF_STR },
    { STR_LOGLEVEL_FAILURE, (STRPTR)STR_LOGLEVEL_FAILURE_STR },
    { STR_LOGLEVEL_ERROR, (STRPTR)STR_LOGLEVEL_ERROR_STR },
    { STR_LOGLEVEL_WARNING, (STRPTR)STR_LOGLEVEL_WARNING_STR },
    { STR_LOGLEVEL_INFORMATION, (STRPTR)STR_LOGLEVEL_INFORMATION_STR },
    { STR_LOGLEVEL_DEBUG, (STRPTR)STR_LOGLEVEL_DEBUG_STR },
    { STR_GROUP_MOUNT, (STRPTR)STR_GROUP_MOUNT_STR },
    { STR_SETTING_BOOTPRIORITY, (STRPTR)STR_SETTING_BOOTPRIORITY_STR },
    { STR_SETTING_BOOTDELAY, (STRPTR)STR_SETTING_BOOTDELAY_STR },
    { STR_ABOUT_CDSCSI, (STRPTR)STR_ABOUT_CDSCSI_STR },
    { STR_CDSCSI_DESCRIPTION, (STRPTR)STR_CDSCSI_DESCRIPTION_STR },
    { STR_CDSCSI_TRANSLATOR, (STRPTR)STR_CDSCSI_TRANSLATOR_STR },
};


#endif /* CdScsi_ARRAY */


/****************************************************************************/


#ifdef CdScsi_BLOCK

static const char CdScsi_Block[] =
{

     "\x00\x00\x00\x00" "\x00\x12"
    STR_MAINWINDOW_TITLE_STR ""
     "\x00\x00\x00\x01" "\x00\x08"
    STR_MENU_PROJECT_STR "\x00"
     "\x00\x00\x00\x02" "\x00\x08"
    STR_MENU_PROJECT_OPEN_STR ""
     "\x00\x00\x00\x03" "\x00\x02"
    STR_MENU_PROJECT_OPEN_KEY_STR "\x00"
     "\x00\x00\x00\x04" "\x00\x0c"
    STR_MENU_PROJECT_SAVEAS_STR "\x00"
     "\x00\x00\x00\x05" "\x00\x02"
    STR_MENU_PROJECT_SAVEAS_KEY_STR "\x00"
     "\x00\x00\x00\x06" "\x00\x0a"
    STR_MENU_PROJECT_ABOUT_STR "\x00"
     "\x00\x00\x00\x07" "\x00\x02"
    STR_MENU_PROJECT_ABOUT_KEY_STR "\x00"
     "\x00\x00\x00\x08" "\x00\x04"
    STR_MENU_PROJECT_QUIT_STR ""
     "\x00\x00\x00\x09" "\x00\x02"
    STR_MENU_PROJECT_QUIT_KEY_STR "\x00"
     "\x00\x00\x00\x0a" "\x00\x04"
    STR_MENU_EDIT_STR ""
     "\x00\x00\x00\x0b" "\x00\x12"
    STR_MENU_EDIT_DEFAULTS_STR "\x00"
     "\x00\x00\x00\x0c" "\x00\x02"
    STR_MENU_EDIT_DEFAULTS_KEY_STR "\x00"
     "\x00\x00\x00\x0d" "\x00\x0a"
    STR_MENU_EDIT_SAVED_STR ""
     "\x00\x00\x00\x0e" "\x00\x02"
    STR_MENU_EDIT_SAVED_KEY_STR "\x00"
     "\x00\x00\x00\x0f" "\x00\x08"
    STR_MENU_EDIT_RESTORE_STR "\x00"
     "\x00\x00\x00\x10" "\x00\x02"
    STR_MENU_EDIT_RESTORE_KEY_STR "\x00"
     "\x00\x00\x00\x11" "\x00\x08"
    STR_MENU_SETTINGS_STR ""
     "\x00\x00\x00\x12" "\x00\x0e"
    STR_MENU_SETTINGS_ICONS_STR "\x00"
     "\x00\x00\x00\x13" "\x00\x02"
    STR_MENU_SETTINGS_ICONS_KEY_STR "\x00"
     "\x00\x00\x00\x14" "\x00\x06"
    STR_GROUP_DEVICE_STR ""
     "\x00\x00\x00\x15" "\x00\x0c"
    STR_SETTING_SCSIDEVICE_STR ""
     "\x00\x00\x00\x16" "\x00\x0a"
    STR_SETTING_SCSIUNIT_STR ""
     "\x00\x00\x00\x17" "\x00\x0e"
    STR_SETTING_AUDIOOUTPUT_STR "\x00"
     "\x00\x00\x00\x18" "\x00\x0a"
    STR_SETTING_AHIUNIT_STR "\x00"
     "\x00\x00\x00\x19" "\x00\x0a"
    STR_SETTING_LOGLEVEL_STR ""
     "\x00\x00\x00\x1a" "\x00\x04"
    STR_BUTTON_SAVE_STR ""
     "\x00\x00\x00\x1b" "\x00\x04"
    STR_BUTTON_USE_STR "\x00"
     "\x00\x00\x00\x1c" "\x00\x06"
    STR_BUTTON_CANCEL_STR ""
     "\x00\x00\x00\x1d" "\x00\x04"
    STR_LOGLEVEL_OFF_STR "\x00"
     "\x00\x00\x00\x1e" "\x00\x08"
    STR_LOGLEVEL_FAILURE_STR "\x00"
     "\x00\x00\x00\x1f" "\x00\x06"
    STR_LOGLEVEL_ERROR_STR "\x00"
     "\x00\x00\x00\x20" "\x00\x08"
    STR_LOGLEVEL_WARNING_STR "\x00"
     "\x00\x00\x00\x21" "\x00\x0c"
    STR_LOGLEVEL_INFORMATION_STR "\x00"
     "\x00\x00\x00\x22" "\x00\x06"
    STR_LOGLEVEL_DEBUG_STR "\x00"
     "\x00\x00\x00\x23" "\x00\x06"
    STR_GROUP_MOUNT_STR "\x00"
     "\x00\x00\x00\x24" "\x00\x0e"
    STR_SETTING_BOOTPRIORITY_STR ""
     "\x00\x00\x00\x25" "\x00\x0c"
    STR_SETTING_BOOTDELAY_STR "\x00"
     "\x00\x00\x00\x26" "\x00\x0c"
    STR_ABOUT_CDSCSI_STR ""
     "\x00\x00\x00\x27" "\x00\x18"
    STR_CDSCSI_DESCRIPTION_STR ""
     "\x00\x00\x00\x28" "\x00\x26"
    STR_CDSCSI_TRANSLATOR_STR "\x00"

};

#endif /* CdScsi_BLOCK */


/****************************************************************************/


#ifdef CdScsi_CODE

#ifndef CdScsi_CODE_EXISTS
 #define CdScsi_CODE_EXISTS

 STRPTR GetCdScsiString(struct CdScsi_LocaleInfo *li, LONG stringNum)
 {
 LONG   *l;
 UWORD  *w;
 STRPTR  builtIn;

     l = (LONG *)CdScsi_Block;

     while (*l != stringNum)
       {
       w = (UWORD *)((ULONG)l + 4);
       l = (LONG *)((ULONG)l + (ULONG)*w + 6);
       }
     builtIn = (STRPTR)((ULONG)l + 6);

// #define CdScsi_XLocaleBase LocaleBase
// #define LocaleBase li->li_LocaleBase
    
     if(LocaleBase && li)
        return(GetCatalogStr(li->li_Catalog, stringNum, builtIn));

// #undef  LocaleBase
// #define LocaleBase XLocaleBase
// #undef  CdScsi_XLocaleBase

     return(builtIn);
 }

#else

 STRPTR GetCdScsiString(struct CdScsi_LocaleInfo *li, LONG stringNum);

#endif /* CdScsi_CODE_EXISTS */

#endif /* CdScsi_CODE */


/****************************************************************************/


#endif /* CdScsi_STRINGS_H */
