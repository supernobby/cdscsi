/*
** Version.c
*/


#include "Version.h"


#define NAME_STRING "CdScsi Prefs"
#define MAJOR_STRING EXPAND_AND_QUOTE( CDSCSI_MAJOR )
#define MINOR_STRING EXPAND_AND_QUOTE( CDSCSI_MINOR )
#define REVISION_STRING EXPAND_AND_QUOTE( CDSCSI_REVISION )
#define DATE_STRING __DATE__
#define COPYRIGHTPERIOD_STRING "Copyright 2020-2021"
#define AUTHOR_STRING "Andreas Barth"


/*
** standard version tag
*/
static const char VersionTag[ ] = "\0$VER: " NAME_STRING " " MAJOR_STRING "." MINOR_STRING "." REVISION_STRING " (" DATE_STRING ")" ;
static const char Copyright[ ] = COPYRIGHTPERIOD_STRING " " AUTHOR_STRING ;

/*
** return pointer to the version string including $VER:
*/
char *GetVersionTagString( void )
{
  return( ( char * )&VersionTag[ 1 ] ) ;
}


/*
** return pointer to the version string excluding $VER:
*/
char *GetVersionString( void )
{
  return( ( char * )&VersionTag[ 7 ] ) ;
}


/*
** return pointer to the copyright string
*/
char *GetCopyrightString( void )
{
  return( ( char * )&Copyright[ 0 ] ) ;
}


/*
** return pointer to the author string
*/
char *GetAuthorString( void )
{
  return( ( char * )&Copyright[ 6 ] ) ;
}
