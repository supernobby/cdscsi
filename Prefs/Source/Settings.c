/*
 * Settings.c
 */



#include "Settings.h"
#include <proto/exec.h>


/*
 * set some defaults
 */
void SetDefaultSettings( struct Settings *MySettings )
{
  if( NULL != MySettings )
  {  /* requirements ok */
  }
  else
  {  /* requirements not ok */
  }
}


/*
 * create settings context
 */
struct Settings *CreateSettings( struct Context *MyContext )
{
  struct Settings *MySettings ;
  
  MySettings = NULL ;
  
  if( NULL != MyContext )
  {  /* requirements ok */
    MySettings = AllocVec( sizeof( struct Settings ), MEMF_ANY | MEMF_CLEAR ) ;
    if( NULL != MySettings )
    {  /* memory for main window context ok */
      MySettings->s_Context = MyContext ;
    }
    else
    {  /* requirements not ok */
    }
  }
  return( MySettings ) ;
}


/*
 * delete the settings
 */
void DeleteSettings( struct Settings *MySettings )
{
  if( NULL != MySettings )
  {  /* settings context needs to be freed */
    FreeVec( MySettings ) ;
  }
}
