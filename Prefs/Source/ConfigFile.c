/*
 * ConfigFile.c
 */



#include "ConfigFile.h"
#include <proto/exec.h>
#include <proto/dos.h>
#include <exec/resident.h>
#include <dos/doshunks.h>
#include <proto/dos.h>


/*
 * data for the code hunk in the config file
 */
struct CodeHunkData
{
  struct Resident chd_Resident ;
  BYTE chd_Pad0[ 2 ] ;
  BYTE chd_Name[ 16 ] ;
  BYTE chd_IdString[ 32 ] ;
  struct ConfigFileData chd_ConfigFileData ;
} ;


/*
 * all the data that make up the config file
 */
struct RomConfig
{
  ULONG rc_HunkHeader[ 6 ] ;
  ULONG rc_HunkCode[ 2 ] ;
  struct CodeHunkData rc_CodeHunkData ;
  ULONG rc_HunkReloc32[ 4 + 5 ] ;
  ULONG rc_HunkEnd[ 1 ] ;
} ;


/*
 * global buffer for out config file
 */
struct RomConfig CdScsiRomConfig ;


/*
 * set some defaults
 */
void SetDefaultConfigFileData( struct ConfigFile *MyConfigFile )
{
  if( NULL != MyConfigFile )
  {  /* requirements ok */
    MyConfigFile->cf_ConfigFileData->cfd_Version = CONFIG_VERSION ;
    strcpy( MyConfigFile->cf_ConfigFileData->cfd_ScsiDevice, "scsi.device" ) ;
    MyConfigFile->cf_ConfigFileData->cfd_ScsiUnit = 0 ;
    MyConfigFile->cf_ConfigFileData->cfd_AudioOutput = 0 ;
    MyConfigFile->cf_ConfigFileData->cfd_AhiUnit = 0 ;
    MyConfigFile->cf_ConfigFileData->cfd_LogLevel = 0 ;
    MyConfigFile->cf_ConfigFileData->cfd_BootPriority = 0 ;
    MyConfigFile->cf_ConfigFileData->cfd_BootDelay = 0 ;
  }
  else
  {  /* requirements not ok */
  }
}


/*
 * load settings
 */
void LoadConfigFile( struct ConfigFile *MyConfigFile, UBYTE *FilePath, UBYTE *FileName )
{
  BPTR Segment ;
  struct Resident *ConfigResident ;

  if( NULL != MyConfigFile )
  {  /* requirements ok */
#if 0
    ConfigResident = FindResident( CONFIG_RESIDENT_NAME ) ; 
    if( NULL != ConfigResident )
    {  /* config resident found in rom */
      CopyMem( ConfigResident->rt_Init, MyConfigFile->cf_ConfigFileData, sizeof( struct ConfigFileData ) ) ;
    }
#else
    ConfigResident = NULL ;
#endif
    if( NULL == ConfigResident )
    {  /* load the config file */
      strcpy( MyConfigFile->cf_WorkString, FilePath ) ;
      if( AddPart( MyConfigFile->cf_WorkString, FileName, CONFIGFILE_WORKSTRING_LENGTH ) )
      {  /* complete file path ok */
        Segment = LoadSeg( MyConfigFile->cf_WorkString ) ;
        if( NULL != Segment )
        {  /* config file ok */
          ConfigResident = ( struct Resident * )( ( ( ( ULONG )BADDR( Segment ) ) + sizeof( BPTR ) ) ) ;
          if( RTC_MATCHWORD == ConfigResident->rt_MatchWord )
          {  /* expected data found */
            CopyMem( ConfigResident->rt_Init, MyConfigFile->cf_ConfigFileData, sizeof( struct ConfigFileData ) ) ;
          }
          UnLoadSeg( Segment ) ;
        }
        else
        {  /* config file not ok */
        }
      }
      else
      {  /* complete file path not ok */
      }
    }
  }
  else
  {  /* requirements not ok */
  }
}


/*
 * save settings in the config file
 */
void SaveConfigFile( struct ConfigFile *MyConfigFile, UBYTE *FilePath, UBYTE *FileName )
{
  BPTR ConfigFile ;
  struct ConfigFileData *MyConfigFileData ;
  
  MyConfigFileData = &CdScsiRomConfig.rc_CodeHunkData.chd_ConfigFileData ;
  MyConfigFileData->cfd_Version = CONFIG_VERSION ;

  CdScsiRomConfig.rc_HunkHeader[ 0 ] = HUNK_HEADER ;
  CdScsiRomConfig.rc_HunkHeader[ 1 ] = 0x00000000 ;  /* no hunk names */
  CdScsiRomConfig.rc_HunkHeader[ 2 ] = 0x00000001 ;  /* only one hunk */
  CdScsiRomConfig.rc_HunkHeader[ 3 ] = 0x00000000 ;  /* load hunk 0 first */
  CdScsiRomConfig.rc_HunkHeader[ 4 ] = 0x00000000 ;  /* load hunk 0 last */
  CdScsiRomConfig.rc_HunkHeader[ 5 ] = ( sizeof( struct CodeHunkData ) ) / 4 ;  /* size of hunk 0 in longwords */
  CdScsiRomConfig.rc_HunkCode[ 0 ] = HUNK_CODE ;
  CdScsiRomConfig.rc_HunkCode[ 1 ] = ( sizeof( struct CodeHunkData ) ) / 4 ;  /* size of this hunk 0 longwords */
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_MatchWord = RTC_MATCHWORD ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_MatchTag =  /* 1. relocation */
    ( struct Resident * )( ( ULONG )&CdScsiRomConfig.rc_CodeHunkData.chd_Resident - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ) ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_EndSkip =  /* 2. relocation */
    ( APTR )( ( ULONG )MyConfigFileData - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ) ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Flags = 0 ;  /* do not call us */
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Version = 0 ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Type = NT_UNKNOWN ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Pri = 0 ;
  strcpy( CdScsiRomConfig.rc_CodeHunkData.chd_Name, CONFIG_RESIDENT_NAME ) ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Name =  /* 3. relocation */
    ( BYTE * )( ( ULONG )&CdScsiRomConfig.rc_CodeHunkData.chd_Name - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ) ;
  strcpy( CdScsiRomConfig.rc_CodeHunkData.chd_IdString, "your CdScsi config" ) ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_IdString =  /* 4. relocation */
    ( BYTE * )( ( ULONG )CdScsiRomConfig.rc_CodeHunkData.chd_IdString - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ) ;
  CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Init =  /* 5. relocation */
    ( APTR )( ( ULONG )MyConfigFileData - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ) ;
  CdScsiRomConfig.rc_HunkReloc32[ 0 ] = HUNK_RELOC32 ;
  CdScsiRomConfig.rc_HunkReloc32[ 1 ] = 5 ;  /* number of relocations */
  CdScsiRomConfig.rc_HunkReloc32[ 2 ] = 0 ;  /* relocations apply to this hunk */
  CdScsiRomConfig.rc_HunkReloc32[ 3 ] =  /* 1. relocation */
    ( ULONG )&CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_MatchTag - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ;
  CdScsiRomConfig.rc_HunkReloc32[ 4 ] =  /* 1. relocation */
    ( ULONG )&CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_EndSkip - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ;
  CdScsiRomConfig.rc_HunkReloc32[ 5 ] =  /* 3. relocation */
    ( ULONG )&CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Name - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ;
  CdScsiRomConfig.rc_HunkReloc32[ 6 ] =  /* 4. relocation */
    ( ULONG )&CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_IdString - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ;
  CdScsiRomConfig.rc_HunkReloc32[ 7 ] =  /* 5. relocation */
    ( ULONG )&CdScsiRomConfig.rc_CodeHunkData.chd_Resident.rt_Init - ( ULONG )&CdScsiRomConfig.rc_CodeHunkData ;
  CdScsiRomConfig.rc_HunkReloc32[ 8 ] = 0 ;  /* no more relocations */
  CdScsiRomConfig.rc_HunkEnd[ 0 ] = HUNK_END ;

  strcpy( MyConfigFile->cf_WorkString, FilePath ) ;
  if( AddPart( MyConfigFile->cf_WorkString, FileName, CONFIGFILE_WORKSTRING_LENGTH ) )
  {  /* complete file path ok */
    ConfigFile = Open( MyConfigFile->cf_WorkString, MODE_NEWFILE ) ;
    if( NULL != ConfigFile )
    {  /* file ok */
      Write( ConfigFile, &CdScsiRomConfig, sizeof( CdScsiRomConfig ) ) ;
      Close( ConfigFile ) ;
    }
  }
}



/*
 * create settings context
 */
struct ConfigFile *CreateConfigFile( struct Context *MyContext )
{
  struct ConfigFile *MyConfigFile ;
  
  MyConfigFile = NULL ;
  
  if( NULL != MyContext )
  {  /* requirements ok */
    MyConfigFile = AllocVec( sizeof( struct ConfigFile ), MEMF_ANY | MEMF_CLEAR ) ;
    if( NULL != MyConfigFile )
    {  /* memory for main window context ok */
      MyConfigFile->cf_Context = MyContext ;
      MyConfigFile->cf_ConfigFileData = &CdScsiRomConfig.rc_CodeHunkData.chd_ConfigFileData ;
    }
    else
    {  /* requirements not ok */
    }
  }
  return( MyConfigFile ) ;
}


/*
 * delete the settings
 */
void DeleteConfigFile( struct ConfigFile *MyConfigFile )
{
  if( NULL != MyConfigFile )
  {  /* settings context needs to be freed */
    FreeVec( MyConfigFile ) ;
  }
}
