/*
 * MainWindow.c
 */


#include "MainWindow.h"
#include "Localization.h"
#include "ConfigFile.h"
#include "About.h"
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/asl.h>
#include <proto/window.h>
#include <classes/window.h>
#include <proto/layout.h>
#include <gadgets/layout.h>
#include <proto/button.h>
#include <gadgets/button.h>
#include <proto/string.h>
#include <gadgets/string.h>
#include <proto/integer.h>
#include <gadgets/integer.h>
#include <proto/chooser.h>
#include <gadgets/chooser.h>
#include <images/label.h>
#include <proto/label.h>
#include <clib/alib_protos.h>


/*
 * update config file with gadget status
 */
static void GadgetsToConfigFileData( struct MainWindow *MyMainWindow )
{
  struct Context *MyContext ;
  struct ConfigFileData *MyConfigFileData ;
  STRPTR TextVal ;
  LONG LongVal ;
  
  if( NULL != MyMainWindow )
  {  /* main window context ok */
    MyContext = MyMainWindow->mw_Context ;
    MyConfigFileData = MyContext->c_ConfigFile->cf_ConfigFileData ;

    GetAttr( STRINGA_TextVal, MyMainWindow->mw_Gadgets[ GID_SCSIDEVICE ], ( ULONG * )&TextVal ) ;
    strcpy( MyConfigFileData->cfd_ScsiDevice, TextVal ) ;
    GetAttr( CHOOSER_Selected, MyMainWindow->mw_Gadgets[ GID_SCSIUNIT ], &LongVal ) ;
    MyConfigFileData->cfd_ScsiUnit = LongVal ;
    GetAttr( CHOOSER_Selected, MyMainWindow->mw_Gadgets[ GID_AUDIOOUTPUT ], &LongVal ) ;
    MyConfigFileData->cfd_AudioOutput = LongVal ;
    GetAttr( CHOOSER_Selected, MyMainWindow->mw_Gadgets[ GID_AHIUNIT ], &LongVal ) ;
    MyConfigFileData->cfd_AhiUnit = LongVal ;
    GetAttr( CHOOSER_Selected, MyMainWindow->mw_Gadgets[ GID_LOGLEVEL ], &LongVal ) ;
    MyConfigFileData->cfd_LogLevel = LongVal ;
    GetAttr( INTEGER_Number, MyMainWindow->mw_Gadgets[ GID_BOOTPRIORITY ], &LongVal ) ;
    MyConfigFileData->cfd_BootPriority = LongVal ;
    GetAttr( INTEGER_Number, MyMainWindow->mw_Gadgets[ GID_BOOTDELAY ], &LongVal ) ;
    MyConfigFileData->cfd_BootDelay = LongVal ;
  }
  else
  {  /* main window context not ok */
  }
}


/*
 * update gadgets with config file data
 */
static void ConfigFileDataToGadgets( struct MainWindow *MyMainWindow )
{
  struct Context *MyContext ;
  struct ConfigFileData *MyConfigFileData ;
  
  if( NULL != MyMainWindow )
  {  /* main window context ok */
    MyContext = MyMainWindow->mw_Context ;
    MyConfigFileData = MyContext->c_ConfigFile->cf_ConfigFileData ;
    
    SetGadgetAttrs( MyMainWindow->mw_Gadgets[ GID_SCSIDEVICE ], MyMainWindow->mw_Window, NULL,
      STRINGA_TextVal, MyConfigFileData->cfd_ScsiDevice,
      TAG_END ) ;
    SetGadgetAttrs( MyMainWindow->mw_Gadgets[ GID_SCSIUNIT ], MyMainWindow->mw_Window, NULL,
      CHOOSER_Selected, MyConfigFileData->cfd_ScsiUnit,
      TAG_END ) ;
    SetGadgetAttrs( MyMainWindow->mw_Gadgets[ GID_AUDIOOUTPUT ], MyMainWindow->mw_Window, NULL,
      CHOOSER_Selected, MyConfigFileData->cfd_AudioOutput,
      TAG_END ) ;
    SetGadgetAttrs( MyMainWindow->mw_Gadgets[ GID_AHIUNIT ], MyMainWindow->mw_Window, NULL,
      CHOOSER_Selected, MyConfigFileData->cfd_AhiUnit,
      TAG_END ) ;
    SetGadgetAttrs( MyMainWindow->mw_Gadgets[ GID_LOGLEVEL ], MyMainWindow->mw_Window, NULL,
      CHOOSER_Selected, MyConfigFileData->cfd_LogLevel,
      TAG_END ) ;
    SetGadgetAttrs( MyMainWindow->mw_Gadgets[ GID_BOOTPRIORITY ], MyMainWindow->mw_Window, NULL,
      INTEGER_Number, MyConfigFileData->cfd_BootPriority,
      TAG_END ) ;
    if( 1 <= MyConfigFileData->cfd_Version )
    {  /* boot delay value avialable in config */
      SetGadgetAttrs( MyMainWindow->mw_Gadgets[ GID_BOOTDELAY ], MyMainWindow->mw_Window, NULL,
        INTEGER_Number, MyConfigFileData->cfd_BootDelay,
        TAG_END ) ;
    }
  }
  else
  {  /* main window context not ok */
  }
}


/*
 * helper function to open the main window
 */
static void OpenMainWindow( struct MainWindow *MyMainWindow )
{
  MyMainWindow->mw_Screen = LockPubScreen( NULL ) ;
  MyMainWindow->mw_VisualInfo = GetVisualInfo( MyMainWindow->mw_Screen, TAG_END ) ;
  LayoutMenus( MyMainWindow->mw_MenuStrip, MyMainWindow->mw_VisualInfo, GTMN_NewLookMenus, TRUE, TAG_END ) ;
  MyMainWindow->mw_Window = ( struct Window * )DoMethod( MyMainWindow->mw_WindowObject, WM_OPEN ) ;
  if( NULL != MyMainWindow->mw_Window )
  {  /* main window is open */
  }
}


/*
 * helper function to close the main window
 */
static void CloseMainWindow( struct MainWindow *MyMainWindow, ULONG Method )
{
  if( NULL != MyMainWindow->mw_Window )
  {  /* main window can be closed */
    DoMethod( MyMainWindow->mw_WindowObject, Method ) ;
    MyMainWindow->mw_Window = NULL ;
  }
  FreeVisualInfo( MyMainWindow->mw_VisualInfo ) ;
  MyMainWindow->mw_VisualInfo = NULL ;
  UnlockPubScreen( NULL, MyMainWindow->mw_Screen ) ;
  MyMainWindow->mw_Screen = NULL ;
}



/*
 * delete the main window
 */
void DeleteMainWindow( struct MainWindow *MyMainWindow )
{
  struct Context *MyContext ;
  struct Node *ChooserNode ;

  if( NULL != MyMainWindow )
  {  /* main window context needs to be freed */
    MyContext = MyMainWindow->mw_Context ;
    if( NULL != MyMainWindow->mw_AppPort )
    {  /* app message port need to be deleted */
      if( NULL != MyMainWindow->mw_WindowObject )
      {  /* main window object needs to be disposed */
        if( NULL != MyMainWindow->mw_MenuStrip )
        {  /* menu strip needs to be freed */
          if( NULL != MyMainWindow->mw_Window )
          {  /* main window needs to be closed */
            RemDispatchee( MyContext->c_Dispatcher, ( struct Dispatchee * )MyMainWindow ) ; 
            CloseMainWindow( MyMainWindow, WM_CLOSE ) ;
          }
          FreeMenus( MyMainWindow->mw_MenuStrip ) ;
          MyMainWindow->mw_MenuStrip = NULL ;
        }
        DisposeObject( MyMainWindow->mw_WindowObject ) ;
        MyMainWindow->mw_WindowObject = NULL ;
        while( ChooserNode = RemHead( &MyMainWindow->mw_LogLevelChooserLabels ) )
        {
          FreeChooserNode( ChooserNode ) ;
        }
        while( ChooserNode = RemHead( &MyMainWindow->mw_AhiUnitChooserLabels ) )
        {
          FreeChooserNode( ChooserNode ) ;
        }
         while( ChooserNode = RemHead( &MyMainWindow->mw_AudioOutputChooserLabels ) )
        {
          FreeChooserNode( ChooserNode ) ;
        }
         while( ChooserNode = RemHead( &MyMainWindow->mw_ScsiUnitChooserLabels ) )
        {
          FreeChooserNode( ChooserNode ) ;
        }
      }
      DeleteMsgPort( MyMainWindow->mw_AppPort ) ;
      MyMainWindow->mw_AppPort = NULL ;
    }
    FreeVec( MyMainWindow ) ;
  }
}



STRPTR ScsiUnitChooserStrings[ ] =
{
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  NULL
} ;


UBYTE *AudioOutputChooserStrings[ ] =
{
  "CD-ROM",
  "AHI",
  NULL
} ;


UBYTE *AhiUnitChooserStrings[ ] =
{
  "0",
  "1",
  "2",
  "3",
  NULL
} ;



UBYTE *LogLevelChooserStrings[ 7 ] ;



struct NewMenu MainWindowMenuSpec[ ] =
{
  { NM_TITLE, NULL, NULL, 0, 0, NULL },  /* project */
    { NM_ITEM, NULL, NULL, 0, 0, NULL },  /* open */
    { NM_ITEM, NULL, NULL, 0, 0, NULL },  /* save as */
    { NM_ITEM, NM_BARLABEL, NULL, 0, 0, NULL },
    { NM_ITEM, NULL, NULL, 0, 0, NULL },  /* about */
    { NM_ITEM, NM_BARLABEL, NULL, 0, 0, NULL },
    { NM_ITEM, NULL, NULL, 0, 0, NULL },  /* quit */
  { NM_TITLE, NULL, NULL, 0, 0, NULL },  /* edit */
    { NM_ITEM, NULL, NULL, 0, 0, NULL },  /* default */
    { NM_ITEM, NULL, NULL, 0, 0, NULL },  /* saved */
    { NM_ITEM, NULL, NULL, 0, 0, NULL },  /* restore */
  { NM_TITLE, NULL, NULL, 0, 0, NULL },  /* settings */
    { NM_ITEM, NULL, NULL, ( CHECKIT | MENUTOGGLE ), 0, NULL },  /* icons */
  { NM_END , NULL, NULL , 0, 0, NULL }
} ;


#define MID_OPEN FULLMENUNUM( 0, 0, NOSUB )
#define MID_SAVEAS FULLMENUNUM( 0, 1, NOSUB )
#define MID_ABOUT FULLMENUNUM( 0, 3, NOSUB )
#define MID_QUIT FULLMENUNUM( 0, 5, NOSUB )
#define MID_DEFAULTS FULLMENUNUM( 1, 0, NOSUB )
#define MID_SAVED FULLMENUNUM( 1, 1, NOSUB )
#define MID_RESTORE FULLMENUNUM( 1, 2, NOSUB )
#define MID_ICONS FULLMENUNUM( 2, 0, NOSUB )


/*
 * create the main window
 */
struct MainWindow *CreateMainWindow( struct Context *MyContext )
{
  struct MainWindow *MyMainWindow ;
  APTR RootLayout ;
  APTR HorizontalGroup, DeviceGroup, MountGroup, GadgetGroup, LabelObject ;
  ULONG Count ;

  MyMainWindow = NULL ;
  
  if( NULL != MyContext )
  {  /* requirements seem ok */
    MyMainWindow = AllocVec( sizeof( struct MainWindow ), MEMF_ANY | MEMF_CLEAR ) ;
    if( NULL != MyMainWindow )
    {  /* memory for main window context ok */
      MyMainWindow->mw_Context = MyContext ;
      MyMainWindow->mw_AppPort = CreateMsgPort( ) ;
      if( NULL != MyMainWindow->mw_AppPort )
      {  /* app message port ok */
        MyMainWindow->mw_WindowObject = NewObject( WINDOW_GetClass( ), NULL,
          WA_DragBar, TRUE,  /* main window should have a dragbar */
          WA_CloseGadget, TRUE,  /* main window should have no close gadget */
          WA_GimmeZeroZero, FALSE,  /* main window should not use gimme zero zero */
          WA_DepthGadget, TRUE,  /* main window should have a depth gadget */
          WA_SizeGadget, FALSE,  /* main window should have no size gadget */
          WA_Activate, TRUE,  /* main window shall get the focus */
          WA_Title, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_MAINWINDOW_TITLE ),
          WINDOW_AppPort, ( ULONG )MyMainWindow->mw_AppPort,  /* main window should be inconifyable */
          WINDOW_IconifyGadget, TRUE,  /* main window should have a iconify gadget */
          WINDOW_Icon, ( ULONG )MyContext->c_DiskObject,
          WINDOW_IconTitle, ( ULONG )"CdScsi",
          WINDOW_Position, WPOS_CENTERMOUSE,
          TAG_END ) ;
        if( NULL != MyMainWindow->mw_WindowObject )
        {  /* main window object ok */
          /* root layout */
          RootLayout = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_VERTICAL,
            LAYOUT_SpaceOuter, TRUE,
            TAG_END ) ;
          SetAttrs( MyMainWindow->mw_WindowObject,
            WINDOW_Layout, ( ULONG )RootLayout,
            TAG_END ) ;
          /* main horizontal group for device and mount settings */
          HorizontalGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_HORIZONTAL,
            LAYOUT_BevelStyle, BVS_NONE,
            TAG_END ) ;
          SetAttrs( RootLayout,
            LAYOUT_AddChild, ( ULONG )HorizontalGroup,
            TAG_END ) ;
          /* device group layout */
          DeviceGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_HORIZONTAL,
            LAYOUT_BevelStyle, BVS_GROUP,
            LAYOUT_Label, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_GROUP_DEVICE ),
            LAYOUT_SpaceOuter, TRUE,
            TAG_END ) ;
          SetAttrs( HorizontalGroup,
            LAYOUT_AddChild, ( ULONG )DeviceGroup,
            TAG_END ) ;
          /* device labels group */
          GadgetGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_VERTICAL,
            LAYOUT_HorizAlignment, LALIGN_RIGHT,
            TAG_END ) ;
          SetAttrs( DeviceGroup,
            LAYOUT_AddChild, ( ULONG )GadgetGroup,
            TAG_END ) ;
          /* scsi device label */
          LabelObject = NewObject( BUTTON_GetClass( ), NULL,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_SETTING_SCSIDEVICE ),
            GA_ReadOnly, TRUE,
            BUTTON_BevelStyle, BVS_NONE,
            BUTTON_Justification, BCJ_RIGHT,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )LabelObject,
            CHILD_WeightedWidth, 100,
            TAG_END ) ;
          /* scsi unit label */
          LabelObject = NewObject( BUTTON_GetClass( ), NULL,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_SETTING_SCSIUNIT ),
            GA_ReadOnly, TRUE,
            BUTTON_BevelStyle, BVS_NONE,
            BUTTON_Justification, BCJ_RIGHT,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )LabelObject,
            CHILD_WeightedWidth, 100,
            TAG_END ) ;
          /* audio output label */
          LabelObject = NewObject( BUTTON_GetClass( ), NULL,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_SETTING_AUDIOOUTPUT ),
            GA_ReadOnly, TRUE,
            BUTTON_BevelStyle, BVS_NONE,
            BUTTON_Justification, BCJ_RIGHT,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )LabelObject,
            CHILD_WeightedWidth, 100,
            TAG_END ) ;
          /* ahi unit label */
          LabelObject = NewObject( BUTTON_GetClass( ), NULL,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_SETTING_AHIUNIT ),
            GA_ReadOnly, TRUE,
            BUTTON_BevelStyle, BVS_NONE,
            BUTTON_Justification, BCJ_RIGHT,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )LabelObject,
            CHILD_WeightedWidth, 100,
            TAG_END ) ;
          /* log level label */
          LabelObject = NewObject( BUTTON_GetClass( ), NULL,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_SETTING_LOGLEVEL ),
            GA_ReadOnly, TRUE,
            BUTTON_BevelStyle, BVS_NONE,
            BUTTON_Justification, BCJ_RIGHT,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )LabelObject,
            CHILD_WeightedWidth, 100,
            TAG_END ) ;
          /* device group gadgets */
          GadgetGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_VERTICAL,
            LAYOUT_VertAlignment, LALIGN_CENTER,
            TAG_END ) ;
          SetAttrs( DeviceGroup,
            LAYOUT_AddChild, ( ULONG )GadgetGroup,
            TAG_END ) ;
          /* scsi device string object */
          MyMainWindow->mw_Gadgets[ GID_SCSIDEVICE ] = NewObject( STRING_GetClass( ), NULL,
            GA_ID, GID_SCSIDEVICE,
            STRINGA_MaxChars, CONFIG_SCSIDEVICE_LENGTH,
            STRINGA_MinVisible, 16,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_SCSIDEVICE ],
            CHILD_WeightedWidth, 0,
            TAG_END ) ;
          /* scsi unit chooser object */
          NewList( &MyMainWindow->mw_ScsiUnitChooserLabels ) ;
          Count = 0 ;
          while( NULL != ScsiUnitChooserStrings[ Count ] )
          {  /* create chooser labels */
            AddTail( &MyMainWindow->mw_ScsiUnitChooserLabels,
                       AllocChooserNode( CNA_Text, ScsiUnitChooserStrings[ Count ], TAG_END ) ) ;
            Count++ ;
          }
          MyMainWindow->mw_Gadgets[ GID_SCSIUNIT ] = NewObject( CHOOSER_GetClass( ), NULL,
            GA_ID, GID_SCSIUNIT,
            CHOOSER_Labels, &MyMainWindow->mw_ScsiUnitChooserLabels,
            CHOOSER_MaxLabels, Count,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_SCSIUNIT ],
            CHILD_WeightedWidth, 0,
            TAG_END ) ;
          /* audio output chooser object */
          NewList( &MyMainWindow->mw_AudioOutputChooserLabels ) ;
          Count = 0 ;
          while( NULL != AudioOutputChooserStrings[ Count ] )
          {  /* create chooser labels */
            AddTail( &MyMainWindow->mw_AudioOutputChooserLabels,
                       AllocChooserNode( CNA_Text, AudioOutputChooserStrings[ Count ], TAG_END ) ) ;
            Count++ ;
          }
          MyMainWindow->mw_Gadgets[ GID_AUDIOOUTPUT ] = NewObject( CHOOSER_GetClass( ), NULL,
            GA_ID, GID_AUDIOOUTPUT,
            CHOOSER_Labels, &MyMainWindow->mw_AudioOutputChooserLabels,
            CHOOSER_MaxLabels, Count,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_AUDIOOUTPUT ],
            CHILD_WeightedWidth, 0,
            TAG_END ) ;
          /* ahi unit chooser object */
          NewList( &MyMainWindow->mw_AhiUnitChooserLabels ) ;
          Count = 0 ;
          while( NULL != AhiUnitChooserStrings[ Count ] )
          {  /* create chooser labels */
            AddTail( &MyMainWindow->mw_AhiUnitChooserLabels,
                       AllocChooserNode( CNA_Text, AhiUnitChooserStrings[ Count ], TAG_END ) ) ;
            Count++ ;
          }
          MyMainWindow->mw_Gadgets[ GID_AHIUNIT ] = NewObject( CHOOSER_GetClass( ), NULL,
            GA_ID, GID_AHIUNIT,
            CHOOSER_Labels, &MyMainWindow->mw_AhiUnitChooserLabels,
            CHOOSER_MaxLabels, Count,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_AHIUNIT ],
            CHILD_WeightedWidth, 0,
            TAG_END ) ;
          /* loglevel chooser object */
          LogLevelChooserStrings[ 0 ] = GetLocalizedString( MyContext->c_Localization, STR_LOGLEVEL_OFF ) ;
          LogLevelChooserStrings[ 1 ] = GetLocalizedString( MyContext->c_Localization, STR_LOGLEVEL_FAILURE ) ;
          LogLevelChooserStrings[ 2 ] = GetLocalizedString( MyContext->c_Localization, STR_LOGLEVEL_ERROR ) ;
          LogLevelChooserStrings[ 3 ] = GetLocalizedString( MyContext->c_Localization, STR_LOGLEVEL_WARNING ) ;
          LogLevelChooserStrings[ 4 ] = GetLocalizedString( MyContext->c_Localization, STR_LOGLEVEL_INFORMATION ) ;
          LogLevelChooserStrings[ 5 ] = GetLocalizedString( MyContext->c_Localization, STR_LOGLEVEL_DEBUG ) ;
          LogLevelChooserStrings[ 6 ] = NULL ;
          NewList( &MyMainWindow->mw_LogLevelChooserLabels ) ;
          Count = 0 ;
          while( NULL != LogLevelChooserStrings[ Count ] )
          {  /* create chooser labels */
            AddTail( &MyMainWindow->mw_LogLevelChooserLabels,
                       AllocChooserNode( CNA_Text, LogLevelChooserStrings[ Count ], TAG_END ) ) ;
            Count++ ;
          }
          MyMainWindow->mw_Gadgets[ GID_LOGLEVEL ] = NewObject( CHOOSER_GetClass( ), NULL,
            GA_ID, GID_LOGLEVEL,
            CHOOSER_Labels, &MyMainWindow->mw_LogLevelChooserLabels,
            CHOOSER_MaxLabels, Count,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_LOGLEVEL ],
            CHILD_WeightedWidth, 0,
            TAG_END ) ;
          /* mount group layout */
          MountGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_HORIZONTAL,
            LAYOUT_BevelStyle, BVS_GROUP,
            LAYOUT_Label, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_GROUP_MOUNT ),
            LAYOUT_SpaceOuter, TRUE,
            TAG_END ) ;
          SetAttrs( HorizontalGroup,
            LAYOUT_AddChild, ( ULONG )MountGroup,
            TAG_END ) ;
          /* mount labels group */
          GadgetGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_VERTICAL,
            LAYOUT_HorizAlignment, LALIGN_RIGHT,
            TAG_END ) ;
          SetAttrs( MountGroup,
            LAYOUT_AddChild, ( ULONG )GadgetGroup,
            TAG_END ) ;
          /* boot priority label */
          LabelObject = NewObject( BUTTON_GetClass( ), NULL,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_SETTING_BOOTPRIORITY ),
            GA_ReadOnly, TRUE,
            BUTTON_BevelStyle, BVS_NONE,
            BUTTON_Justification, BCJ_RIGHT,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )LabelObject,
            CHILD_WeightedWidth, 100,
            TAG_END ) ;
          /* boot delay label */
          LabelObject = NewObject( BUTTON_GetClass( ), NULL,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_SETTING_BOOTDELAY ),
            GA_ReadOnly, TRUE,
            BUTTON_BevelStyle, BVS_NONE,
            BUTTON_Justification, BCJ_RIGHT,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )LabelObject,
            CHILD_WeightedWidth, 100,
            TAG_END ) ;
          /* mount gadgets group */
          GadgetGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_VERTICAL,
            LAYOUT_VertAlignment, LALIGN_CENTER,
            TAG_END ) ;
          SetAttrs( MountGroup,
            LAYOUT_AddChild, ( ULONG )GadgetGroup,
            TAG_END ) ;
          /* boot priority integer object */
          MyMainWindow->mw_Gadgets[ GID_BOOTPRIORITY ] = NewObject( INTEGER_GetClass( ), NULL,
            GA_ID, GID_BOOTPRIORITY,
            INTEGER_Maximum, 128,
            INTEGER_Minimum, -127,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_BOOTPRIORITY ],
            CHILD_WeightedWidth, 0,
            TAG_END ) ;
          /* boot delay integer object */
          MyMainWindow->mw_Gadgets[ GID_BOOTDELAY ] = NewObject( INTEGER_GetClass( ), NULL,
            GA_ID, GID_BOOTDELAY,
            INTEGER_Maximum, 100,
            INTEGER_Minimum, 0,
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_BOOTDELAY ],
            CHILD_WeightedWidth, 0,
            TAG_END ) ;
          /* bottom button group */
          GadgetGroup = NewObject( LAYOUT_GetClass( ), NULL,
            LAYOUT_Orientation, LAYOUT_HORIZONTAL,
            LAYOUT_EvenSize, TRUE,
            TAG_END ) ;
          SetAttrs( RootLayout,
            LAYOUT_AddChild, ( ULONG )GadgetGroup,
            TAG_END ) ;
          /* save button object */
          MyMainWindow->mw_Gadgets[ GID_SAVE ] = NewObject( BUTTON_GetClass( ), NULL,
            GA_ID, GID_SAVE,
            GA_RelVerify, TRUE,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_BUTTON_SAVE ),
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_SAVE ],
            CHILD_WeightedWidth, 0,
            CHILD_WeightedHeight, 0,
            TAG_END ) ;
          /* use button object */
          MyMainWindow->mw_Gadgets[ GID_USE ] = NewObject( BUTTON_GetClass( ), NULL,
            GA_ID, GID_USE,
            GA_RelVerify, TRUE,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_BUTTON_USE ),
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_USE ],
            CHILD_WeightedWidth, 0,
            CHILD_WeightedHeight, 0,
            TAG_END ) ;
          /* cancel button object */
          MyMainWindow->mw_Gadgets[ GID_CANCEL ] = NewObject( BUTTON_GetClass( ), NULL,
            GA_ID, GID_CANCEL,
            GA_RelVerify, TRUE,
            GA_Text, ( ULONG )GetLocalizedString( MyContext->c_Localization, STR_BUTTON_CANCEL ),
            TAG_END ) ;
          SetAttrs( GadgetGroup,
            LAYOUT_AddChild, ( ULONG )MyMainWindow->mw_Gadgets[ GID_CANCEL ],
            CHILD_WeightedWidth, 0,
            CHILD_WeightedHeight, 0,
            TAG_END ) ;
          /* fill gadgets with actual values */
          ConfigFileDataToGadgets( MyMainWindow ) ;
          /* get localized menu texts for menu strip */
          MainWindowMenuSpec[ 0 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT ) ;
          MainWindowMenuSpec[ 1 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_OPEN ) ;
          MainWindowMenuSpec[ 1 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_OPEN_KEY ) ;
          MainWindowMenuSpec[ 2 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_SAVEAS ) ;
          MainWindowMenuSpec[ 2 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_SAVEAS_KEY ) ;
          MainWindowMenuSpec[ 4 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_ABOUT ) ;
          MainWindowMenuSpec[ 4 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_ABOUT_KEY ) ;
          MainWindowMenuSpec[ 6 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_QUIT ) ;
          MainWindowMenuSpec[ 6 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_PROJECT_QUIT_KEY ) ;
          MainWindowMenuSpec[ 7 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_EDIT ) ;
          MainWindowMenuSpec[ 8 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_EDIT_DEFAULTS ) ;
          MainWindowMenuSpec[ 8 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_EDIT_DEFAULTS_KEY ) ;
          MainWindowMenuSpec[ 9 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_EDIT_SAVED ) ;
          MainWindowMenuSpec[ 9 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_EDIT_SAVED_KEY ) ;
          MainWindowMenuSpec[ 10 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_EDIT_RESTORE ) ;
          MainWindowMenuSpec[ 10 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_EDIT_RESTORE_KEY ) ;
          MainWindowMenuSpec[ 11 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_SETTINGS ) ;
          MainWindowMenuSpec[ 12 ].nm_Label = GetLocalizedString( MyContext->c_Localization, STR_MENU_SETTINGS_ICONS ) ;
          MainWindowMenuSpec[ 12 ].nm_CommKey = GetLocalizedString( MyContext->c_Localization, STR_MENU_SETTINGS_ICONS_KEY ) ;
          MyMainWindow->mw_MenuStrip = CreateMenus( MainWindowMenuSpec, GTMN_FullMenu, TRUE, TAG_END ) ;
          if( NULL != MyMainWindow->mw_MenuStrip )
          {  /* menu strip ok */
            SetAttrs( MyMainWindow->mw_WindowObject,
              WINDOW_MenuStrip, ( ULONG )MyMainWindow->mw_MenuStrip,
              TAG_END ) ;
            OpenMainWindow( MyMainWindow ) ;
            if( NULL != MyMainWindow->mw_Window )
            {  /* main window did open */
              GetAttr( WINDOW_SigMask, MyMainWindow->mw_WindowObject, &MyMainWindow->mw_SignalMask ) ;
              MyMainWindow->mw_SignalMask |= ( 1UL << MyMainWindow->mw_AppPort->mp_SigBit ) ;
              MyMainWindow->mw_Dispatchee.d_Signals = MyMainWindow->mw_SignalMask ;
              MyMainWindow->mw_Dispatchee.d_DoFunction = ( DISPATCHER_DOFUNCTION )DoMainWindow ;
              MyMainWindow->mw_Dispatchee.d_DoData = MyMainWindow ;                              
              AddDispatchee( MyContext->c_Dispatcher, ( struct Dispatchee * )MyMainWindow ) ; 
            }
            else
            {  /* main window did not open */
              DeleteMainWindow( MyMainWindow ) ;
              MyMainWindow = NULL ;
            }
          }
          else
          {  /* menu strip not ok */
            DeleteMainWindow( MyMainWindow ) ;
            MyMainWindow = NULL ;
          }
        }
        else
        {  /* main window object not ok */
          DeleteMainWindow( MyMainWindow ) ;
          MyMainWindow = NULL ;
        }
      }
      else
      {  /* app message port not ok */
        DeleteMainWindow( MyMainWindow ) ;
        MyMainWindow = NULL ;
      }
    }
    else
    {  /* memory for main window context not ok */
    }
  }
  else
  {  /* requirements not ok */
  }

  return( MyMainWindow ) ;
}


/*
 * handle main window events
 */
ULONG DoMainWindow( struct MainWindow *MyMainWindow, ULONG TriggerSignals )
{
  struct Context *MyContext ;
  ULONG Result, Code ;
  struct FileRequester *MyFileRequester ;
  
  MyContext = MyMainWindow->mw_Context ;

  while( Result = DoMethod( MyMainWindow->mw_WindowObject, WM_HANDLEINPUT, &Code ) )
  {
    //Printf( "WMHI: 0x%lX\n", WMHI_CLASSMASK & Result ) ;
    switch( WMHI_CLASSMASK & Result )
    {
      case WMHI_CLOSEWINDOW:
        MyContext->c_ShutdownRequest = 1 ;
        break ;
      case WMHI_GADGETUP:
        switch( Result & WMHI_GADGETMASK )
        {
          case GID_SAVE:
            GadgetsToConfigFileData( MyMainWindow ) ;
            SaveConfigFile( MyContext->c_ConfigFile, "ENV:", "cdscsi.config" ) ;
            SaveConfigFile( MyContext->c_ConfigFile, "ENVARC:", "cdscsi.config" ) ;
            MyContext->c_ShutdownRequest = 1 ;
            break ;
          case GID_USE:
            GadgetsToConfigFileData( MyMainWindow ) ;
            SaveConfigFile( MyContext->c_ConfigFile, "ENV:", "cdscsi.config" ) ;
            MyContext->c_ShutdownRequest = 1 ;
            break ;
          case GID_CANCEL:
            MyContext->c_ShutdownRequest = 1 ;
            break ;
        } ;
        break ;
      case WMHI_MENUPICK:
        switch( ( Result & WMHI_MENUMASK ) )
        {
          case MID_OPEN:
            MyFileRequester = AllocAslRequestTags( ASL_FileRequest,
              ASLFR_Window, MyMainWindow->mw_Window,
              ASLFR_SleepWindow, TRUE,
              ASLFR_DoSaveMode, FALSE,
              TAG_END ) ;
            if( NULL != MyFileRequester )
            {  /* file requester ok */
              if( AslRequestTags( MyFileRequester, TAG_END ) )
              {  /* some file was selected */
                LoadConfigFile( MyContext->c_ConfigFile, MyFileRequester->fr_Drawer, MyFileRequester->fr_File ) ;
                ConfigFileDataToGadgets( MyMainWindow ) ;
              }
              FreeAslRequest( MyFileRequester ) ;
            }
            break ;
          case MID_SAVEAS:
            MyFileRequester = AllocAslRequestTags( ASL_FileRequest,
              ASLFR_Window, MyMainWindow->mw_Window,
              ASLFR_SleepWindow, TRUE,
              ASLFR_DoSaveMode, TRUE,
              TAG_END ) ;
            if( NULL != MyFileRequester )
            {  /* file requester ok */
              if( AslRequestTags( MyFileRequester, TAG_END ) )
              {  /* some file was selected */
                GadgetsToConfigFileData( MyMainWindow ) ;
                SaveConfigFile( MyContext->c_ConfigFile, MyFileRequester->fr_Drawer, MyFileRequester->fr_File ) ;
              }
              FreeAslRequest( MyFileRequester ) ;
            }
            break ;
          case MID_ABOUT:
            ShowAbout( MyContext ) ;
            break ;
          case MID_QUIT:
            MyContext->c_ShutdownRequest = 1 ;
            break ;
          case MID_DEFAULTS:
            SetDefaultConfigFileData( MyContext->c_ConfigFile ) ;
            ConfigFileDataToGadgets( MyMainWindow ) ;
            break ;
          case MID_SAVED:
            LoadConfigFile( MyContext->c_ConfigFile, "ENVARC:", "cdscsi.config" ) ;
            ConfigFileDataToGadgets( MyMainWindow ) ;
            break ;
          case MID_RESTORE:
            LoadConfigFile( MyContext->c_ConfigFile, "ENV:", "cdscsi.config" ) ;
            ConfigFileDataToGadgets( MyMainWindow ) ;
            break ;
        }
        break ;
      case WMHI_ICONIFY:
        CloseMainWindow( MyMainWindow, WM_ICONIFY ) ;
        MyMainWindow->mw_SignalMask = ( 1L << MyMainWindow->mw_AppPort->mp_SigBit ) ;
        break ;
      case WMHI_UNICONIFY:
        OpenMainWindow( MyMainWindow ) ;
        if( NULL != MyMainWindow->mw_Window )
        {  /* main window did reopen */
          GetAttr( WINDOW_SigMask, MyMainWindow->mw_WindowObject, &MyMainWindow->mw_SignalMask ) ;
          MyMainWindow->mw_SignalMask |= ( 1L << MyMainWindow->mw_AppPort->mp_SigBit ) ;
        }
        else
        {  /* main window did not reopen */
          MyContext->c_ShutdownRequest = 1 ;
        }
        break ;
      default:
        break ;
    }
  }

  return( MyMainWindow->mw_SignalMask ) ;
}
