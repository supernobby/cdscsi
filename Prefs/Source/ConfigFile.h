/*
 * ConfigFile.h
 */


#ifndef _CONFIGFILE_H
#define _CONFIGFILE_H


#include "ConfigFileData.h"


#define CONFIGFILE_WORKSTRING_LENGTH ( 256 )

/*
 * config file context
 */
struct ConfigFile
{
  struct Context *cf_Context ;  /* pointer to the context */
  struct ConfigFileData *cf_ConfigFileData ;  /* the actual data area */
  UBYTE cf_WorkString[ CONFIGFILE_WORKSTRING_LENGTH ] ;
} ;


/*
 * module functions
 */
struct ConfigFile *CreateConfigFile( struct Context *MyContext ) ;
void DeleteConfigFile( struct ConfigFile *MyConfigFile ) ;
void SetDefaultConfigFileData( struct ConfigFile *MyConfigFile ) ;
void LoadConfigFile( struct ConfigFile *MyConfigFile, UBYTE *FilePath, UBYTE *FileName ) ;
void SaveConfigFile( struct ConfigFile *MyConfigFile, UBYTE *FilePath, UBYTE *FileName ) ;


#endif   /* !_CONFIGFILE_H */
