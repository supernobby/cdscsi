/*
 * Libraries.c
 */


#include "Libraries.h"
#include <exec/libraries.h>
#include <proto/exec.h>


/*
 * possible library requirements
 */
enum
{
  LIB_OPTIONAL,
  LIB_ESSENTIAL
} ;


/*
 * collect relevant info of a library we use in there
 */
struct LibrarySpec
{
  struct Library **ls_Base ;  /* pointer to the library base */
  char *ls_Name ;  /* name of the library */
  ULONG ls_Version ;  /* required version */
  BYTE ls_Requirement ;  /* LIB_ESSENTIAL or LIB_OPTIONAL */
} ;


/*
 * all global library base variables
 */
/* struct ExecBase *SysBase is always open */
/* struct DosLibrary *DOSBase done automatically for amiga.lib */
struct IntuitionBase *IntuitionBase ;
struct LocaleBase *LocaleBase ;
struct Library *IconBase ;
struct Library *GadToolsBase ;
struct Library *AslBase ;
/* reaction classes */
struct Library *WindowBase ;
struct Library *LayoutBase ;
struct Library *StringBase ;
struct Library *ButtonBase ;
struct Library *ChooserBase ;
struct Library *IntegerBase ;



/*
 * collect all library specs in an array
 */
static const struct LibrarySpec Libraries[ ] =
{
  { ( struct Library ** )&IntuitionBase, "intuition.library", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&LocaleBase, "locale.library", 0, LIB_OPTIONAL },
  { ( struct Library ** )&IconBase, "icon.library", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&GadToolsBase, "gadtools.library", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&AslBase, "asl.library", 0, LIB_ESSENTIAL },
  /* reaction gadgets and classes */
  { ( struct Library ** )&WindowBase, "window.class", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&LayoutBase, "gadgets/layout.gadget", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&StringBase, "gadgets/string.gadget", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&ButtonBase, "gadgets/button.gadget", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&ChooserBase, "gadgets/chooser.gadget", 0, LIB_ESSENTIAL },
  { ( struct Library ** )&IntegerBase, "gadgets/integer.gadget", 0, LIB_ESSENTIAL }
} ;


/*
 * number of libraries
 */
#define LIBRARYCOUNT ( sizeof( Libraries ) / sizeof( struct LibrarySpec ) )


/*
 * open all libraries
 */
LONG OpenLibraries( void )
{
  UBYTE Count ;
  const struct LibrarySpec * Spec ;

  for( Count = 0 ; Count < LIBRARYCOUNT ; Count++ )
  {
    Spec = ( &Libraries[ Count ] ) ;
    *( Spec->ls_Base ) = OpenLibrary( Spec->ls_Name, Spec->ls_Version ) ;
    if( ( NULL == *( Spec->ls_Base ) ) && ( LIB_ESSENTIAL == Spec->ls_Requirement ) )
    {  /* library not ok */
      CloseLibraries( ) ;

      return( FALSE ) ;
    }
    else
    {  /* library ok or we can live without it */
    }
  }

  /* all done */
  return( TRUE ) ;
}


/*
 * close all libraries
 */
void CloseLibraries( void )
{
  UBYTE Count ;
  const struct LibrarySpec *Spec ;

  for( Count = 0 ; Count < LIBRARYCOUNT ; Count++ )
  {
    Spec = ( &Libraries[ Count ] ) ;
    if( NULL != *( Spec->ls_Base ) )
    {
      CloseLibrary( *( Spec->ls_Base ) ) ;
    }
  }
}
