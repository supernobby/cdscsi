/*
 * CdScsi.c
 */


#include "CdScsi.h"
#include "Libraries.h"
#include "Dispatcher.h"
#include "Localization.h"
#include "Settings.h"
#include "ConfigFile.h"
#include "MainWindow.h"
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/icon.h>
#include <workbench/startup.h>


/*
 * handle the action
 */
void CdScsiMain( struct Context *MyContext, enum DispatchMode MyMode  )
{
  DoDispatcher( MyContext->c_Dispatcher, MyMode ) ;

  if( MyContext->c_ShutdownRequest )
  {  /* check if it is save to shutdown */
    MyContext->c_Shutdown = 1 ;
  }
}


/*
 * common entry
 */
static LONG CdScsiEntry( int argc, char *argv[] )
{
  struct Context *MyContext ;
  struct WBStartup *MyWBStartup ;

  if( OpenLibraries( ) )
  {  /* libraries ok */
    MyContext = CreateAppContext( ) ;
    if( NULL != MyContext )
    {  /* context ok */
      MyContext->c_Dispatcher = CreateDispatcher( MyContext ) ;
      if( NULL != MyContext->c_Dispatcher )
      {  /* dispatcher ok */
        MyContext->c_Localization = CreateLocalization( MyContext ) ;
        MyContext->c_Settings = CreateSettings( MyContext ) ;
        if( NULL != MyContext->c_Settings )
        {  /* settings ok */
          MyContext->c_ConfigFile = CreateConfigFile( MyContext ) ;
          if( NULL != MyContext->c_ConfigFile )
          {  /* config file ok */
            SetDefaultConfigFileData( MyContext->c_ConfigFile ) ;
            LoadConfigFile( MyContext->c_ConfigFile, "ENV:", "cdscsi.config" ) ;
            if( 0 == argc )
            {  /* start from workbench */
              MyWBStartup = ( struct WBStartup * )argv ;
              CurrentDir( MyWBStartup->sm_ArgList[ 0 ].wa_Lock ) ;
              MyContext->c_DiskObject = GetDiskObject( MyWBStartup->sm_ArgList[ 0 ].wa_Name ) ;
            }
            else
            {  /* start from shell */
              MyContext->c_DiskObject = GetDiskObject( argv[ 0 ] ) ;
            }
            MyContext->c_MainWindow = CreateMainWindow( MyContext ) ;
            if( NULL != MyContext->c_MainWindow )
            {  /* main window ok */
              while( !( MyContext->c_Shutdown ) )
              {  /* as long as it goes ... */
                CdScsiMain( MyContext, DISPATCH_BLOCKING ) ;
              }
              DeleteMainWindow( MyContext->c_MainWindow ) ;
              /* disc object is released by window class */
              MyContext->c_DiskObject = NULL ;
            }
            else
            {  /* main window not ok */
            }
            if( NULL != MyContext->c_DiskObject )
            {  /* disk object needs to be freed */
              FreeDiskObject( MyContext->c_DiskObject ) ;
            }
            DeleteConfigFile( MyContext->c_ConfigFile ) ;
          }
          else
          {  /* config file not ok */
          }
          DeleteSettings( MyContext->c_Settings ) ;
        }
        else
        {  /* settings not ok */
        }
        DeleteLocalization( MyContext->c_Localization ) ;
        DeleteDispatcher( MyContext->c_Dispatcher ) ;
      }
      else
      {  /* dispatcher not ok */
      }
      DeleteAppContext( MyContext ) ;
    }
    else
    {  /* context not ok */
    }
    CloseLibraries( ) ;
  }
  else
  {  /* libraries not ok */
  }
  
  return( 0 ) ;
}


/*
 * called by strom startup when launched from cli
 */
int main( int argc, char *argv[] )
{
  return( CdScsiEntry( argc, argv ) ) ;
}


/*
 * called by strom startup when launched from workbench
 */
void wbmain( struct WBStartup *MyWBStartup )
{
  CdScsiEntry( 0, ( char ** )MyWBStartup ) ;
}
