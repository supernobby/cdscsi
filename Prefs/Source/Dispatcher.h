/*
 * Dispatcher.h
 */


#ifndef _DISPATCHER_H
#define _DISPATCHER_H


#include "Context.h"
#include <exec/lists.h>


/*
 * dispatch modes
 */
enum DispatchMode
{
  DISPATCH_NONBLOCKING,
  DISPATCH_BLOCKING
} ;


/*
 * dispatcher context
 */
struct Dispatcher
{
  struct Context *d_Context ;
  struct List d_DispatcheeList ;  /* list of installed signal handlers */
  ULONG d_SignalMask ;  /* signal mask of all the clients */
  ULONG d_SignalMaskDiff ;  /* if not null, mask update required */
} ;


/*
 * helper macro to cast callback function pointers
 */
#define DISPATCHER_DOFUNCTION ULONG ( * )( APTR, ULONG )


/*
 * common data for all signal handlers
 */
struct Dispatchee
{
  struct Node d_Node ;  /* to build a list of handlers */
  ULONG d_Signals ;  /* the signals the handler acts on, can be updated by handler return value */
  ULONG ( *d_DoFunction )( APTR DoData, ULONG TriggerSignals ) ;  /* actual handler function */
  APTR d_DoData ;  /* parameter for do function */
} ;


/*
** module functions
*/
struct Dispatcher *CreateDispatcher( struct Context *MyContext ) ;
void DeleteDispatcher( struct Dispatcher *MyDispatcher ) ;
void AddDispatchee( struct Dispatcher *MyDispatcher, struct Dispatchee *MyDispatchee ) ;
void RemDispatchee( struct Dispatcher *MyDispatcher, struct Dispatchee *MyDispatchee ) ;
void DoDispatcher( struct Dispatcher *MyDispatcher, enum DispatchMode MyMode ) ;


#endif  /* !_DISPATCHER_H */
